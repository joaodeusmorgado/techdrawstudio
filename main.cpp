#include <QGuiApplication>
#include <QQmlApplicationEngine>
//#include <QSurfaceFormat>
#include "mathlib.h"
#include "entities/entity_line.h"
#include "entities/entity_linethick.h"
#include "entities/entity_polylinethick.h"
#include "entities/entity_circle.h"
#include "entities/entity_circlethick.h"
#include "entities/entity_rectangle.h"
#include "entities/entity_rectanglethick.h"
#include "entities/linetexture.h"
#include "entities/enums.h"
#include "entities/cursorcross.h"
#include "entities/entity_square.h"
#include "pdfexporter.h"

int main(int argc, char *argv[])
{
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setOrganizationName("TechDrawStudio");
    app.setOrganizationDomain("estudiotecnologico.carrd.co");
    app.setApplicationName("TechDrawStudio");

    qmlRegisterType<MathLib>("MathLib", 1, 0, "MathLib");
    qmlRegisterType<Entity_Line>("Entity_Line", 1, 0, "EntityLine");//remove later
    qmlRegisterType<Entity_LineThick>("Entity_LineThick", 1, 0, "EntityLineThick");
    qmlRegisterType<Entity_PolyLineThick>("Entity_PolyLineThick", 1, 0, "EntityPolyLineThick");
    qmlRegisterType<Entity_Circle>("Entity_Circle", 1, 0, "EntityCircle");//remove later
    qmlRegisterType<Entity_CircleThick>("Entity_CircleThick", 1, 0, "EntityCircleThick");
    qmlRegisterType<Entity_Rectangle>("Entity_Rectangle", 1, 0, "EntityRectangle");//Remove later
    qmlRegisterType<Entity_RectangleThick>("Entity_RectangleThick", 1, 0, "EntityRectangleThick");
    qmlRegisterType<LineTexture>("LineTexture", 1, 0, "LineTexture");
    qmlRegisterType<CursorCross>("CursorCross", 1, 0, "CursorCross");
    qmlRegisterUncreatableType<CursorCross>("CursorEnum",1,0,"CursorEnum","Enum is not a type");
    qmlRegisterType<Entity_Square>("Entity_Square", 1, 0, "EntitySquare");

    MathLib li;

    //LineTexture

    Events::declareQML();

    //import com.Entity_Line 1.0
    //define a qml Line{}

    QQmlApplicationEngine engine;
    //engine.rootContext()->setContextProperty("_EntityDef", &entityDefinition);
    PdfExporter * dr = new PdfExporter(&engine);
    engine.rootContext()->setContextProperty("drawer",dr);

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

   // QSurfaceFormat surfaceFormat;
   // surfaceFormat.setMajorVersion(3);
   // surfaceFormat.setMinorVersion(3);
    //surfaceFormat.setProfile(QSurfaceFormat::CoreProfile);
    //surfaceFormat.setProfile(QSurfaceFormat::CompatibilityProfile);
    //QSurfaceFormat::setDefaultFormat(surfaceFormat);

    return app.exec();
}
