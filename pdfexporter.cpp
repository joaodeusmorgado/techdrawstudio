#include "pdfexporter.h"
#include <QQuickWindow>
#include <QPrinter>
#include <QPainter>

PdfExporter::PdfExporter(QQmlApplicationEngine *engine) : QQmlApplicationEngine(engine), mEngine(engine)
{
    m_fileName="file:///C:/Users/joao/Documents/test.pdf";
}

void PdfExporter::setFilePathAndName(QString fileName)
{
    m_fileName = fileName;
    qDebug()<<"setFilePathAndName m_fileName: "<<m_fileName;
}

void PdfExporter::screenShot()
{
    foreach(QObject* obj, mEngine->rootObjects()) {
      QQuickWindow* window = qobject_cast<QQuickWindow*>(obj);
      if (window)
      {
        QImage windowImage = window->grabWindow();
        QPrinter pdfPrinter(QPrinter::HighResolution);
        pdfPrinter.setOutputFormat(QPrinter::PdfFormat);
        pdfPrinter.setResolution(96);
        qDebug()<<"m_fileName: "<<m_fileName+".pdf";
        pdfPrinter.setOutputFileName(m_fileName+".pdf");
        //pdfPrinter.setOutputFileName("/home/joao/Desktop/test.pdf");
        QPainter painter;
        painter.begin(&pdfPrinter);
        painter.drawImage(QRect(0,0,windowImage.width(),windowImage.height()),
                          windowImage,
                          {0,0,windowImage.width(),windowImage.height()});
        painter.end();
      }
    }
}
