#ifndef PDFEXPORTER_H
#define PDFEXPORTER_H

#include <QQmlApplicationEngine>

class PdfExporter : public QQmlApplicationEngine
{
    Q_OBJECT

public:
    PdfExporter(QQmlApplicationEngine * engine);
    Q_INVOKABLE void setFilePathAndName(QString fileName);
    Q_INVOKABLE void screenShot();

private:
    QQmlApplicationEngine * mEngine;
    QString m_fileName;
};

#endif // PDFEXPORTER_H
