#include "mathlib.h"
#include <QDebug>

MathLib::MathLib(QObject *parent)
    : QObject(parent),
    m_offset(35)
{

}


QVector3D MathLib::sum3D(const QVector3D &p0, const QVector3D &p1) const
{
    return p0+p1;
}

QVector3D MathLib::sub3D(const QVector3D &p0, const QVector3D &p1) const
{
    return p0-p1;
}

bool MathLib::distance3DDelta(const QVector3D &p1, const QVector3D &p2, const float &delta) const
{
    return distance3D(p1, p2) > delta ? false : true;
}

float MathLib::distance3D(const QVector3D &p1, const QVector3D &p2) const
{
    return p1.distanceToPoint(p2);
}

float MathLib::distance2D(const float &a, const float &b) const
{
    return qAbs(a-b);
}

bool MathLib::checkPointInLine(const QVector3D &p0, const QVector3D &p1,
                                    const QVector3D &p, const float &delta) const
{
    QVector3D p0aux = p0.x() < p1.x() ? p0 : p1;
    QVector3D p1aux = p0.x() < p1.x() ? p1 : p0;

    float xMax = p0.x() > p1.x() ? p0.x() : p1.x();
    float xMin = p0.x() < p1.x() ? p0.x() : p1.x();
    float yMax = p0.y() > p1.y() ? p0.y() : p1.y();
    float yMin = p0.y() < p1.y() ? p0.y() : p1.y();

    //check for vertical lines
    //if ( p0aux.x() == p1aux.x() ) {
    if ( qAbs(p0aux.x() - p1aux.x() ) < delta){
        if (qAbs( p0aux.x() - p.x()) <  delta)
        //check if point is near the vertical line
        //line.highlighted = true
        return true;
    }

    //check for horizontal lines
    if (p0aux.y() == p1aux.y()) {
        if (qAbs( p0aux.y() - p.y() ) < delta )
        //console.log("check for horizontal lines")
        //line.highlighted = true
        return true;
    }

    // check other lines than vertical / horizontal
    float lineX = xMax - xMin;
    float lineY = yMax - yMin;
    float distX = p.x() - xMin;
    float distY = distX * lineY / lineX;

    //line is from left to right and top to bottom
    if ( p0aux.y() > p1aux.y() ) {
        if ( qAbs( yMax - distY - p.y() ) < delta ) {
            //console.log("line.highlighted before: "+line.highlighted)
            //line.highlighted = true
            return true;
        }
    }
    //line is from left to right and bottom to top
    else {
        if ( qAbs( yMin + distY - p.y() ) < delta ) {
            //line.highlighted = true
            return true;
        }
    }

    return false;
}

float MathLib::angleFromPoints(const QVector3D &centerPoint, const QVector3D &radiusPoint) const
{
    return qAtan2(radiusPoint.y() - centerPoint.y(), radiusPoint.x() - centerPoint.x());
}

QVector3D MathLib::rotate2d(const QVector3D &p, const float &angle) const
{
    float cosAngle = qCos(angle);
    float sinAngle = qSin(angle);

    return QVector3D(p.x() * cosAngle - p.y() * sinAngle,
                           p.x() * sinAngle + p.y() * cosAngle, p.z());
}

QVector3D MathLib::rotate2DFromPoint(const QVector3D &center, const float &angle, const QVector3D &p) const
{
    float cosAngle = qCos(angle);
    float sinAngle = qSin(angle);
    float x = cosAngle*p.x() - sinAngle*p.y() + center.x() - cosAngle*center.x() + sinAngle*center.y();
    float y = sinAngle*p.x() + cosAngle*p.y() + center.y() - sinAngle*center.x() - cosAngle*center.y();

    return QVector3D(x, y, p.z());
    /*        function rotate2DFromPoint(center: vector3, o: real, p: vector3d) {
        var x = Math.cos(o)*p.x - Math.sin(o)*p.y + center.x - Math.cos(o)*center.x + Math.sin(o)*center.y;
        var y = Math.sin(o)*p.x + Math.cos(o)*p.y + center.y - Math.sin(o)*center.x - Math.cos(o)*center.y;
        var r = Qt.vector3d(x, y, p.z);
        return r;
    }
    }*/
}

QVector3D MathLib::rectangleP0P2GetP1(const QVector3D &p0, const QVector3D &p2) const
{
    return QVector3D(p2.x(), p0.y(), average( p0.z(), p2.z() ) );
}

QVector3D MathLib::rectangleP0P2GetP3(const QVector3D &p0, const QVector3D &p2) const
{
    return QVector3D(p0.x(), p2.y(), average( p0.z(), p2.z() ) );
}

float MathLib::average(const float &a, const float &b) const
{
    return (a + b)/2;
}

bool MathLib::segmentFullyInsideRectangle(const QVector3D &lineP0, const QVector3D &lineP1,
                                          const QVector3D &rectP0, const QVector3D &rectP2) const
{
    float l0x = lineP0.x();
    float l0y = lineP0.y();
    float l0z = lineP0.z();

    float l1x = lineP1.x();
    float l1y = lineP1.y();
    float l1z = lineP1.z();

    //bottom left rectangle point
    float rxMin;
    float ryMin;
    float rzMin;

    // top right rectangle point
    float rxMax;
    float ryMax;
    float rzMax;


    if (rectP0.x() < rectP2.x()) {
        rxMin = rectP0.x();
        rxMax = rectP2.x();
    }
    else {
        rxMax = rectP0.x();
        rxMin = rectP2.x();
    }

    if (rectP0.y() < rectP2.y()) {
        ryMin = rectP0.y();
        ryMax = rectP2.y();
    }
    else {
        ryMax = rectP0.y();
        ryMin = rectP2.y();
    }

    if (rectP0.z() < rectP2.z()) {
        rzMin = rectP0.z();
        rzMax = rectP2.z();
    }
    else {
        rzMax = rectP0.z();
        rzMin = rectP2.z();
    }

    if (l0x >= rxMin && l1x >= rxMin && l0x <= rxMax && l1x <= rxMax &&
        l0y >= ryMin && l1y >= ryMin && l0y <= ryMax && l1y <= ryMax &&
        l0z >= rzMin && l1z >= rzMin && l0z <= rzMax && l1z <= rzMax)
        return true;
    else
        return false;
}

/*bool MathLib::checkSegmentsIntersection(const QVector3D &p0, const QVector3D &p1,
                                              const QVector3D &l0, const QVector3D &l1) const
{
    QVector3D dummy;
    return checkSegmentsIntersection(p0, p1, l0, l1, dummy);

}*/

bool MathLib::checkSegmentsIntersection(const QVector3D &l0, const QVector3D &l1,
                                     const QVector3D &r0, const QVector3D &r1
                                     /*,QVector3D &intersection*/) const
{
    //https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect

    float p0_x = l0.x();
    float p0_y = l0.y();
    float p1_x = l1.x();
    float p1_y = l1.y();

    float p2_x = r0.x();
    float p2_y = r0.y();
    float p3_x = r1.x();
    float p3_y = r1.y();


    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;
    float s1_y = p1_y - p0_y;
    float s2_x = p3_x - p2_x;
    float s2_y = p3_y - p2_y;
    float p0x_p2x = p0_x - p2_x;
    float p0y_p2y = p0_y - p2_y;


    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    if (qAbs(denominator) <= 1e-5)
        return false;

    float s, t;
    s = (-s1_y * p0x_p2x + s1_x * p0y_p2y) / denominator;
    t = ( s2_x * p0y_p2y - s2_y * p0x_p2x) / denominator;

    //s >= 0 && s <= 1 //intersectin between l0 and l1
    //t >= 0 && t <= 1 //intersectin between r0 and r1

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        //intersection.setX(p0_x + (t * s1_x));
        //intersection.setY(p0_y + (t * s1_y));
        //intersection.setZ(0);
        return true;
    }

    return false; // No collision


    /*float x1 = l0.x();
    float y1 = l0.y();
    float x2 = l1.x();
    float y2 = l1.y();

    float x3 = r0.x();
    float y3 = r0.y();
    float x4 = r1.x();
    float y4 = r1.y();

    float uA = ( (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3) / ( (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1) ) );

    float uB = ( (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3) / ( (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1) ) );

    if ( (uA >=0 && uA <=1) && (uB >=0 && uB <=1) )
        return true;
    else
        return false;
*/
    /*
    float intersectionX = x1 + (uA * (x2-x1));
    float intersectionY = y1 + (uA * (y2-y1));
    */

    /*

    the following code does not work
    float num = r1.x() - r0.x() - l1.x() + l0.x();
    //float num = r1.y() - r0.y() - l1.y() + l0.y();



    if (num == 0)
        return false;

    //float t = (r0.x() - l0.x()) / num;
    float t = (l0.x() - r0.x()) / num;

    float num1 = r1.y() - r0.y() - l1.y() + l0.y();
    if (num1 == 0)
        return false;

    float t1 = (l0.y() - r0.y()) / num1;

    if (t >= 0 && t <= 1 && t1 >= 0 && t1 <= 1) {
        //qDebug()<<"t is inside 0-1: "<<true;
        return true;
    }
    else {
        //qDebug()<<"t is outside 0-1: "<<false;
        return false;
    }*/
}


bool MathLib::isLinesIntersection(const QVector3D &l0, const QVector3D &l1,
                                     const QVector3D &r0, const QVector3D &r1) const
{
    float p0_x = l0.x();float p0_y = l0.y();float p1_x = l1.x();float p1_y = l1.y();
    float p2_x = r0.x();float p2_y = r0.y();float p3_x = r1.x();float p3_y = r1.y();

    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;float s1_y = p1_y - p0_y;float s2_x = p3_x - p2_x;float s2_y = p3_y - p2_y;
    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    if (qAbs(denominator) <= 1e-5)
        return false; // No collision

    return true;
}

QVector3D MathLib::getLinesIntersection(const QVector3D &l0, const QVector3D &l1,
                                     const QVector3D &r0, const QVector3D &r1) const
{
    float p0_x = l0.x();float p0_y = l0.y();float p1_x = l1.x();float p1_y = l1.y();
    float p2_x = r0.x();float p2_y = r0.y();float p3_x = r1.x();float p3_y = r1.y();

    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;float s1_y = p1_y - p0_y;float s2_x = p3_x - p2_x;float s2_y = p3_y - p2_y;
    float p0x_p2x = p0_x - p2_x;float p0y_p2y = p0_y - p2_y;
    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    QVector3D intersection;
    if (qAbs(denominator) <= 1e-5)
        return intersection; // No collision

    float t;
    t = ( s2_x * p0y_p2y - s2_y * p0x_p2x) / denominator;

    intersection.setX(p0_x + (t * s1_x));
    intersection.setY(p0_y + (t * s1_y));
    intersection.setZ(0);

    return intersection;
}


bool MathLib::checkLineRectangleIntersection(const QVector3D &l0, const QVector3D &l1,
                                          const QVector3D &r0, const QVector3D &r2) const
{


    QVector3D r1 = rectangleP0P2GetP1(r0, r2);

    if (checkSegmentsIntersection(l0, l1, r0, r1))
        return true;

    if (checkSegmentsIntersection(l0, l1, r1, r2))
        return true;

    QVector3D r3 = rectangleP0P2GetP3(r0, r2);

    if (checkSegmentsIntersection(l0, l1, r0, r3))
        return true;

    if (checkSegmentsIntersection(l0, l1, r2, r3))
        return true;

    return false;

}

/*
bool MathLib::segmentIntersectRectangle(const QVector3D &l0, const QVector3D &l1,
                                        const QVector3D &r0, const QVector3D &r2) const
{

    float rectangleMinX = r0.x() < r2.x() ? r0.x() : r2.x();
    float rectangleMinY = r0.y() < r2.y() ? r0.y() : r2.y();
    float rectangleMaxX = r0.x() < r2.x() ? r2.x() : r0.x();
    float rectangleMaxY = r0.y() < r2.y() ? r2.y() : r0.y();
    float p1X = l0.x();
    float p1Y = l0.y();
    float p2X = l1.x();
    float p2Y = l1.y();

    // Find min and max X for the segment
    double minX = p1X < p2X ? p1X : p2X;
    double maxX = p1X < p2X ? p2X : p1X;

    // Find the intersection of the segment's and rectangle's x-projections
    if (maxX > rectangleMaxX)
    {
        maxX = rectangleMaxX;
    }

    if (minX < rectangleMinX)
    {
        minX = rectangleMinX;
    }

    if (minX > maxX) // If their projections do not intersect return false
    {
        return false;
    }

    // Find corresponding min and max Y for min and max X we found before
    double minY = p1Y;
    double maxY = p2Y;

    double dx = p2X - p1X;

    if (qAbs(dx) > 0.0000001)
    {
        double a = (p2Y - p1Y)/dx;
        double b = p1Y - a*p1X;
        minY = a*minX + b;
        maxY = a*maxX + b;
    }

    if (minY > maxY)
    {
        double tmp = maxY;
        maxY = minY;
        minY = tmp;
    }

    // Find the intersection of the segment's and rectangle's y-projections
    if (maxY > rectangleMaxY)
    {
        maxY = rectangleMaxY;
    }

    if (minY < rectangleMinY)
    {
        minY = rectangleMinY;
    }

    if (minY > maxY) // If Y-projections do not intersect return false
    {
        return false;
    }

    return true;
}*/

bool MathLib::circleInsideRectangle(const QVector3D &center, const float &radius,
                                            const QVector3D &r0, const QVector3D &r2) const
{
    float reXmin = rectXmin(r0, r2);
    float reXmax = rectXmax(r0, r2);
    float reYmin = rectYmin(r0, r2);
    float reYmax = rectYmax(r0, r2);

    float cirXmin = center.x() - radius;
    float cirXmax = center.x() + radius;
    float cirYmin = center.y() - radius;
    float cirYmax = center.y() + radius;



    if ( (cirXmin > reXmin) &&  (cirXmax < reXmax) && (cirYmin > reYmin) &&  (cirYmax < reYmax) )
        return true;

    return false;
}

bool MathLib::circleOutsideRectangle(const QVector3D &center, const float &radius,
                                            const QVector3D &r0, const QVector3D &r2) const
{
    float reXmin = rectXmin(r0, r2);
    float reXmax = rectXmax(r0, r2);
    float reYmin = rectYmin(r0, r2);
    float reYmax = rectYmax(r0, r2);

    float cirXmin = center.x() - radius;
    float cirXmax = center.x() + radius;
    float cirYmin = center.y() - radius;
    float cirYmax = center.y() + radius;

    if ( (cirXmax < reXmin ) && (cirXmin > reXmax) && (cirYmin > reYmax) && (cirYmax < reYmin) )
        return true;

    return false;
}

bool MathLib::circleIntersectRectangle(const QVector3D &center, const float &radius,
                                            const QVector3D &r0, const QVector3D &r2) const
{
    //if (circleInsideRectangle(center, radius, r0, r2))
      //  return true;

    float reXmax = max(r0.x(), r2.x());
    float reXmin = min(r0.x(), r2.x());

    float reYmax = max(r0.y(), r2.y());
    float reYmin = min(r0.y(), r2.y());

    float nearestX = max(reXmin, min(center.x(), reXmax));
    float nearestY = max(reYmax, min(center.y(), reYmin) );

    //float deltaX = center.x() - max(reXmin, min(center.x(), reXmax));
    //float deltaY = center.y() - max(reYmax, min(center.y(), reYmin) );

    float deltaX = center.x() - nearestX;
    float deltaY = center.y() - nearestY;

    return (deltaX*deltaX + deltaY*deltaY) < (radius*radius);
}

float MathLib::rectXmin(const QVector3D &r0, const QVector3D &r2) const
{
    return r0.x() < r2.x() ? r0.x() : r2.x();
}

float MathLib::rectXmax(const QVector3D &r0, const QVector3D &r2) const
{
    return r0.x() > r2.x() ? r0.x() : r2.x();
}

float MathLib::rectYmin(const QVector3D &r0, const QVector3D &r2) const
{
    return r0.y() < r2.y() ? r0.y() : r2.y();
}

float MathLib::rectYmax(const QVector3D &r0, const QVector3D &r2) const
{
    return r0.y() > r2.y() ? r0.y() : r2.y();
}

float MathLib::min(const float &a, const float &b) const
{
    return a < b ? a : b;
}

float MathLib::max(const float &a, const float &b) const
{
    return a > b ? a : b;
}

bool MathLib::filletCalculate(const QVector3D &l0, const QVector3D &l1, const QVector3D &pickPointL,
                                    const QVector3D &r0, const QVector3D &r1, const QVector3D &pickPointR)
{
    //https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect

    m_fillet_l0 = l0;
    m_fillet_l1 = l1;
    m_fillet_r0 = r0;
    m_fillet_r1 = r1;

    float p0_x = l0.x();
    float p0_y = l0.y();
    float p1_x = l1.x();
    float p1_y = l1.y();

    float p2_x = r0.x();
    float p2_y = r0.y();
    float p3_x = r1.x();
    float p3_y = r1.y();


    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;
    float s1_y = p1_y - p0_y;
    float s2_x = p3_x - p2_x;
    float s2_y = p3_y - p2_y;
    float p0x_p2x = p0_x - p2_x;
    float p0y_p2y = p0_y - p2_y;


    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    if (qAbs(denominator) <= 1e-5)
        return false;

    float tR, tL;
    tR = (-s1_y * p0x_p2x + s1_x * p0y_p2y) / denominator; // line R
    tL = ( s2_x * p0y_p2y - s2_y * p0x_p2x) / denominator; // line L

    //s >= 0 && s <= 1 //intersection between l0 and l1
    //t >= 0 && t <= 1 //intersection between r0 and r1

    //if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    m_fillet_intersection.setX(p0_x + (tL * s1_x)); //check if should be tR instead of tL
    m_fillet_intersection.setY(p0_y + (tL * s1_y)); //check if should be tR instead of tL
    m_fillet_intersection.setZ(0);
    qDebug()<<"qDebug() m_fillet_intersection: " << m_fillet_intersection;

    qDebug()<<"tL: "<<tL;
    qDebug()<<"tR: "<<tR;

    if ( (tL < 0 || tL > 1) && (tR < 0 || tR > 1)) //intersection is out of both segments
    {
        qDebug()<<"intersection is out of both segments";

        float distL0Intersec = distance3D(l0, m_fillet_intersection);
        float distL1Intersec = distance3D(l1, m_fillet_intersection);

        qDebug()<<"m_fillet_intersection 2: " << m_fillet_intersection;
        if (distL1Intersec < distL0Intersec)
            m_fillet_l1 = m_fillet_intersection;
        else
            m_fillet_l0 = m_fillet_intersection;

        float distR0Intersec = distance3D(r0, m_fillet_intersection);
        float distR1Intersec = distance3D(r1, m_fillet_intersection);

        if (distR1Intersec < distR0Intersec)
            m_fillet_r1 = m_fillet_intersection;
        else
            m_fillet_r0 = m_fillet_intersection;

        qDebug()<<"m_fillet_l0: "<<m_fillet_l0;
        qDebug()<<"m_fillet_l1: "<<m_fillet_l1;
        qDebug()<<"m_fillet_r0: "<<m_fillet_r0;
        qDebug()<<"m_fillet_r1: "<<m_fillet_r1;

        return true;
    }



    if ( (tL < 0 || tL > 1) && (tR >= 0 && tR <= 1)) //intersection in segment R, not in segment L
    {
        qDebug()<<"intersection in segment R, not in segment L";


        float distL0Intersec = distance3D(l0, m_fillet_intersection);
        float distL1Intersec = distance3D(l1, m_fillet_intersection);

        if (distL0Intersec < distL1Intersec)
            m_fillet_l0 = m_fillet_intersection;
        else
            m_fillet_l1 = m_fillet_intersection;

        float distR0Intersec = distance3D(r0, m_fillet_intersection);
        float distR0PickPoint = distance3D(r0, pickPointR);

        if (distR0PickPoint < distR0Intersec)
            m_fillet_r1 = m_fillet_intersection;
        else
            m_fillet_r0 = m_fillet_intersection;

        return true;
    }




    if ( (tL >= 0 && tL <= 1) && (tR < 0 || tR > 1)) //intersection in segment L, not in segment R
    {
        qDebug()<<"intersection in segment L, not in segment R";
        qDebug()<<"tL: "<<tL;
        qDebug()<<"tR: "<<tR;
        qDebug()<<"l0"<<l0;
        qDebug()<<"l1"<<l1;
        qDebug()<<"r0"<<r0;
        qDebug()<<"r1"<<r1;
        qDebug()<<"pickPointL: "<<pickPointL;

        float distR0Intersec = distance3D(r0, m_fillet_intersection);
        float distR1Intersec = distance3D(r1, m_fillet_intersection);

        if (distR0Intersec < distR1Intersec)
            m_fillet_r0 = m_fillet_intersection;
        else
            m_fillet_r1 = m_fillet_intersection;


        float distL0Intersec = distance3D(l0, m_fillet_intersection);
        float distL0PickPoint = distance3D(l0, pickPointL);

        if (distL0PickPoint < distL0Intersec)
            m_fillet_l1 = m_fillet_intersection;
        else
            m_fillet_l0 = m_fillet_intersection;

        return true;
    }

    return false;

    //---------------------
/*
    if (s < 0 || s > 1) {//intersection is outside line segment

        float distL0Intersec = distance3D(l0, m_fillet_intersection);
        float distL1Intersec = distance3D(l1, m_fillet_intersection);

        if (distL0Intersec > distL1Intersec) {
            m_fillet_l0 = l0;
            qDebug() << "qDebug() m_fillet_l0: " << m_fillet_l0;
            m_fillet_l1 = m_fillet_intersection;
        }
        else {
            m_fillet_l0 = m_fillet_intersection;
            qDebug() << "qDebug() m_fillet_l0: " << m_fillet_l0;
            m_fillet_l1 = l1;
        }
    }

    if (s >= 0 && s <= 1) {//intersection is inside line segment
        float distP0PickPoint = distance3D(l0,pickPointL);
        float distP1Inters = distance3D(l0, m_fillet_intersection);

        if (distP0PickPoint < distP1Inters) {
            m_fillet_l0 = l0;
            m_fillet_l1 = m_fillet_intersection;
        }
        else {
            m_fillet_l0 = m_fillet_intersection;
            m_fillet_l1 = l1;
        }m_fillet_intersection
    }

    if (t < 0 || t > 1) {//intersection is outside line segment
        float distR0Intersec = distance3D(r0, m_fillet_intersection);
        float distR1Intersec = distance3D(r1, m_fillet_intersection);

        if (distR0Intersec > distR1Intersec) {
            m_fillet_r0 = r0;m_fillet_intersection
            m_fillet_r1 = m_fillet_intersection;
        }
        else {
            m_fillet_r0 = m_fillet_intersection;
            m_fillet_r1 = r1;
        }
    }

    if (t >= 0 && t <= 1) {//intersection is inside line segment
        float distR0PickPoint = distance3D(r0,pickPointR);
        float distR1Inters = distance3D(r0, m_fillet_intersection);

        if (distR0PickPoint < distR1Inters) {
            m_fillet_r0 = r0;
            m_fillet_r1 = m_fillet_intersection;
        }
        else {
            m_fillet_r0 = m_fillet_intersection;
            m_fillet_r1 = r1;
        }
    }*/

}

bool MathLib::trimCalculate(const QVector3D &l0, const QVector3D &l1,
                            const QVector3D &r0, const QVector3D &r1, const QVector3D &pickPointR)
{
    m_fillet_l0 = l0;
    m_fillet_l1 = l1;
    m_fillet_r0 = r0;
    m_fillet_r1 = r1;

    float p0_x = l0.x();
    float p0_y = l0.y();
    float p1_x = l1.x();
    float p1_y = l1.y();

    float p2_x = r0.x();
    float p2_y = r0.y();
    float p3_x = r1.x();
    float p3_y = r1.y();


    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;
    float s1_y = p1_y - p0_y;
    float s2_x = p3_x - p2_x;
    float s2_y = p3_y - p2_y;
    float p0x_p2x = p0_x - p2_x;
    float p0y_p2y = p0_y - p2_y;


    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    if (qAbs(denominator) <= 1e-5)
        return false;

    float tR, tL;
    tR = (-s1_y * p0x_p2x + s1_x * p0y_p2y) / denominator; // line R
    tL = ( s2_x * p0y_p2y - s2_y * p0x_p2x) / denominator; // line L

    //s >= 0 && s <= 1 //intersection between l0 and l1
    //t >= 0 && t <= 1 //intersection between r0 and r1

    //if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    QVector3D intersection;
    intersection.setX(p0_x + (tL * s1_x));
    intersection.setY(p0_y + (tL * s1_y));
    intersection.setZ(0);
    qDebug()<<"qDebug() trim intersection: "<<intersection;

    qDebug()<<"tL: "<<tL;
    qDebug()<<"tR: "<<tR;

    if ( (tL >= 0 && tL <= 1) && (tR >= 0 && tR <= 1)) //intersection is inside of both segments
    {
         float distR0Intersec = distance3D(r0, intersection);
         float distR0PickPoint = distance3D(r0, pickPointR);

         if (distR0PickPoint < distR0Intersec){
             m_trim_lines_r0 = intersection;
             m_trim_lines_r1 = r1;
         }
         else {
             m_trim_lines_r0 = r0;
             m_trim_lines_r1 = intersection;
         }
         return true;
    }

    return false;
}

void MathLib::setExtendReferenceLine(const QVector3D &p0, const QVector3D &p1)
{
    m_extend_referenceLine_p0 = p0;
    m_extend_referenceLine_p1 = p1;


}

bool MathLib::setExtendLine(const QVector3D &p0, const QVector3D &p1)
{

    m_extend_lines_p0 = p0;
    m_extend_lines_p1 = p1;


    float p0_x = m_extend_referenceLine_p0.x();
    float p0_y = m_extend_referenceLine_p0.y();
    float p1_x = m_extend_referenceLine_p1.x();
    float p1_y = m_extend_referenceLine_p1.y();

    float p2_x = p0.x();
    float p2_y = p0.y();
    float p3_x = p1.x();
    float p3_y = p1.y();


    //float s1_x, s1_y, s2_x, s2_y;
    float s1_x = p1_x - p0_x;
    float s1_y = p1_y - p0_y;
    float s2_x = p3_x - p2_x;
    float s2_y = p3_y - p2_y;
    float p0x_p2x = p0_x - p2_x;
    float p0y_p2y = p0_y - p2_y;


    float denominator = (-s2_x * s1_y + s1_x * s2_y);

    if (qAbs(denominator) <= 1e-5)
        return false;

    float tR, tL;
    tR = (-s1_y * p0x_p2x + s1_x * p0y_p2y) / denominator; // line R
    tL = ( s2_x * p0y_p2y - s2_y * p0x_p2x) / denominator; // line L

    //s >= 0 && s <= 1 //intersection between l0 and l1
    //t >= 0 && t <= 1 //intersection between r0 and r1

    //if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    m_extend_intersection.setX(p0_x + (tL * s1_x)); //check if should be tR instead of tL
    m_extend_intersection.setY(p0_y + (tL * s1_y)); //check if should be tR instead of tL
    m_extend_intersection.setZ(0);
    qDebug()<<"qDebug() m_extend_intersection: " << m_extend_intersection;

    qDebug()<<"tL: "<<tL;
    qDebug()<<"tR: "<<tR;

    //if (tL >= 0 && tL <= 1) {
        float distP0Intersec = distance3D(p0, m_extend_intersection);
        float distP1Intersec = distance3D(p1, m_extend_intersection);

        if (distP0Intersec < distP1Intersec)
            m_extend_lines_p0 = m_extend_intersection;
        else
            m_extend_lines_p1 = m_extend_intersection;
        return true;
   // }

    return false;
}

void MathLib::setOffset(const float &offset)
{
    m_offset = offset;
}

void MathLib::offsetLineCalculate(const QVector3D &p0, const QVector3D &p1, const QVector3D &side)
{

    qDebug()<<"p0: "<<p0;
    qDebug()<<"p1: "<<p1;

    float angle = angleFromPoints(p0, p1);
    float dx = m_offset * qCos(angle+M_PI_2);
    float dy = m_offset * qSin(angle+M_PI_2);

    /*qDebug()<<"angle rad: "<<angle<<" ...................................";
    qDebug()<<"angle degree: "<<qRadiansToDegrees(angle)<<" ...................................";
    qDebug()<<"dx: "<<dx;
    qDebug()<<"dy: "<<dy;*/

    m_offsetLineP0 = QVector3D(p0.x() + dx, p0.y() + dy, 0);
    m_offsetLineP1 = QVector3D(p1.x() + dx, p1.y() + dy, 0);

    if ( distance3D(p0, side) <  distance3D(m_offsetLineP0, side) ) {
        m_offsetLineP0 = QVector3D(p0.x() - dx, p0.y() - dy, 0);
        m_offsetLineP1 = QVector3D(p1.x() - dx, p1.y() - dy, 0);
    }

    //qDebug()<<"m_offsetLineP0: "<<m_offsetLineP0;
    //qDebug()<<"m_offsetLineP1: "<<m_offsetLineP1;
}

bool MathLib::offsetCircleCalculate(const QVector3D &center, const float &radius, const QVector3D &side)
{
    float distCenterSide = distance3D(center, side);
    if (distCenterSide < radius)
        m_offsetCircleRadius = radius - m_offset;
    else
        m_offsetCircleRadius = radius + m_offset;

    if (m_offsetCircleRadius > 0)
        return true;
    else
        return false;
}

