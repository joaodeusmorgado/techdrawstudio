QT += quick quick3d printsupport

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        entities/cursorcross.cpp \
        entities/entity.cpp \
        entities/entity_circle.cpp \
        entities/entity_circlethick.cpp \
        entities/entity_line.cpp \
        entities/entity_linethick.cpp \
        entities/entity_polylinethick.cpp \
        entities/entity_rectangle.cpp \
        entities/entity_rectanglethick.cpp \
        entities/entity_square.cpp \
        entities/geometryposition.cpp \
        entities/linetexture.cpp \
        main.cpp \
        mathlib.cpp \
        pdfexporter.cpp

RESOURCES += qml.qrc \
    images.qrc \
    shaders.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    entities/cursorcross.h \
    entities/entity.h \
    entities/entity_circle.h \
    entities/entity_circlethick.h \
    entities/entity_line.h \
    entities/entity_linethick.h \
    entities/entity_polylinethick.h \
    entities/entity_rectangle.h \
    entities/entity_rectanglethick.h \
    entities/entity_square.h \
    entities/enums.h \
    entities/geometryposition.h \
    entities/linetexture.h \
    mathlib.h \
    pdfexporter.h

ios {
    ios_icon.files = $$files($$PWD/ios/AppIcon*.png)
    QMAKE_BUNDLE_DATA += ios_icon
    QMAKE_INFO_PLIST = ios/Info.plist
}

DISTFILES += \
    android/AndroidManifest.xml \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/build.gradle \
    android/gradle.properties \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew \
    android/gradlew.bat \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/res/values/libs.xml

win32: RC_FILE = appicon.rc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

ANDROID_MIN_SDK_VERSION = "23"
ANDROID_TARGET_SDK_VERSION = "33"

ICON = images/circle256.icns
