import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    color: colorsUI.currentStyle.mainColor

    ButtonGroup {
        buttons: column.children
    }

    property alias btnP1P2: btnP1P2
    property alias btnSquareCenterSideLenght: btnSquareCenterSideLenght

    Column {
        id: column

        padding: defaultMargins
        spacing: defaultMargins
        RadioButton {
            id: btnP1P2
            checked: true
            text: qsTr("Rectangle: p1(x,y,z) -> p2(x,y,z)")
            onClicked: {
                setModelP1P2()
            }
        }

        RadioButton {
            id: btnSquareCenterSideLenght
            text: qsTr("Square center(x,y,z) -> Square side lenght")
            //height: btnSize
            onClicked: {
                setModelCenterSideLenght()
            }
        }

    }

    PropertiesListEditor {
        id: lineEd
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: column.bottom
        anchors.topMargin: -defaultMargins
        anchors.bottom: btnBox.top
        //anchors.margins: defaultMargins

        Component.onCompleted: {
            setModelP1P2()
        }
    }

    // default values
    //p1 -> p2
    property vector3d p0: Qt.vector3d(0, 0, 0)
    property vector3d p2: Qt.vector3d(300,200,0)
    property vector3d p1: mathLib.rectangleP0P2GetP1(p0, p2)
    property vector3d p3: mathLib.rectangleP0P2GetP3(p0, p2)

    //center -> side lenght
    property vector3d squareCenter: Qt.vector3d(5, 5, 0)
    property real squareSideLenght: 50
    property real halfLenght: squareSideLenght * 0.5
    property vector3d squareP0: Qt.vector3d(squareCenter.x-halfLenght, squareCenter.y+halfLenght, 0)
    property vector3d squareP1: Qt.vector3d(squareCenter.x+halfLenght, squareCenter.y+halfLenght, 0)
    property vector3d squareP2: Qt.vector3d(squareCenter.x+halfLenght, squareCenter.y-halfLenght, 0)
    property vector3d squareP3: Qt.vector3d(squareCenter.x-halfLenght, squareCenter.y-halfLenght, 0)


    //p0: Qt.vector3d(center.x-squareDist, center.y+squareDist, 0)
    //p2: Qt.vector3d(center.x+squareDist, center.y-squareDist, 0)


    function setModelP1P2() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "Rectangle p0", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "p0.x", "propertyValue": String(p0.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p0.y", "propertyValue": String(p0.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p0.z", "propertyValue": String(p0.z), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "Rectangle p2", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "p2.x", "propertyValue": String(p2.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p2.y", "propertyValue": String(p2.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p2.z", "propertyValue": String(p2.z), "propertyIsTitle": false})
    }

    function setModelCenterSideLenght() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "Square center", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "center.x", "propertyValue": String(squareCenter.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "center.y", "propertyValue": String(squareCenter.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "center.z", "propertyValue": String(squareCenter.z), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "Square Side Lenght", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "lenght", "propertyValue": String(squareSideLenght), "propertyIsTitle": false})
    }

    function getP1P2Values() {
        if (lineEd.myModel.count !== 8 ) {
            console.log("line model Error!!!")
            return;
        }

        p0.x = parseFloat(lineEd.myModel.get(1).propertyValue)
        p0.y = parseFloat(lineEd.myModel.get(2).propertyValue)
        p0.z = parseFloat(lineEd.myModel.get(3).propertyValue)

        p2.x = parseFloat(lineEd.myModel.get(5).propertyValue)
        p2.y = parseFloat(lineEd.myModel.get(6).propertyValue)
        p2.z = parseFloat(lineEd.myModel.get(7).propertyValue)

        console.log("getP1P2Values()------------------ rectangle")
        console.log("p0: "+p0)
        console.log("p2: "+p2)


    }

    function getCenterSideLenght() {
        if (lineEd.myModel.count !== 6 ) {
            console.log("line model Error!!!")
            return;
        }

        squareCenter.x = parseFloat(lineEd.myModel.get(1).propertyValue)
        squareCenter.y = parseFloat(lineEd.myModel.get(2).propertyValue)
        squareCenter.z = parseFloat(lineEd.myModel.get(3).propertyValue)

        squareSideLenght = parseFloat(lineEd.myModel.get(5).propertyValue)
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            if (btnP1P2.checked)
                getP1P2Values()
            if (btnSquareCenterSideLenght.checked)
                getCenterSideLenght()
            root.ok()
        }
        onCancel: root.cancel()
    }
}
