import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    color: colorsUI.currentStyle.mainColor

    PropertiesListEditor {
        id: lineEd
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        //anchors.topMargin: defaultMargins
        anchors.bottom: btnBox.top
        //anchors.margins: defaultMargins

        Component.onCompleted: {
            setModelCenterRadius()
        }
    }

    property vector3d circleCenter: Qt.vector3d(5, 5, 0) //default value
    property real circleRadius: 50 // default value

    function setModelCenterRadius() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "Circle center", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "center.x", "propertyValue": String(circleCenter.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "center.y", "propertyValue": String(circleCenter.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "center.z", "propertyValue": String(circleCenter.z), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "Circle radius", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "radius", "propertyValue": String(circleRadius), "propertyIsTitle": false})
    }

    function getCenterRadius() {
        if (lineEd.myModel.count !== 6 ) {
            console.log("circle model Error!!!")
            return;
        }
        circleCenter.x = parseFloat(lineEd.myModel.get(1).propertyValue)
        circleCenter.y = parseFloat(lineEd.myModel.get(2).propertyValue)
        circleCenter.z = parseFloat(lineEd.myModel.get(3).propertyValue)
        circleRadius = parseFloat(lineEd.myModel.get(5).propertyValue)
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            getCenterRadius()
            root.ok()
        }
        onCancel: root.cancel()
    }
}
