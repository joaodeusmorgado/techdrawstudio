import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Window  {
    id: root
    //x: mainRoot.x
    //y: mainRoot.y
    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    title: qsTr("Offset editor")

    property alias offsetEd: offsetEd

    OffSetEditor {
        id: offsetEd
        anchors.fill: parent
    }
}
