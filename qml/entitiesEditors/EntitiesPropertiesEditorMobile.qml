import QtQuick 2.6
import "../components"


Rectangle  {
    id: root
    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    property alias entitiesPropertiesEd: entitiesPropertiesEd

    EntitiesPropertiesEditor {
        id: entitiesPropertiesEd
        anchors.fill: parent
    }
}
