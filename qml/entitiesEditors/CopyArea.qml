import QtQuick 2.6
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width *0.2
    height: mainRoot.height * 0.8


    color: colorsUI.currentStyle.mainColor
    border.color: colorsUI.currentStyle.frameBorderColor
    border.width: btnBorderWidth
    radius: btnSize * 0.4

    MouseArea {
        anchors.fill: parent
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            root.ok()
        }
        onCancel: root.cancel()
    }


}
