import QtQuick 2.0
import "../components"

Rectangle {
    width: 33*mm
    height: 8*mm
    color: "orange"
    TextInput {
        anchors.centerIn: parent
        width: mainRoot.btnSize * 2
        height: mainRoot.btnSize * 1.5
        color: "green"
        clip: true
    }
}
