import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

//lineEditorDesktop uses a Window, is a wrapper for desktop Windows, Linux, MacOS
//LineEditorMobile uses a Rectangle, a wrapper for Android, iOS
//LineEditor is the real deal

Rectangle  {
    id: root
    width: mainRoot.width //*0.2
    height: mainRoot.height //*0.5

    property alias lineEditor: lineEd

    LineEditor {
        id: lineEd
        anchors.fill: parent
    }
}
