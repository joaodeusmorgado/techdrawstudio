import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width //*0.2
    height: mainRoot.height //*0.5

    property alias circleEditor: circleEd

    CircleEditor {
        id: circleEd
        anchors.fill: parent
    }
}

