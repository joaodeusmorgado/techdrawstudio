import QtQuick 2.6
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    color: colorsUI.currentStyle.mainColor

    PropertiesListEditor {
        id: entitiesPropertiesEd
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        //anchors.topMargin: defaultMargins
        anchors.bottom: btnBox.top
        //anchors.margins: defaultMargins

        Component.onCompleted: {
            setEntitiesValuesLineSelection()
        }
    }

    property vector3d circleCenter: Qt.vector3d(5, 5, 0) //default value
    property real circleRadius: 50 // default value

    function setEntitiesValuesNoSelection() {
        entitiesPropertiesEd.myModel.clear()

        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("General"), "propertyValue": "", "propertyIsTitle": true})
        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("color"), "propertyValue": color4NewEntities, "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("Layer"), "propertyValue": layer0, "propertyIsTitle": false})
    }

    function setEntitiesValuesLineSelection() {
        entitiesPropertiesEd.myModel.clear()

        var x0 =-2, y0=-4, z0=-3
        var x1=12, y1=14, z1=25
        var length=34, angle=48
        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("Line"), "propertyValue": "", "propertyIsTitle": true})
        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("Start point"), "propertyValue": "", "propertyIsTitle": true})

        entitiesPropertiesEd.myModel.append({"propertyName": "x", "propertyValue": String(x0), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "y", "propertyValue": String(y0), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "z", "propertyValue": String(z0), "propertyIsTitle": false})

        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("End point"), "propertyValue": "", "propertyIsTitle": true})
        entitiesPropertiesEd.myModel.append({"propertyName": "x", "propertyValue": String(x1), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "y", "propertyValue": String(y1), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "z", "propertyValue": String(z1), "propertyIsTitle": false})

        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("Delta"), "propertyValue": "", "propertyIsTitle": true})
        entitiesPropertiesEd.myModel.append({"propertyName": "x", "propertyValue": String(x1-x0), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "y", "propertyValue": String(y1-y0), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": "z", "propertyValue": String(z1-z0), "propertyIsTitle": false})

        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("length"), "propertyValue": String(length), "propertyIsTitle": false})
        entitiesPropertiesEd.myModel.append({"propertyName": qsTr("angle"), "propertyValue": String(angle), "propertyIsTitle": false})

    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            //getCenterRadius()
            root.ok()
        }
        onCancel: root.cancel()
    }

}
