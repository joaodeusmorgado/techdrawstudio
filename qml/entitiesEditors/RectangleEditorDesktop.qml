import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

//rectangleEditorDesktop uses a Window, is a wrapper for desktop Windows, Linux, MacOS
//rectangleEditorMobile uses a Rectangle, a wrapper for Android, iOS
//rectangleEditor is the real deal

Window  {
    id: root
    //x: mainRoot.x
    //y: mainRoot.y
    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    title: qsTr("Rectangle editor")

    property alias rectangleEditor: rectangleEd

    RectangleEditor {
        id: rectangleEd
        anchors.fill: parent
    }
}
