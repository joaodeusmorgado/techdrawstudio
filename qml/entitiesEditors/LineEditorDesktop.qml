import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

//lineEditorDesktop uses a Window, is a wrapper for desktop Windows, Linux, MacOS
//LineEditorMobile uses a Rectangle, a wrapper for Android, iOS
//LineEditor is the real deal

Window  {
    id: root
    //x: mainRoot.x
    //y: mainRoot.y
    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    title: qsTr("Line editor")

    property alias lineEditor: lineEd

    LineEditor {
        id: lineEd
        anchors.fill: parent
    }
}
