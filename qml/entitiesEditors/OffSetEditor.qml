import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    color: colorsUI.currentStyle.mainColor

    PropertiesListEditor {
        id: lineEd
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        //anchors.topMargin: defaultMargins
        anchors.bottom: btnBox.top
        //anchors.margins: defaultMargins

        Component.onCompleted: {
            setOffsetValue()
        }
    }

    property real offsetValue: 10//default value

    function setOffsetValue() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "Offset", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "Offset value", "propertyValue": String(offsetValue), "propertyIsTitle": false})
    }

    function getOffsetValue() {
        if (lineEd.myModel.count !== 2 ) {
            console.log("offset model Error!!!")
            return;
        }
        offsetValue = parseFloat(lineEd.myModel.get(1).propertyValue)
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            getOffsetValue()
            root.ok()
        }
        onCancel: root.cancel()
    }
}
