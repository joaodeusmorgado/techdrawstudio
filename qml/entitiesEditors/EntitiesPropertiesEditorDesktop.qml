import QtQuick 2.6
import "../components"

Window  {
    id: root
    //x: mainRoot.x
    //y: mainRoot.y
    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    title: qsTr("Entities Property Editor")

    property alias entitiesPropertiesEd: entitiesPropertiesEd

    EntitiesPropertiesEditor {
        id: entitiesPropertiesEd
        anchors.fill: parent
    }

    onVisibleChanged: {
        focus  = visible ? true : false
    }
    onActiveFocusItemChanged: console.log("onActiveFocusItemChanged: ", activeFocus)

    /*Keys.onPressed: {
        cmdLine.focus = true
    }*/

    /*Keys.onPressed: function (event){
        if ((event.key === Qt.Key_1) && (event.modifiers & Qt.ControlModifier)) {

            console.log("crtl+1 sdfsfsfsewetrwtr")

            root.visible = false
        }
    }*/

}
