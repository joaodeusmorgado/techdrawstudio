import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.qmlmodels 1.0
import "../components"

Rectangle  {
    id: root
    width: mainRoot.width
    height: mainRoot.height

    color: colorsUI.currentStyle.mainColor

    ButtonGroup {
        buttons: column.children
    }

    property alias btnP1P2: btnP1P2
    property alias btnP1DistAngle: btnP1DistAngle

    Column {
        id: column

        padding: defaultMargins
        spacing: defaultMargins
        RadioButton {
            id: btnP1P2
            checked: true
            text: qsTr("point 1(x,y,z) -> point 2(x,y,z)")
            onClicked: {
                setModelP1P2()
            }
        }

        RadioButton {
            id: btnP1DistAngle
            text: qsTr("point(x,y,z) -> lenght -> angle")
            //height: btnSize
            onClicked: {
                setModelP1DistAngle()
            }
        }
    }

    PropertiesListEditor {
        id: lineEd
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: column.bottom
        anchors.topMargin: -defaultMargins
        anchors.bottom: btnBox.top
        //anchors.margins: defaultMargins

        Component.onCompleted: {
            setModelP1P2()
        }
    }

    // default values
    //p1 -> p2
    property vector3d p1: Qt.vector3d(0, 0, 0)
    property vector3d p2: Qt.vector3d(100,100,0)

    //p -> lenght -> angle
    property vector3d p: Qt.vector3d(5, 5, 0)
    property vector3d paux
    property real lenght: 50
    property real angle: 45


    function setModelP1P2() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "point p1", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "p1.x", "propertyValue": String(p1.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p1.y", "propertyValue": String(p1.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p1.z", "propertyValue": String(p1.z), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "point p2", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "p2.x", "propertyValue": String(p2.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p2.y", "propertyValue": String(p2.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p2.z", "propertyValue": String(p2.z), "propertyIsTitle": false})
    }

    function setModelP1DistAngle() {
        lineEd.myModel.clear()

        lineEd.myModel.append({"propertyName": "point p", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "p.x", "propertyValue": String(p.x), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p.y", "propertyValue": String(p.y), "propertyIsTitle": false})
        lineEd.myModel.append({"propertyName": "p.z", "propertyValue": String(p.z), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "Lenght", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "lenght", "propertyValue": String(lenght), "propertyIsTitle": false})

        lineEd.myModel.append({"propertyName": "Angle", "propertyValue": "", "propertyIsTitle": true})
        lineEd.myModel.append({"propertyName": "angle", "propertyValue": String(angle), "propertyIsTitle": false})
    }

    function getP1P2Values() {
        if (lineEd.myModel.count !== 8 ) {
            console.log("line model Error!!!")
            return;
        }

        p1.x = parseFloat(lineEd.myModel.get(1).propertyValue)
        p1.y = parseFloat(lineEd.myModel.get(2).propertyValue)
        p1.z = parseFloat(lineEd.myModel.get(3).propertyValue)

        p2.x = parseFloat(lineEd.myModel.get(5).propertyValue)
        p2.y = parseFloat(lineEd.myModel.get(6).propertyValue)
        p2.z = parseFloat(lineEd.myModel.get(7).propertyValue)

    }

    function getPDistAngleValues() {
        if (lineEd.myModel.count !== 8 ) {
            console.log("line model Error!!!")
            return;
        }

        p.x = parseFloat(lineEd.myModel.get(1).propertyValue)
        p.y = parseFloat(lineEd.myModel.get(2).propertyValue)
        p.z = parseFloat(lineEd.myModel.get(3).propertyValue)

        lenght = parseFloat(lineEd.myModel.get(5).propertyValue)

        angle = parseFloat(lineEd.myModel.get(7).propertyValue)

        paux.x = p.x + Math.cos(angle * degreeRadGrad) * lenght
        paux.y = p.y + Math.sin(angle * degreeRadGrad) * lenght
        paux.z = p.z
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onOk: {
            if (btnP1P2.checked)
                getP1P2Values()
            if (btnP1DistAngle.checked)
                getPDistAngleValues()
            root.ok()
        }
        onCancel: root.cancel()
    }
}
