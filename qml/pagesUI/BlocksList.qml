import QtQuick 2.0

Rectangle {
    color: colorsUI.currentStyle.mainColor

    border.width: colorsUI.currentStyle.width

    Text {
        id: blockName
        text: qsTr("Block name")
    }
}
