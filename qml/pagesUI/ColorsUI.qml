import QtQuick 2.0

Item {
    // current style
    property StyleUI currentStyle: defaultStyle
    //colors
    property color mainColor: currentStyle.mainColor
    property color frameBorderColor: currentStyle.frameBorderColor
    property color textDefault: currentStyle.textDefault

    StyleUI {
        id: defaultStyle
        mainColor: "AliceBlue"
        frameBorderColor: "#0ea1ee" //"blue"

        //button
        btnBkgColor: "white"
        btnBkgColorPressed: "gold"
        btnBorderColor: "#0ea1ee"// "grey"
    }

}
