import QtQuick 2.0
import QtQuick3D
import "../qmlEntities"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: colorsUI.currentStyle.mainColor

    property int currentIndex: 0
    property string selectedColor: "Cyan"
    property string notSelectedColor: "black"

    property real currentLineThick: lineThickEditorModel.get(currentIndex).lineThickValue+1


    ListView {
        id: lineThickEditorView
        //width: parent.width
        anchors.top: parent.top
        anchors.bottom: btnBox.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: defaultMargins
        delegate: layerDelegate
        model: lineThickEditorModel
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }

        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
    }



    Component {
        id: layerDelegate
        Row {
            //width: root.width

            Rectangle {
                id: delegateName
                width: mainRoot.width*0.25 - defaultMargins
                height: btnSize
                border.color: "black" //index === currentIndex ? selectedColor : notSelectedColor
                color: index === currentIndex ? selectedColor : "white"
                //color: index === currentIndex ? listBkgColorSelect : listBkgColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    //onDoubleClicked: currentLayer = index
                }
                Text {
                    anchors.centerIn: parent
                    text: lineThickName
                }
            }

            Rectangle {
                id: delegateCurrentLayer
                width: mainRoot.width*0.75 - defaultMargins
                height: btnSize
                border.color: "black"
                //color: index === currentIndex ? listBkgColorSelect : listBkgColor

                View3D {
                    id: view
                    visible: true
                    anchors.fill: parent
                    anchors.margins: 4//defaultMargins
                    //renderMode: View3D.Underlay

                    environment: SceneEnvironment {
                        backgroundMode: SceneEnvironment.Color
                        clearColor: index === currentIndex ? selectedColor : "white"
                        //antialiasingMode: SceneEnvironment.MSAA
                        //antialiasingQuality: SceneEnvironment.High
                        antialiasingQuality: SceneEnvironment.VeryHigh
                    } //environment: SceneEnvironment

                    camera: OrthographicCamera{
                        id: mainCamera
                        z: 20


                    }//OrthographicCamera
                    Node {
                        LineThick {
                            id: lineTemplate
                            p0: Qt.vector3d(-1000, 0, 0)
                            p1: Qt.vector3d(1000, 0, 0)
                            thick: lineThickValue+1
                            color1: "black"
                            color2: "black"
                            //zoomScale: 10 * mainCamera.zoomCamera
                            //mousePosition: mouseWorldPosition // triggers picking
                        }
                    }//Node

                }//View3D
                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    //onDoubleClicked: currentLayer = index
                }
            }//Rectangle
        }//Row
    }//Component


    ListModel {
        id: lineThickEditorModel

        ListElement {
            lineThickName: "ByLayer"
            lineThickValue: 0
        }

        ListElement {
            lineThickName: "0 mm"
            lineThickValue: 0
        }

        ListElement {
            lineThickName: "1 mm"
            lineThickValue: 1
        }

        ListElement {
            lineThickName: "2 mm"
            lineThickValue: 2
        }

        ListElement {
            lineThickName: "3 mm"
            lineThickValue: 3
        }

        ListElement {
            lineThickName: "4 mm"
            lineThickValue: 4
        }

        ListElement {
            lineThickName: "5 mm"
            lineThickValue: 5
        }

        ListElement {
            lineThickName: "6 mm"
            lineThickValue: 6
        }
    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onCancel: root.cancel()
        onOk: root.ok()
    }
}
