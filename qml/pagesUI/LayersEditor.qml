import QtQuick 2.4
import QtQml.Models 2.2
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: colorsUI.currentStyle.mainColor

    property string iconCurrentLayer: "selected.png"
    property string iconNormalLayer: "layerIcon.png"

    property int currentIndex: 0
    property int colorIndex: 0
    property color listBkgColor: "AliceBlue"
    property color listBkgColorSelect: "Cyan"

    property int currentLayer: 0

    property color currentLayerColor: "white"
    property bool currentLayerVisible: true

    onCurrentLayerChanged: {
        currentLayerColor = layersModel.get(currentLayer).layerColor
    }

    signal ok()
    signal cancel()

    ButtonHybrid {
        id: btnNew
        width: btn2Size//mainRoot.width/rowsCount
        height: btnSize
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        text: qsTr("New")
        onButtonClick: {
            layersModel.append({"layerName": "layer"+layersModel.count,
                                   "layerVisible": 1,
                                   "layerColor": "white",
                                   "locked": 0,
                                   "printable": 1})
        }
    }

    ButtonHybrid {
        id: btnSetCurrent
        width: btn2Size//mainRoot.width/rowsCount
        height: btnSize
        anchors.left: btnNew.right
        anchors.margins: defaultMargins
        anchors.verticalCenter: btnNew.verticalCenter
        text: qsTr("Current")
        onButtonClick: {
            currentLayer = currentIndex
        }
    }

    Text {
        id: textCurrentLayer
        anchors.left: btnSetCurrent.right
        anchors.leftMargin: defaultMargins
        anchors.verticalCenter: btnSetCurrent.verticalCenter
        text: qsTr("Current Layer: "+layersModel.get(currentLayer).layerName)
    }

    ListView {
        id: layersView
        //width: parent.width
        anchors.top: btnNew.bottom
        anchors.bottom: btnBox.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: defaultMargins
        header: headerDelegate
        headerPositioning: ListView.OverlayHeader
        delegate: layerDelegate
        model: layersModel
        highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        clip: true
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
    }

    Component.onCompleted: {
        //textCurrentLayer.text = Qt.binding(function(){ return  qsTr("Current Layer: "+layersModel.get(currentLayer).layerName) })
    }

    Component {
        id: headerDelegate
        Row {
            //width: root.width
            z: 2
            Rectangle {
                id: headerLayer
                width: mainRoot.width/(rowsCount*3); height: btnSize
                border.color: "black"
                color: listBkgColor
            }
            Rectangle {
                id: delegateName
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: listBkgColor
                Text {
                    anchors.centerIn: parent
                    text: "layerName"
                }
            }
            Rectangle {
                id: delegateVisible
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: listBkgColor
                Text {
                    id: textLayerVisibleOnOff
                    anchors.centerIn: parent
                    text: qsTr("Visibility")
                }
            }
            Rectangle {
                id: delegateColor
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: listBkgColor
                Text {
                    anchors.centerIn: parent
                    text: qsTr("Color")
                }
            }
            Rectangle {
                id: delegateLocked
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: listBkgColor
                Text {
                    anchors.centerIn: parent
                    text: qsTr("Locked")
                }
            }
            Rectangle {
                id: delegatePrintable
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: listBkgColor
                Text {
                    anchors.centerIn: parent
                    text: qsTr("Printable")
                }
            }
        }//Row
    }//headerDelegate
    //---------------------------------------------------------------------------------

    property int rowsCount: 6
    Component {
        id: layerDelegate
        Row {
            //width: root.width

            Rectangle {
                id: delegateCurrentLayer
                width: mainRoot.width/(rowsCount*3); height: btnSize
                border.color: "black"
                color: index === currentIndex ? listBkgColorSelect : listBkgColor
                Image {
                    id: visible
                    anchors.fill: parent
                    anchors.centerIn: parent
                    fillMode: Image.PreserveAspectFit
                    mipmap: true
                    source: index === currentLayer ? "qrc:/images/" + iconCurrentLayer : ""
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    onDoubleClicked: currentLayer = index
                }
            }


            Rectangle {
                id: delegateName
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: index === currentIndex ? listBkgColorSelect : listBkgColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    onDoubleClicked: currentLayer = index
                }
                Text {
                    anchors.centerIn: parent
                    text: layerName
                }
            }
            Rectangle {
                id: delegateVisible
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: index === currentIndex ? listBkgColorSelect : listBkgColor
                /*Image {
                    id: visible
                    anchors.fill: parent
                    anchors.centerIn: parent
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/" + iconName
                }*/
                Text {
                    id: textLayerVisibleOnOff
                    anchors.centerIn: parent
                    text: qsTr("Visible On")
                    //horizontalAlignment: Text.AlignHCenter
                    //verticalAlignment: Text.AlignVCenter
                }
                MouseArea {
                    anchors.fill: parent
                    //onClicked: iconName = iconName === "Visible.png"  ? "Invisible.png" : "Visible.png"
                    onClicked: {
                        textLayerVisibleOnOff.text =  textLayerVisibleOnOff.text === qsTr("Visible On") ? qsTr("Visible Off") : qsTr("Visible On")
                        currentLayerVisible = !layersModel.get(currentIndex).layerVisible
                        layersModel.setProperty(currentIndex, "layerVisible", currentLayerVisible)
                    }
                }

            }
            Rectangle {
                id: delegateColor
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: layerColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        colorIndex = index
                        colorPicker.visible = true
                    }
                }
            }

            Rectangle {
                id: delegateLocked
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: index === currentIndex ? listBkgColorSelect : listBkgColor
                Text {
                    id: textLayerLockedOnOff
                    anchors.centerIn: parent
                    text: qsTr("Locked On")
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: textLayerLockedOnOff.text =  textLayerLockedOnOff.text === qsTr("Locked On") ? qsTr("Locked Off") : qsTr("Locked On")
                }
            }

            Rectangle {
                id: delegatePrintable
                width: mainRoot.width/rowsCount; height: btnSize
                border.color: "black"
                color: index === currentIndex ? listBkgColorSelect : listBkgColor
                Text {
                    id: textLayerPrintableOnOff
                    anchors.centerIn: parent
                    text: qsTr("Print On")
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: textLayerPrintableOnOff.text =  textLayerPrintableOnOff.text === qsTr("Print On") ? qsTr("Print Off") : qsTr("Print On")
                }
            }

        }//Row
    }//Component

    property alias layersModel: layersModel

    ListModel {
        id: layersModel
        //dynamicRoles: true
        ListElement {
            layerName: "0"
            layerVisible: true
            layerColor: "white"
        }
        /*ListElement {
            layerName: "0"
            layerVisible: 1
            layerColor: "red"
            locked: 0
            printable: 1
        }*/
        /*({"layerName": "layer"+layersModel.count,
                                           "layerVisible": 1,
                                           "layerColor": "red",
                                           "locked": 0,
                                           "printable": 1})*/

    }

    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onCancel: root.cancel()
        onOk: root.ok()
    }

    ColorPicker {
        id: colorPicker
        anchors.fill: parent
        visible: false
        onCancel: visible = false
        onOk: {
            layersModel.setProperty(colorIndex, "layerColor", colorPicker.selectedColor)
            visible = false
            if (colorIndex === currentLayer)
                currentLayerColor = layersModel.get(currentLayer).layerColor
        }
    }


}
