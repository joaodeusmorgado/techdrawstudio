import QtQuick 2.0
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: colorsUI.mainColor

    property string selectedColor: m_colorSelected.color
    property string selectedColorName: currentIndex === 0 ? "ByLayer" : colorName
    property string colorName: "ByLayer"

    property bool isByLayerColor: currentIndex === 0 ? true : false

    property int currentIndex: 0

    signal ok()
    signal cancel()

    GridView {
        id: gridColors
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: btnOk.top
        anchors.margins: defaultMargins
        anchors.bottomMargin: defaultMargins * 4
        cellWidth: btnSize + defaultMargins / 2
        cellHeight: btnSize + defaultMargins / 2
        delegate: colorsDelegate
        model: mymodel
        cacheBuffer: 10
    }

    Component {
        id: colorsDelegate
        ButtonColor {
            color: modelColor
            m_text: modelText
            onButtonClick: {
                currentIndex = index
                m_colorSelected.color = color
                //colorName = modelColor
                colorName = index-1

            }
        }
    }

    onCurrentIndexChanged: console.log("ColorPicker color index: " + currentIndex + "-------------------------------------------")


    property alias modelColors: mymodel

    ListModel {
        id: mymodel
        ListElement { modelColor: "#00FF00"; modelText:"By\nLayer" }
        ListElement { modelColor: "#000000"; modelText:"0" }
        //ListElement { modelColor: "#FF0000"; modelText:"1" }
        ListElement { modelColor: "red"; modelText:"1" }
        ListElement { modelColor: "#FFFF00"; modelText:"2" }
        ListElement { modelColor: "#00FF00"; modelText:"3" }
        ListElement { modelColor: "#00FFFF"; modelText:"4" }
        ListElement { modelColor: "#0000FF"; modelText:"5" }
        ListElement { modelColor: "#FF00FF"; modelText:"6" }
        ListElement { modelColor: "#FFFFFF"; modelText:"7" }
        ListElement { modelColor: "#414141"; modelText:"8" }
        ListElement { modelColor: "#808080"; modelText:"9" }
        ListElement { modelColor: "#FF0000"; modelText:"10" }
        ListElement { modelColor: "#FFAAAA"; modelText:"11" }
        ListElement { modelColor: "#BD0000"; modelText:"12" }
        ListElement { modelColor: "#BD7E7E"; modelText:"13" }
        ListElement { modelColor: "#810000"; modelText:"14" }
        ListElement { modelColor: "#815656"; modelText:"15" }
        ListElement { modelColor: "#680000"; modelText:"16" }
        ListElement { modelColor: "#684545"; modelText:"17" }
        ListElement { modelColor: "#4F0000"; modelText:"18" }
        ListElement { modelColor: "#4F3535"; modelText:"19" }
        ListElement { modelColor: "#FF3F00"; modelText:"20" }
        ListElement { modelColor: "#FFBFAA"; modelText:"21" }
        ListElement { modelColor: "#BD2E00"; modelText:"22" }
        ListElement { modelColor: "#BD8D7E"; modelText:"23" }
        ListElement { modelColor: "#811F00"; modelText:"24" }
        ListElement { modelColor: "#816056"; modelText:"25" }
        ListElement { modelColor: "#681900"; modelText:"26" }
        ListElement { modelColor: "#684E45"; modelText:"27" }
        ListElement { modelColor: "#4F1300"; modelText:"28" }
        ListElement { modelColor: "#4F3B35"; modelText:"29" }
        ListElement { modelColor: "#FF7F00"; modelText:"30" }
        ListElement { modelColor: "#FFD4AA"; modelText:"31" }
        ListElement { modelColor: "#BD5E00"; modelText:"32" }
        ListElement { modelColor: "#BD9D7E"; modelText:"33" }
        ListElement { modelColor: "#814000"; modelText:"34" }
        ListElement { modelColor: "#816B56"; modelText:"35" }
        ListElement { modelColor: "#683400"; modelText:"36" }
        ListElement { modelColor: "#685645"; modelText:"37" }
        ListElement { modelColor: "#4F2700"; modelText:"38" }
        ListElement { modelColor: "#4F4235"; modelText:"39" }
        ListElement { modelColor: "#FFBF00"; modelText:"40" }
        ListElement { modelColor: "#FFEAAA"; modelText:"41" }
        ListElement { modelColor: "#BD8D00"; modelText:"42" }
        ListElement { modelColor: "#BDAD7E"; modelText:"43" }
        ListElement { modelColor: "#816000"; modelText:"44" }
        ListElement { modelColor: "#817656"; modelText:"45" }
        ListElement { modelColor: "#684E00"; modelText:"46" }
        ListElement { modelColor: "#685F45"; modelText:"47" }
        ListElement { modelColor: "#4F3B00"; modelText:"48" }
        ListElement { modelColor: "#4F4935"; modelText:"49" }
        ListElement { modelColor: "#FFFF00"; modelText:"50" }
        ListElement { modelColor: "#FFFFAA"; modelText:"51" }
        ListElement { modelColor: "#BDBD00"; modelText:"52" }
        ListElement { modelColor: "#BDBD7E"; modelText:"53" }
        ListElement { modelColor: "#818100"; modelText:"54" }
        ListElement { modelColor: "#818156"; modelText:"55" }
        ListElement { modelColor: "#686800"; modelText:"56" }
        ListElement { modelColor: "#686845"; modelText:"57" }
        ListElement { modelColor: "#4F4F00"; modelText:"58" }
        ListElement { modelColor: "#4F4F35"; modelText:"59" }
        ListElement { modelColor: "#BFFF00"; modelText:"60" }
        ListElement { modelColor: "#EAFFAA"; modelText:"61" }
        ListElement { modelColor: "#8DBD00"; modelText:"62" }
        ListElement { modelColor: "#ADBD7E"; modelText:"63" }
        ListElement { modelColor: "#608100"; modelText:"64" }
        ListElement { modelColor: "#768156"; modelText:"65" }
        ListElement { modelColor: "#4E6800"; modelText:"66" }
        ListElement { modelColor: "#5F6845"; modelText:"67" }
        ListElement { modelColor: "#3B4F00"; modelText:"68" }
        ListElement { modelColor: "#494F35"; modelText:"69" }
        ListElement { modelColor: "#7FFF00"; modelText:"70" }
        ListElement { modelColor: "#D4FFAA"; modelText:"71" }
        ListElement { modelColor: "#5EBD00"; modelText:"72" }
        ListElement { modelColor: "#9DBD7E"; modelText:"73" }
        ListElement { modelColor: "#408100"; modelText:"74" }
        ListElement { modelColor: "#6B8156"; modelText:"75" }
        ListElement { modelColor: "#346800"; modelText:"76" }
        ListElement { modelColor: "#566845"; modelText:"77" }
        ListElement { modelColor: "#274F00"; modelText:"78" }
        ListElement { modelColor: "#424F35"; modelText:"79" }
        ListElement { modelColor: "#3FFF00"; modelText:"80" }
        ListElement { modelColor: "#BFFFAA"; modelText:"81" }
        ListElement { modelColor: "#2EBD00"; modelText:"82" }
        ListElement { modelColor: "#8DBD7E"; modelText:"83" }
        ListElement { modelColor: "#1F8100"; modelText:"84" }
        ListElement { modelColor: "#608156"; modelText:"85" }
        ListElement { modelColor: "#196800"; modelText:"86" }
        ListElement { modelColor: "#4E6845"; modelText:"87" }
        ListElement { modelColor: "#134F00"; modelText:"88" }
        ListElement { modelColor: "#3B4F35"; modelText:"89" }
        ListElement { modelColor: "#00FF00"; modelText:"90" }
        ListElement { modelColor: "#AAFFAA"; modelText:"91" }
        ListElement { modelColor: "#00BD00"; modelText:"92" }
        ListElement { modelColor: "#7EBD7E"; modelText:"93" }
        ListElement { modelColor: "#008100"; modelText:"94" }
        ListElement { modelColor: "#568156"; modelText:"95" }
        ListElement { modelColor: "#006800"; modelText:"96" }
        ListElement { modelColor: "#456845"; modelText:"97" }
        ListElement { modelColor: "#004F00"; modelText:"98" }
        ListElement { modelColor: "#354F35"; modelText:"99" }
        ListElement { modelColor: "#00FF3F"; modelText:"100" }
        ListElement { modelColor: "#AAFFBF"; modelText:"101" }
        ListElement { modelColor: "#00BD2E"; modelText:"102" }
        ListElement { modelColor: "#7EBD8D"; modelText:"103" }
        ListElement { modelColor: "#00811F"; modelText:"104" }
        ListElement { modelColor: "#568160"; modelText:"105" }
        ListElement { modelColor: "#006819"; modelText:"106" }
        ListElement { modelColor: "#45684E"; modelText:"107" }
        ListElement { modelColor: "#004F13"; modelText:"108" }
        ListElement { modelColor: "#354F3B"; modelText:"109" }
        ListElement { modelColor: "#00FF7F"; modelText:"110" }
        ListElement { modelColor: "#AAFFD4"; modelText:"111" }
        ListElement { modelColor: "#00BD5E"; modelText:"112" }
        ListElement { modelColor: "#7EBD9D"; modelText:"113" }
        ListElement { modelColor: "#008140"; modelText:"114" }
        ListElement { modelColor: "#56816B"; modelText:"115" }
        ListElement { modelColor: "#006834"; modelText:"116" }
        ListElement { modelColor: "#456856"; modelText:"117" }
        ListElement { modelColor: "#004F27"; modelText:"118" }
        ListElement { modelColor: "#354F42"; modelText:"119" }
        ListElement { modelColor: "#00FFBF"; modelText:"120" }
        ListElement { modelColor: "#AAFFEA"; modelText:"121" }
        ListElement { modelColor: "#00BD8D"; modelText:"122" }
        ListElement { modelColor: "#7EBDAD"; modelText:"123" }
        ListElement { modelColor: "#008160"; modelText:"124" }
        ListElement { modelColor: "#568176"; modelText:"125" }
        ListElement { modelColor: "#00684E"; modelText:"126" }
        ListElement { modelColor: "#45685F"; modelText:"127" }
        ListElement { modelColor: "#004F3B"; modelText:"128" }
        ListElement { modelColor: "#354F49"; modelText:"129" }
        ListElement { modelColor: "#00FFFF"; modelText:"130" }
        ListElement { modelColor: "#AAFFFF"; modelText:"131" }
        ListElement { modelColor: "#00BDBD"; modelText:"132" }
        ListElement { modelColor: "#7EBDBD"; modelText:"133" }
        ListElement { modelColor: "#008181"; modelText:"134" }
        ListElement { modelColor: "#568181"; modelText:"135" }
        ListElement { modelColor: "#006868"; modelText:"136" }
        ListElement { modelColor: "#456868"; modelText:"137" }
        ListElement { modelColor: "#004F4F"; modelText:"138" }
        ListElement { modelColor: "#354F4F"; modelText:"139" }
        ListElement { modelColor: "#00BFFF"; modelText:"140" }
        ListElement { modelColor: "#AAEAFF"; modelText:"141" }
        ListElement { modelColor: "#008DBD"; modelText:"142" }
        ListElement { modelColor: "#7EADBD"; modelText:"143" }
        ListElement { modelColor: "#006081"; modelText:"144" }
        ListElement { modelColor: "#567681"; modelText:"145" }
        ListElement { modelColor: "#004E68"; modelText:"146" }
        ListElement { modelColor: "#455F68"; modelText:"147" }
        ListElement { modelColor: "#003B4F"; modelText:"148" }
        ListElement { modelColor: "#35494F"; modelText:"149" }
        ListElement { modelColor: "#007FFF"; modelText:"150" }
        ListElement { modelColor: "#AAD4FF"; modelText:"151" }
        ListElement { modelColor: "#005EBD"; modelText:"152" }
        ListElement { modelColor: "#7E9DBD"; modelText:"153" }
        ListElement { modelColor: "#004081"; modelText:"154" }
        ListElement { modelColor: "#566B81"; modelText:"155" }
        ListElement { modelColor: "#003468"; modelText:"156" }
        ListElement { modelColor: "#455668"; modelText:"157" }
        ListElement { modelColor: "#00274F"; modelText:"158" }
        ListElement { modelColor: "#35424F"; modelText:"159" }
        ListElement { modelColor: "#003FFF"; modelText:"160" }
        ListElement { modelColor: "#AABFFF"; modelText:"161" }
        ListElement { modelColor: "#002EBD"; modelText:"162" }
        ListElement { modelColor: "#7E8DBD"; modelText:"163" }
        ListElement { modelColor: "#001F81"; modelText:"164" }
        ListElement { modelColor: "#566081"; modelText:"165" }
        ListElement { modelColor: "#001968"; modelText:"166" }
        ListElement { modelColor: "#454E68"; modelText:"167" }
        ListElement { modelColor: "#00134F"; modelText:"168" }
        ListElement { modelColor: "#353B4F"; modelText:"169" }
        ListElement { modelColor: "#0000FF"; modelText:"170" }
        ListElement { modelColor: "#AAAAFF"; modelText:"171" }
        ListElement { modelColor: "#0000BD"; modelText:"172" }
        ListElement { modelColor: "#7E7EBD"; modelText:"173" }
        ListElement { modelColor: "#000081"; modelText:"174" }
        ListElement { modelColor: "#565681"; modelText:"175" }
        ListElement { modelColor: "#000068"; modelText:"176" }
        ListElement { modelColor: "#454568"; modelText:"177" }
        ListElement { modelColor: "#00004F"; modelText:"178" }
        ListElement { modelColor: "#35354F"; modelText:"179" }
        ListElement { modelColor: "#3F00FF"; modelText:"180" }
        ListElement { modelColor: "#BFAAFF"; modelText:"181" }
        ListElement { modelColor: "#2E00BD"; modelText:"182" }
        ListElement { modelColor: "#8D7EBD"; modelText:"183" }
        ListElement { modelColor: "#1F0081"; modelText:"184" }
        ListElement { modelColor: "#605681"; modelText:"185" }
        ListElement { modelColor: "#190068"; modelText:"186" }
        ListElement { modelColor: "#4E4568"; modelText:"187" }
        ListElement { modelColor: "#13004F"; modelText:"188" }
        ListElement { modelColor: "#3B354F"; modelText:"189" }
        ListElement { modelColor: "#7F00FF"; modelText:"190" }
        ListElement { modelColor: "#D4AAFF"; modelText:"191" }
        ListElement { modelColor: "#5E00BD"; modelText:"192" }
        ListElement { modelColor: "#9D7EBD"; modelText:"193" }
        ListElement { modelColor: "#400081"; modelText:"194" }
        ListElement { modelColor: "#6B5681"; modelText:"195" }
        ListElement { modelColor: "#340068"; modelText:"196" }
        ListElement { modelColor: "#564568"; modelText:"197" }
        ListElement { modelColor: "#27004F"; modelText:"198" }
        ListElement { modelColor: "#42354F"; modelText:"199" }
        ListElement { modelColor: "#BF00FF"; modelText:"200" }
        ListElement { modelColor: "#EAAAFF"; modelText:"201" }
        ListElement { modelColor: "#8D00BD"; modelText:"202" }
        ListElement { modelColor: "#AD7EBD"; modelText:"203" }
        ListElement { modelColor: "#600081"; modelText:"204" }
        ListElement { modelColor: "#765681"; modelText:"205" }
        ListElement { modelColor: "#4E0068"; modelText:"206" }
        ListElement { modelColor: "#5F4568"; modelText:"207" }
        ListElement { modelColor: "#3B004F"; modelText:"208" }
        ListElement { modelColor: "#49354F"; modelText:"209" }
        ListElement { modelColor: "#FF00FF"; modelText:"210" }
        ListElement { modelColor: "#FFAAFF"; modelText:"211" }
        ListElement { modelColor: "#BD00BD"; modelText:"212" }
        ListElement { modelColor: "#BD7EBD"; modelText:"213" }
        ListElement { modelColor: "#810081"; modelText:"214" }
        ListElement { modelColor: "#815681"; modelText:"215" }
        ListElement { modelColor: "#680068"; modelText:"216" }
        ListElement { modelColor: "#684568"; modelText:"217" }
        ListElement { modelColor: "#4F004F"; modelText:"218" }
        ListElement { modelColor: "#4F354F"; modelText:"219" }
        ListElement { modelColor: "#FF00BF"; modelText:"220" }
        ListElement { modelColor: "#FFAAEA"; modelText:"221" }
        ListElement { modelColor: "#BD008D"; modelText:"222" }
        ListElement { modelColor: "#BD7EAD"; modelText:"223" }
        ListElement { modelColor: "#810060"; modelText:"224" }
        ListElement { modelColor: "#815676"; modelText:"225" }
        ListElement { modelColor: "#68004E"; modelText:"226" }
        ListElement { modelColor: "#68455F"; modelText:"227" }
        ListElement { modelColor: "#4F003B"; modelText:"228" }
        ListElement { modelColor: "#4F3549"; modelText:"229" }
        ListElement { modelColor: "#FF007F"; modelText:"230" }
        ListElement { modelColor: "#FFAAD4"; modelText:"231" }
        ListElement { modelColor: "#BD005E"; modelText:"232" }
        ListElement { modelColor: "#BD7E9D"; modelText:"233" }
        ListElement { modelColor: "#810040"; modelText:"234" }
        ListElement { modelColor: "#81566B"; modelText:"235" }
        ListElement { modelColor: "#680034"; modelText:"236" }
        ListElement { modelColor: "#684556"; modelText:"237" }
        ListElement { modelColor: "#4F0027"; modelText:"238" }
        ListElement { modelColor: "#4F3542"; modelText:"239" }
        ListElement { modelColor: "#FF003F"; modelText:"240" }
        ListElement { modelColor: "#FFAABF"; modelText:"241" }
        ListElement { modelColor: "#BD002E"; modelText:"242" }
        ListElement { modelColor: "#BD7E8D"; modelText:"243" }
        ListElement { modelColor: "#81001F"; modelText:"244" }
        ListElement { modelColor: "#815660"; modelText:"245" }
        ListElement { modelColor: "#680019"; modelText:"246" }
        ListElement { modelColor: "#68454E"; modelText:"247" }
        ListElement { modelColor: "#4F0013"; modelText:"248" }
        ListElement { modelColor: "#4F353B"; modelText:"249" }
        ListElement { modelColor: "#333333"; modelText:"250" }
        ListElement { modelColor: "#505050"; modelText:"251" }
        ListElement { modelColor: "#696969"; modelText:"252" }
        ListElement { modelColor: "#828282"; modelText:"253" }
        ListElement { modelColor: "#BEBEBE"; modelText:"254" }
        ListElement { modelColor: "#FFFFFF"; modelText:"255" }
    }


    ButtonHybrid {
        id: btnOk
        text: qsTr("Ok")
        width: btn2Size
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onButtonClick: {
            root.visible = false
            ok()
        }
    }

    ButtonHybrid {
        id: btnCancel
        text: qsTr("Cancel")
        width: btn2Size
        anchors.right: btnOk.left
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onButtonClick: {
            root.visible = false
            cancel()
        }
    }

    Rectangle {
        id: m_colorSelected
        width: btn2Size
        height: btnSize
        radius: btnSize
        anchors.bottom: parent.bottom
        anchors.right: btnCancel.left
        anchors.margins: defaultMargins
        color: "red"
    }

}
