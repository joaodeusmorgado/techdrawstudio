﻿import QtQuick 2.0
import QtQuick3D
import "../qmlEntities"
import "../components"

Rectangle {
    id: root
    width: mainRoot.width
    height: mainRoot.height
    color: colorsUI.currentStyle.mainColor

    property int currentIndex: 0
    property string selectedColor: "Cyan"
    property string notSelectedColor: "black"

    property alias layerModel: layerModel

    //property real currentLineThick: layerModel.get(currentIndex).lineThickValue+1


    ListView {
        id: lineThickEditorView
        //width: parent.width
        anchors.top: parent.top
        anchors.bottom: btnBox.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: defaultMargins
        delegate: layerDelegate
        model: layerModel
        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }

        //highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
    }



    Component {
        id: layerDelegate
        Row {
            //width: root.width

            Rectangle {
                id: delegateName
                width: mainRoot.width*0.25 - defaultMargins
                height: btnSize
                border.color: "black" //index === currentIndex ? selectedColor : notSelectedColor
                color: index === currentIndex ? selectedColor : "white"
                //color: index === currentIndex ? listBkgColorSelect : listBkgColor
                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    //onDoubleClicked: currentLayer = index
                }
                Text {
                    anchors.centerIn: parent
                    text: layerName
                }
            }

            Rectangle {
                id: delegateCurrentLayer
                width: mainRoot.width*0.75 - defaultMargins
                height: btnSize
                border.color: "black"
                //color: index === currentIndex ? listBkgColorSelect : listBkgColor

                MouseArea {
                    anchors.fill: parent
                    onClicked: currentIndex = index
                    //onDoubleClicked: currentLayer = index
                }
            }//Rectangle
        }//Row
    }//Component


    ListModel {
        id: layerModel

        ListElement {
            layerName: "0"
            lineThickValue: 0
        }

        ListElement {
            layerName: "layer1"
            lineThickValue: 1
        }

        ListElement {
            layerName: "layer2"
            lineThickValue: 2
        }


    }

    signal ok()
    signal cancel()
    ButtonBox {
        id: btnBox
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        onCancel: root.cancel()
        onOk: root.ok()
    }
}
