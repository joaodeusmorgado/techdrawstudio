import QtQuick 2.15
import "../components"

Rectangle {

    color: colorsUI.currentStyle.mainColor

    signal btnOk()
    signal btnCancel()

    Flickable {
        id: m_verticalFlickable

        anchors.top: parent.top
        anchors.left: parent.left

        width: parent.width
        height: parent.height - okCancel.height
        contentWidth: parent.width
        contentHeight: colSettingsItems.height + 1 * defaultMargins
        clip: true

        Column {
            id: colSettingsItems
            width: parent.width - 2*defaultMargins
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: defaultMargins

            spacing: 2 * mm


        }//Column


    } //Flickable


    ButtonBox {
        id: okCancel
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        showBtnCancel: false
        onOk: parent.btnOk()
        onCancel: parent.btnCancel()
    }


}
