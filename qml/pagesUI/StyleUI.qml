import QtQuick 2.0

Item {
    //colors
    property color mainColor: "black"
    property color frameBorderColor: "blue"
    property color textDefault: "black"

    //button
    property color btnBkgColor: "white"
    property color btnBkgColorPressed: "gold"
    property color btnBorderColor: "grey"
}
