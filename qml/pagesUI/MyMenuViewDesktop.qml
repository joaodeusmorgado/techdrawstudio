import QtQuick 2.15

Window {
    id: winSettingsDesktop

    width: listEditorWidth //mainRoot.width *0.2
    height: listEditorHeight //mainRoot.height *0.5

    MyMenuView {
        anchors.fill: parent
        onBtnOk: winSettingsDesktop.close()
    }
}
