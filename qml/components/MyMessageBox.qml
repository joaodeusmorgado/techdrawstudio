import QtQuick 2.3

Rectangle {
    id: messageBox
    width: mainRoot.width * 0.5
    height: mainRoot.height * 0.25
    color: colorsUI.currentStyle.mainColor

    border.color: "grey"//"white"
    border.width: btnSize * 0.1
    radius: btnSize * 0.4


    property alias text: tex.text

    Text {
        id: tex
        width: parent.width * 0.9
        anchors.top: parent.top
        anchors.margins: defaultMargins
        horizontalAlignment: Text.AlignHCenter //Text.AlignJustify
        text: qsTr("message box!!!")
    }

    signal btnOk()
    signal btnCancel()

    ButtonHybrid {
        text: qsTr("Ok")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        onButtonClick: btnOk()
    }

}
