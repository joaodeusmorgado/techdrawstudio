import QtQuick 2.0

Item {
    width: btn2Size * 2 + defaultMargins * 3
    height: btnSize + defaultMargins * 2

    property alias btnOkLabel: btnOK.text
    property alias btnCancelLabel: btnCancel.text
    property bool showBtnOk: true
    property bool showBtnCancel: true

    signal ok()
    signal cancel()

    ButtonHybrid {
        id: btnOK
        visible: showBtnOk
        width: btn2Size
        text: qsTr("Ok")
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.bottomMargin: defaultMargins
        anchors.rightMargin: defaultMargins
        onButtonClick: ok()
    }

    ButtonHybrid {
        id: btnCancel
        visible: showBtnCancel
        width: btn2Size
        text: qsTr("Cancel")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: defaultMargins
        anchors.right: btnOK.left
        anchors.rightMargin: defaultMargins
        onButtonClick: cancel()
    }


}
