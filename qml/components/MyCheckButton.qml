import QtQuick 2.3

Rectangle {
    width: 18 * mm
    height: 5 * mm

    color: colorsUI.mainColor

    property color textColor: "black" //colors.textDefault
    property bool checked: false
    property alias label: label.text

    signal clickCheckBtn

    Rectangle {
        id: checkBtn
        width: parent.height
        height: parent.height
        color: "white"
        border.color: "lightgrey"
        radius: width * 0.3

        Rectangle {
            width: parent.width * 0.6
            height: width
            radius: width * 0.3
            anchors.centerIn: parent
            color: checked ? "black" : "white"
        }
    }

    Text {
        id: label
        anchors.left: checkBtn.right
        anchors.leftMargin: defaultMargins
        anchors.verticalCenter: parent.verticalCenter
        text: ""
        color: textColor
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            checked = !checked
            clickCheckBtn()
        }
    }
}
