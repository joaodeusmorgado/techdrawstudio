import QtQuick 2.9
import QtQuick.Controls 2.2

Item {

    property alias myModel: _model
    property alias myView: _view

    ListView {
        id: _view
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: defaultMargins
        delegate: myDelegate
        model: _model
    }

    Component {
        id: myDelegate
        Row {
            property bool isTitle: propertyIsTitle
            Rectangle {
                id: delegateName
                width: isTitle ? _view.width : _view.width * 0.5
                height: 3 * mm //btnSize * 0.7
                border.color: "black"
                color: isTitle ? "lightgrey" : "white"

                Text {
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: defaultMargins
                    anchors.right: parent.right
                    verticalAlignment: Text.AlignVCenter
                    text: propertyName
                    font.bold: isTitle
                }
            }

            TextInputHy {
                width: isTitle ? 0 : _view.width * 0.5
                height: delegateName.height
                radius: 0
                text: propertyValue
                onTextEdited: function(tex) {
                    //console.log("tex: "+tex)
                    var aux = parseFloat(tex)
                    //console.log("aux: "+aux)
                    //_model.setProperty(index, "propertyValue", parseFloat(tex))
                    //_model.get(index).propertyValue = parseFloat(tex)
                    _model.get(index).propertyValue = tex

                    //lineEd.myModel.append({"propertyName": "point p1", "propertyValue": 0, "propertyIsTitle": true})

                }
            }
        }//Row
    }//Component

    ListModel {
        id: _model
        //dynamicRoles: true
    }

}
