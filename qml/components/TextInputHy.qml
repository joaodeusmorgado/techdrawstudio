import QtQuick 2.9

// Line edit
Rectangle {
    id: m_textEdit
    width: mainRoot.btnSize * 2
    height: mainRoot.btnSize
    radius: btnSize * 0.4
    border.color: colorsUI.currentStyle.btnBorderColor //"lightgrey"
    color: readOnly ? "grey" : "white"

    property alias readOnly: m_text.readOnly
    property alias text: m_text.text
    property alias accepInput: m_text.acceptableInput
    property alias hasFocus: m_text.cursorVisible
    property int _margins: 2+ radius

    signal textAccepted()
    signal textEdited(var tex)

    TextInput {
        id: m_text
        height: 30
        verticalAlignment: TextInput.AlignVCenter
        anchors.fill: parent
        anchors.leftMargin: _margins
        anchors.rightMargin: _margins
        selectByMouse : true
        text: ""
        onAccepted: textAccepted()
        clip: true
        onTextEdited: m_textEdit.textEdited(text)
    }

    function clear(){
        m_text.remove(m_text.text.length-1, m_text.text.length)
    }
}
