import QtQuick 2.3
import "../qmlEntities"

Rectangle {
    id:comboBox

    //radius: 1*mm
    radius: defaultMargins

    border.width: 3//1*mm
    border.color: "grey"

    property real itemHeight: 6*mm
    property string itemText
    property alias comboModel: list.model
    property int elementsCount//: comboModel.count

    signal comboClick()
    signal comboPress()

    ListView {
        id: list
        anchors.fill: parent
        anchors.margins: comboBox.border.width

        //model: myModel
        delegate:
            Item {
                width: parent.width
                height: itemHeight

                Text {
                    id: mytext
                    anchors.centerIn: parent
                    text: name
                }
                MouseArea {
                    anchors.fill: parent
                    onPressed: {

                        list.currentIndex = index
                        //console.log("MyComBox" + myModel.get(index).name)
                        itemText = myModel.get(index).name

                        comboPress()


                    }
                    onClicked: comboClick()
                }
        }
        highlight: Rectangle { color: "lightsteelblue"; radius: 1*mm }
        focus: true
        clip: true
    }

}

