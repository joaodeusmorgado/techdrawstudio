import QtQuick 2.3
import QtQuick.Controls
import "../qmlEnums"

Rectangle {
    id: textEditor
    width: parent.width
    height: parent.height * 0.15
 //   anchors.bottom: parent.bottom

    ScrollView {
        id: viewscroll
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: cmdLine.top



        TextArea {
            id: texArea
           // readOnly: true
            //width: parent.width
            anchors.fill: parent

           // KeyNavigation.priority: KeyNavigation.BeforeItem
            //KeyNavigation.tab: cmdLine
            Keys.onPressed: {
                cmdLine.focus = true
            }
        }
    }//ScrollView

    property alias cmdLine: cmdLine

    function checkCommands(str) {
        str = str.replace(" ", "")

        texArea.text += "\n"+str

        if (str === "l")
            activeCommand = Commands.Line

        if (str === "pl")
            activeCommand = Commands.Polyline

        if (str === "spl")
            activeCommand = Commands.Spline

        if (str === "ci")
            activeCommand = Commands.Circle

        if (str === "re")
            activeCommand = Commands.Rectangle

        if (str === "c")
            activeCommand = Commands.Copy

        if (str === "m")
            activeCommand = Commands.Move

        if (str === "m")
            activeCommand = Commands.Move

        if (str === "p")
            activeCommand = Commands.Pan

        if (str === "r")
            activeCommand = Commands.Rotate

        if (str === "f")
            activeCommand = Commands.Fillet

        if (str === "tr")
            activeCommand = Commands.Trim

        if (str === "ex")
            activeCommand = Commands.Extend

        if (str === "e")
            genericCommand(Commands.Delete)

        if (str === "u")
            genericCommand(Commands.UnPick)

        if (str === "len") {
            activeCommand = Commands.Length
            console.log("active command is Commands.Length")
            rootMainView.genericCommand(Commands.Length)
        }




        /*if () {

            if (activeCommand === Commands.Polyline) {
                if (polyLineTemplate.receiveEvent(EventsQML.ReleaseRightClick, getCursorPosWorld3DNode(), layer0) === EntitiesQML.Finish)
                    menuPolylineEnd.clearMenu()
            }
        }*/


        viewscroll.ScrollBar.vertical.position = 1-viewscroll.ScrollBar.vertical.size
        cmdLine.clear()
    }

    TextField {
        id: cmdLine
        width: parent.width
        anchors.bottom: parent.bottom
        onAccepted: {
            console.log("onAccepted cmdLine.text: ",text)

            if (activeCommand === Commands.Polyline) {

                if (activeCommand === Commands.Polyline) {
                    if (polyLineTemplate.receiveEvent(EventsQML.ReleaseRightClick, ma.getCursorPosWorld3DNode(), layer0) === EntitiesQML.Finish)
                        menuPolylineEnd.clearMenu()
                }
            }

            checkCommands(text)
        }

        Keys.onPressed: function (event){
            console.log("Keys.onPressed event.key "+event.key)
            console.log("cmdLine.text: "+cmdLine.text)


            //force cmdLine onAccepted() with a space key - disable for it is buggy, not always work
            if (event.key === Qt.Key_Space) {

                if (activeCommand === Commands.Polyline) {

                    if (activeCommand === Commands.Polyline) {
                        if (polyLineTemplate.receiveEvent(EventsQML.ReleaseRightClick, ma.getCursorPosWorld3DNode(), layer0) === EntitiesQML.Finish)
                            menuPolylineEnd.clearMenu()
                    }
                }

                checkCommands(cmdLine.text)

            }

            if (event.key === Qt.Key_Escape) {
                activeCommand = Commands.None
                genericCommand(Commands.UnPick)
            }

            //https://doc.qt.io/qt-5/qml-qtquick-keyevent.html#modifiers-prop
            //detect crtl+1
            if ((event.key === Qt.Key_1) && (event.modifiers & Qt.ControlModifier)) {
                entitiesPropertiesEditor.visible = !entitiesPropertiesEditor.visible
            }
        }//Keys.onPressed:

    }//TextField


    signal addText(str: var)

    onAddText: function(str){
        console.log("CmdTextList received test and is gonna added it----------------------: ", str)
        texArea.text += "\n"+str

       // viewscroll.ScrollBar.vertical.position = viewscroll.contentHeight
       // viewscroll.ScrollBar
        //viewscroll.ScrollBar.vertical.position = scrollPosition

    }
}

