import QtQuick //2.3

Rectangle {

    width: btnSize * 0.8
    height: width
    radius: width * 0.5
    color: colorsUI.currentStyle.mainColor
    border.width: defaultMargins*0.25
    border.color: colorsUI.frameBorderColor
    property bool expanded: false
    //property string iconName: expanded ? "up32.png" : "down32.png"
    property string iconName: "up32.png"
    rotation: expanded ? 180 : 0

    TextTemplate {
        id: tex
        width: parent.width
        height: parent.height
        //color: colorsUI.frameBorderColor
        bold: true
        //anchors.centerIn: parent
        text: qsTr(">>")
    }
    /*Image {
        id: imageItem
        anchors.fill: parent
        source: "qrc:/images/"+iconName
        mirrorVertically: !expanded
        mipmap: true
        //smooth: true
    }*/

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: expanded = !expanded
    }
}
