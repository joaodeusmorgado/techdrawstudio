import QtQuick 2.0
import QtQuick3D
import "../qmlEntities"

Node {
    id: root

    property vector3d center
    property real squareDist: 5 / mainCamera.zoomCamera
    property real lineDist: 50
    property real lineDistScaled: lineDist / mainCamera.zoomCamera

    //property real scaleCursor: 1
    property real thickCursor: 1

    property color colorSquare: "white"
    property color colorHorLine: "red"
    property color colorVertLine: "skyblue"

   RectangleThick {
        //p0: Qt.vector3d(center.x-squareDist, center.y+squareDist, 0)
        //p3: Qt.vector3d(center.x+squareDist, center.y-squareDist, 0)
        p0: Qt.vector3d(center.x-squareDist, center.y+squareDist, 0)
        p1: Qt.vector3d(center.x+squareDist, center.y+squareDist, 0)
        p2: Qt.vector3d(center.x+squareDist, center.y-squareDist, 0)
        p3: Qt.vector3d(center.x-squareDist, center.y-squareDist, 0)

        layerColor: colorSquare
        noLayerColor: colorSquare
        thick: thickCursor / mainCamera.zoomCamera
    }

    // vertical +y
    LineThick {
        p0: Qt.vector3d(center.x, center.y+squareDist, 0)
        p1: Qt.vector3d(center.x, center.y+squareDist+lineDistScaled, 0)
        thick: thickCursor / mainCamera.zoomCamera
        color1: colorVertLine
        color2: colorVertLine
        zoomScale: 10 * mainCamera.zoomCamera
    }


    // vertical -y
    LineThick {
        p0: Qt.vector3d(center.x, center.y-squareDist, 0)
        p1: Qt.vector3d(center.x, center.y-squareDist-lineDistScaled, 0)
        thick: thickCursor / mainCamera.zoomCamera
        color1: colorVertLine
        color2: colorVertLine
        zoomScale: 10 * mainCamera.zoomCamera
    }

    // horizontal +x
    LineThick {
        p0: Qt.vector3d(center.x+squareDist, center.y, 0)
        p1: Qt.vector3d(center.x+squareDist+lineDistScaled, center.y, 0)
        thick: thickCursor / mainCamera.zoomCamera
        color1: colorHorLine
        color2: colorHorLine
        zoomScale: 10 * mainCamera.zoomCamera
    }

    // horizontal -x
    LineThick {
        p0: Qt.vector3d(center.x-squareDist, center.y, 0)
        p1: Qt.vector3d(center.x-squareDist-lineDistScaled, center.y, 0)
        thick: thickCursor / mainCamera.zoomCamera
        color1: colorHorLine
        color2: colorHorLine
        zoomScale: 10 * mainCamera.zoomCamera
    }

}
