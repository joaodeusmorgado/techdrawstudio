import QtQuick 2.0

Rectangle {
    width: btnSize
    height: btnSize
    color: "white"
    border.color: "white"
    radius: btnSize * 0.4

    property string m_text: "0"
    property string m_colorText: "#FFFFFF"

    Text {
        id: m_BtncolorNumber
        anchors.centerIn: parent
        font.bold: true
        color: m_colorText
        text: m_text
    }

    signal buttonClick()

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: buttonClick()
    }
}
