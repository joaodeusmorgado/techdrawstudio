import QtQuick 2.3
import "../qmlEntities"
import "../qmlEnums"

Rectangle {
    id: root
    width: btnSize
    height: btnHeight
    color: isPressed || isSelected ? colorPressed : colorClear
    border.color: colorsUI.currentStyle.btnBorderColor
    border.width: btnBorderWidth
    radius: btnSize * 0.4
    //radius: 24


    property alias containsMouse: mouseArea.containsMouse
    property string colorClear: colorsUI.currentStyle.btnBkgColor
    property string colorPressed: colorsUI.currentStyle.btnBkgColorPressed

    property bool isSelectable: true
    property bool isSelected: false
    property bool isPressed: false
    property int type: Commands.None

    // command to be deleted later, after MenuSecondary cleanup ???
    //property string command: type//example: "", "circle", "circleUI"
    property alias iconVisible: imageItem.visible
    property alias text: textItem.text


    Text {
        id: textItem
        anchors.centerIn: parent
        text: ""
    }

    property string iconName:"circle256.png"
    Image {
        id: imageItem
        anchors.fill: parent
        anchors.margins: parent.width * 0.1
        source: "qrc:/images/"+iconName
        visible: (textItem.text === "")? true: false
        fillMode: Image.PreserveAspectFit
        mipmap: true
    }

    signal buttonClick()
    signal buttonDoubleClick()
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            console.log("button "+type + " is pressed............................")
            buttonClick()
        }
        onDoubleClicked: {
            buttonDoubleClick()
        }
        onPressed: isPressed = true
        onReleased: isPressed = false
        onCanceled: isPressed = false
    }
}
