import QtQuick 2.9
import "components"
import "qmlEntities"
import "qmlEnums"

Item {
    id: root
    //width: mainRoot.width
    width: listViewMenu2.width > mainRoot.width ? mainRoot.width : listViewMenu2.width
    height: btnHeight

    property alias modelMenu: modelMenu
    property int currentButton: Commands.None

    signal btnClick(btnSignal: int)//generic click
    signal btnDoubleClick(btnSignal: int)//generic doubleClick
    signal clearMenu()
    //signal btnOffsetEditor()

    //signal btnPolylineFinish()

    onClearMenu: modelMenu.clear()

    ListView {
        id: listViewMenu2
        //width: parent.width - defaultMargins*2
        width: modelMenu.count * (btnSize + defaultMargins/2)
        height: btnSize

        orientation: ListView.Horizontal
        spacing: defaultMargins / 2
        clip: true
        model: modelMenu
        delegate: delegateMenu2
    }

    ListModel {
        id: modelMenu
    }

    Component {
        id: delegateMenu2
        ButtonHybrid {
            text: textName
            type: typeName
            isSelected: activeCommand === type
            onButtonClick: {
                currentButton = currentButton === typeName ? Commands.None : type
                // activeCommand = currentButton
                btnClick(type)
            }

            onButtonDoubleClick: {
                /*if (typeName === "polylineFinish")  {
                    return;
                }*/

                //console.log("btn double click")
                //currentButton = typeName + "Editor"
                currentButton = currentButton === typeName ? Commands.None : type
                //activeCommand = currentButton
                btnDoubleClick(type)
            }
        }
    }
}
