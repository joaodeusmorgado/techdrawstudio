import QtQuick 2.0

Item {
    id: root


    function sum3D(p0: vector3d, p1: vector3d): vector3d{
        var p = Qt.vector3d(p0.x+p1.x, p0.y+p1.y, p0.z+p1.z)
        return p
    }

    function sub3D(p1: vector3d, p0: vector3d): vector3d{
        var p = Qt.vector3d(p1.x-p0.x, p1.y-p0.y, p1.z-p0.z)
        return p
    }

    function distance3DDelta(p1: vector3d, p2: vector3d, delta: real): vector3d{
        var dist = distance3D(p1, p2)
        return dist > delta ? false : true
    }

    function distance3D(p1: vector3d, p2: vector3d): vector3d{
        var dist = Math.sqrt( (p2.x-p1.x)*(p2.x-p1.x) + (p2.y-p1.y)*(p2.y-p1.y) + (p2.z-p1.z)*(p2.z-p1.z) )
        return dist;
    }

    function distance2D(a: real, b: real): real{
        return Math.abs(a-b)
    }

    //p0 and p1 are the start and end points of the line, p is the point to check if is inside the line
    function checkPointInLine(p0: vector3d, p1: vector3d, p: vector3d, delta: real): bool{
        var p0aux = p0.x < p1.x ? p0 : p1
        var p1aux = p0.x < p1.x ? p1 : p0

        var xMax = p0.x > p1.x ? p0.x : p1.x
        var xMin = p0.x < p1.x ? p0.x : p1.x
        var yMax = p0.y > p1.y ? p0.y : p1.y
        var yMin = p0.y < p1.y ? p0.y : p1.y


        //check for vertical lines
        if (p0aux.x === p1aux.x) {
            if (Math.abs( p0aux.x - p.x) <  delta)
            //check if point is near the vertical line
            //line.highlighted = true
            return true;
        }

        //check for horizontal lines
        if (p0aux.y === p1aux.y) {
            if (Math.abs( p0aux.y - p.y ) < delta )
            //console.log("check for horizontal lines")
            //line.highlighted = true
            return true;
        }

        // check other lines than vertical / horizontal
        var lineX = xMax - xMin;
        var lineY = yMax - yMin;
        var distX = p.x - xMin;
        var distY = distX * lineY / lineX;

        //line is from left to right and top to bottom
        if ( p0aux.y > p1aux.y ) {
            if ( Math.abs( yMax - distY - p.y ) < delta ) {
                //console.log("line.highlighted before: "+line.highlighted)
                //line.highlighted = true
                return true;
            }
        }
        //line is from left to right and bottom to top
        else {
            if ( Math.abs( yMin + distY - p.y ) < delta ) {
                //line.highlighted = true
                return true;
            }
        }
        return false
    }//function checkPointInLine(p0: vector3d, p1: vector3d, delta: real)

    function angleFromPoints(centerPoint: vector3d, radiusPoint: vector3d): real{
        var distX = radiusPoint.x - centerPoint.x
        var distY = radiusPoint.y - centerPoint.y
        var angle = Math.atan2(distY, distX)
        //if (angle < 0 && angle > -Math.PI)
          //  angle = 2*Math.PI + angle
        return angle
    }

    function rotate2d(p: vector3d, angle: real): vector3d{
        var r = Qt.vector3d(p.x * Math.cos(angle) - p.y * Math.sin(angle),
                               p.x * Math.sin(angle) + p.y * Math.cos(angle), p.z)

        return r;
    }

    function rotate2DFromPoint(center: vector3d, o: real, p: vector3d): vector3d{
        var x = Math.cos(o)*p.x - Math.sin(o)*p.y + center.x - Math.cos(o)*center.x + Math.sin(o)*center.y;
        var y = Math.sin(o)*p.x + Math.cos(o)*p.y + center.y - Math.sin(o)*center.x - Math.cos(o)*center.y;
        var r = Qt.vector3d(x, y, p.z);
        return r;
    }

    function scale2DfromPoint(center: vector3d, s: real, p: vector3d): vector3d{
        var x = s * p.x - center.x * s + center.x;
        var y = s * p.y - center.y * s + center.y;
        var result = Qt.vector3d(x, y, p.z)
        return result;
    }

}
