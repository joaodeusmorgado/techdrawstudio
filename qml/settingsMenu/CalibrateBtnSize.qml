import QtQuick 2.0
import "../components"

MySettingsItemFrame {
    id: root

    width: mainRoot.width
    //height: expanded ? calibrateButtons.height + myFrame.height + 3*defaultMargins :
      //                 myFrame.height + defaultMargins

    height: expanded ? heightOpen : heightClose

    heightOpen : calibrateButtons.height + myFrame.height + 3*defaultMargins
    heightClose : myFrame.height + defaultMargins

    itemText.text: qsTr("Calibrate size of buttons")


    Row {
        id: calibrateButtons
        anchors.top: myFrame.bottom//parent.top
        anchors.topMargin: defaultMargins
        anchors.left: parent.left
        anchors.leftMargin: defaultMargins

        opacity: childOpacity

        spacing: defaultMargins

        ButtonHybrid {
            id: btnDecrease
            text: qsTr("-")
            onButtonClick: mainRoot.calibrationFactor >= 0.5 ? mainRoot.calibrationFactor -= 0.1 :
                                                               mainRoot.calibrationFactor
        }

        TextInputHy {
            id: inputCalibrate
            text: parseFloat(mainRoot.calibrationFactor).toFixed(2)
        }

        ButtonHybrid {
            id: btnIncrease
            text: qsTr("+")
            onButtonClick: mainRoot.calibrationFactor <= 2 ? mainRoot.calibrationFactor += 0.1 :
                                                               mainRoot.calibrationFactor
        }
    }//Row


}//Calibrate size of buttons
