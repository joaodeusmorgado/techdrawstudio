import QtQuick 2.0
import "../components"

MySettingsItemFrame {
    id: root

    width: mainRoot.width
    height: expanded ? heightOpen : heightClose

    heightOpen : col.height + myFrame.height + 3*defaultMargins
    heightClose : myFrame.height + defaultMargins

    itemText.text: qsTr("Object snap settings")

    Column {
        id: col
        width: parent.width
        anchors.top: myFrame.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins
        spacing: defaultMargins

        opacity: childOpacity

        OsnapElement {
            id: endpoint
            width: parent.width - defaultMargins
            iconName: "osnap_Rectangle.png"
            label: qsTr("Endpoint")
            onCheckedChanged: {
                console.log("onCheckedChanged:"+checked)
                drawingBoard.osnapEndpoint = checked
            }
        }

        OsnapElement {
            id: osnapMidpoint
            width: parent.width - defaultMargins
            iconName: "osnap_Triangle.png"
            label: qsTr("Midpoint")
            onCheckedChanged: drawingBoard.osnapMidpoint = checked
        }

        OsnapElement {
            id: osnapCenter
            width: parent.width - defaultMargins
            iconName: "osnap_Circle.png"
            label: qsTr("Center")
            onCheckedChanged: drawingBoard.osnapCenter = checked
        }


    }//Column



}
