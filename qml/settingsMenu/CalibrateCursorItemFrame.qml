import QtQuick 2.0
import "../components"

MySettingsItemFrame {

    id: root

    width: mainRoot.width
    height: expanded ? heightOpen : heightClose

    heightOpen : col.height + myFrame.height + 3*defaultMargins
    heightClose : myFrame.height + defaultMargins

    itemText.text: qsTr("Calibrate cursor")

    signal calibrateCursorSignal()
    Column {
        id: col
        width: parent.width
        anchors.top: myFrame.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins
        spacing: defaultMargins

        opacity: childOpacity

        ButtonHybrid {
            width: btn2Size*2.5
            text: qsTr("Calibrate cursor")
            onButtonClick: calibrateCursorSignal()
        }
    }

}
