import QtQuick 2.15
import "../components"
import "../qmlEntities"
import "../qmlEnums"

Rectangle {
    id: root

    color: colorsUI.currentStyle.mainColor

    signal btnOk()
    signal btnCancel()

    Flickable {
        id: m_verticalFlickable

        anchors.top: parent.top
        anchors.left: parent.left

        width: parent.width
        height: parent.height - okCancel.height
        contentWidth: parent.width
        contentHeight: colSettingsItems.height + 1 * defaultMargins
        clip: true

        Column {
            id: colSettingsItems
            width: parent.width - 2*defaultMargins
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: defaultMargins

            spacing: 2 * mm
            CalibrateBtnSize {
                id: calibrateBtn
                width: parent.width
                color: colorsUI.mainColor
            }

            EntitiesCreation {
                id: entitiesCreation
                width: parent.width

            }

            ObjectSnap {
                width: parent.width
            }

            CalibrateCursorItemFrame {
                width: parent.width
                onCalibrateCursorSignal: calibrateCursor_01.visible = true
            }

        }//Column


    } //Flickable


    MyComboBox {
        id: myComboBoxEntityStart
        //x: entitiesCreation.startInput.x
        //y: entitiesCreation.startInput.y

        //x: entitiesCreation.mapToItem(root, 0, 0).x
        //y: entitiesCreation.mapToItem(root, 0, 0).y

        width: entitiesCreation.mouseAreaStart.width
        height: entitiesCreation.mouseAreaStart.height * elementsCount

        itemHeight: entitiesCreation.mouseAreaStart.height

        visible: false
        onComboClick: {
            entitiesCreation.startInput.text = itemText
            visible = false
            console.log(itemText)
        }
        comboModel: myModel
        elementsCount: myModel.count

        ListModel {
            id: myModel
            ListElement {
                name: "Press"
            }
            ListElement {
                name: "Release"
            }
            ListElement {
                name: "Click"
            }
        }//ListModel

        onComboPress: {
            if (itemText === "Press") {
                entityCreationDef.entityStart = EventsQML.Press;
                entityCreationDef.entityFinish = EventsQML.Release;
                commands.cpr = EventsQML.Press
            }
            else if(itemText === "Click") {
                entityCreationDef.entityStart = EventsQML.Click;
                entityCreationDef.entityFinish = EventsQML.Click;
                commands.cpr = EventsQML.Click
            }
            else if (itemText === "Release") {
                entityCreationDef.entityStart = EventsQML.Release;
                entityCreationDef.entityFinish = EventsQML.Release;
                commands.cpr = EventsQML.Release
            }
            else ;
        }
    }//MyComboBox

    ButtonBox {
        id: okCancel
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        showBtnCancel: false
        onOk: parent.btnOk()
        onCancel: parent.btnCancel()
    }


    CalibrateCursor_Wizard_01 {
        id: calibrateCursor_01
        anchors.fill: parent
        visible: false
        onBtnOk: {
            calibrateCursor_01.visible = false
        }
        /*onCalibrateDesktop: {
            fingerCursorDist = 0
            calibrateCursor_01.visible = false
        }*/

        onCalibrateTouchDevices: {
            calibrateCursor_01.visible = false
            calibrateCursor_02.visible = true
        }
    }

    CalibrateCursor_Wizard_02 {
        id: calibrateCursor_02
        anchors.fill: parent
        visible: false
        onBtnCancel: calibrateCursor_02.visible = false
        onBtnOk: {
            calibrateCursor_02.visible = false
            calibrateCursor_03.visible = true
        }
    }

    CalibrateCursor_Wizard_03 {
        id: calibrateCursor_03
        anchors.fill: parent
        visible: false
    }


}
