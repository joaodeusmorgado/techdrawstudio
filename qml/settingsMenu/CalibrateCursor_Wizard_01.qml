import QtQuick 2.0
import "../components"

Rectangle {
    id: calibrateCursor_01
    width: mainRoot.width
    height: mainRoot.height
    color: colorsUI.currentStyle.mainColor

    //signal calibrateDesktop()
    signal calibrateTouchDevices()


    ButtonHybrid {
        id: btnPC
        width: btn2Size*4.5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: defaultMargins
        text: qsTr("Calibrate cursor for desktop")
        onButtonClick: {
            fingerCursorDist = 0
            messageBox.visible = true
            //calibrateCursor_01.visible = false
        }
    }

    ButtonHybrid {
        anchors.top: btnPC.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins
        width: btn2Size*4.5
        text: qsTr("Calibrate cursor for touch devices")
        onButtonClick: calibrateTouchDevices()
    }



    signal btnOk()
    signal btnCancel()
    ButtonBox {
        id: okCancel
        anchors.bottom: parent.bottom
        showBtnCancel: false
        onOk: parent.btnOk()
        onCancel: parent.btnCancel()
    }

    MyMessageBox {
        id: messageBox
        visible: false
        anchors.fill: parent
        text: qsTr("Calibration finnished!")
        onBtnOk: {
            messageBox.visible = false
            calibrateCursor_01.visible = false
        }
    }
}
