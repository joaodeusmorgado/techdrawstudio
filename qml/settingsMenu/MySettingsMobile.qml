import QtQuick 2.15

Rectangle {
    id: winSettingsMobile

    //width: mainRoot.width //*0.2
    //height: mainRoot.height //*0.5
    anchors.fill: parent

    MySettings {
        anchors.fill: parent
        onBtnOk: parent.visible = false
    }

}
