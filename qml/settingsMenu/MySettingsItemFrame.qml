import QtQuick 2.0
import "../components"

Rectangle {
    id: root

    radius: defaultMargins
    color: colorsUI.mainColor
    border.width: defaultMargins/2
    border.color: colorsUI.frameBorderColor

    property alias itemText: itemTitle
    property alias expanded: btnExpand.expanded
    property alias myFrame: myFrame

    property real heightOpen// : myFrame.height
    property real heightClose //: myFrame.height
    property real childOpacity

    onExpandedChanged: {
        expanded ? root.state = "Open" : root.state = "Close"
    }

    state: "Close"

    states: [
        State {
            name: "Open"
            PropertyChanges { target: root; height: heightOpen; childOpacity: 1 }
        },
        State {
            name: "Close"
            PropertyChanges { target: root; height: heightClose; childOpacity: 0 }
        }
    ]

    /*transitions: Transition {
        NumberAnimation {
            target: root
            properties: "height, childOpacity"
            duration: 500
            easing.type: Easing.OutCubic
        }
    }*/

    property int closeOpenDuration: 300
    property int fade: 300
    transitions: [
        Transition {
        from: "Close"; to: "Open"
        SequentialAnimation {
            NumberAnimation {
                target: root
                properties: "height"
                duration: closeOpenDuration
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                target: root
                properties: "childOpacity"
                duration: fade
                easing.type: Easing.OutCubic
            }
        }
        },
        Transition {
        from: "Open"; to: "Close"
        SequentialAnimation {
            NumberAnimation {
                target: root
                properties: "childOpacity"
                duration: fade
                easing.type: Easing.OutCubic
            }
            NumberAnimation {
                target: root
                properties: "height"
                duration: closeOpenDuration
                easing.type: Easing.OutCubic
            }
        }
        }
    ]

    Item {
        id: myFrame
        width: parent.width
        height: (itemTitle.height > btnExpand.height ? itemTitle.height : btnExpand.height)
                + defaultMargins

        Text {
            id: itemTitle
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: defaultMargins
            font.bold: true
            font.italic: true
            color: colorsUI.textDefault
        }

        ButtonExpand {
            id: btnExpand
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: defaultMargins
        }
    }

}

