import QtQuick 2.3
import "../components"

Rectangle {
    id: calibrateCursorWizard03
    width: mainRoot.width
    height: mainRoot.height
    color:  colorsUI.currentStyle.mainColor

   // signal close()

    Image {
        id: imgCalibrate
        anchors.centerIn: parent
        sourceSize.width: 30*mm
        //width: 35*mm
        fillMode: Image.PreserveAspectFit
        anchors.horizontalCenter: parent.horizontalCenter
        source: "qrc:/images/cursor.png"
        mipmap: true
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            fingerCursorDist = mouseY - parent.height*0.5
            console.log("fingerCursorDist is: "+fingerCursorDist)
            messageBox.visible = true

        //    close()
        }
    }

    MyMessageBox {
        id: messageBox
        visible: false
        anchors.fill: parent
        //anchors.centerIn: parent
        text: qsTr("Calibration finnished!")
        onBtnOk: {
            messageBox.visible = false
            calibrateCursorWizard03.visible = false
        }
    }

}
