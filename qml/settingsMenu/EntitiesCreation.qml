import QtQuick 2.0
import QMLEnumEvents 1.0
//import Qt.labs.settings 1.0
//import com.EntityDefinition 1.0
import "../components"

MySettingsItemFrame {
    id: root

    width: mainRoot.width
    height: expanded ? heightOpen : heightClose

    heightOpen : col.height + myFrame.height + 3*defaultMargins
    heightClose : myFrame.height + defaultMargins

    itemText.text: qsTr("Entities settings")

    property alias mouseAreaStart: mouseAreaStart
    property alias startInput: startInput

    Column {
        id: col
        width: parent.width
        anchors.top: myFrame.bottom
        anchors.left: parent.left
        anchors.margins: defaultMargins
        spacing: defaultMargins

        opacity: childOpacity

        Text {
            id: startEntityText
            text: qsTr("Start a new entity with a:")
        }

        //Row Entity start
        Row {
            width: parent.width - defaultMargins

            Item {
                width: parent.width / 2
                height: btnSize
                Text {
                    id: textStart
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Start:")
                }
            }
            Rectangle {
                id: btnStart
                width: parent.width / 3
                height: btnSize
                radius: defaultMargins
                Text {
                    id: startInput
                    anchors.centerIn: parent
                    text: "Press"//entityState(myComboBoxEntityStart.itemText)
                }
                MouseArea {
                    id: mouseAreaStart
                    anchors.fill: parent
                    onClicked: {
                        myComboBoxEntityStart.x = mouseAreaStart.mapToItem(root, 0, 0).x + defaultMargins
                        myComboBoxEntityStart.y = mouseAreaStart.mapToItem(root, 0, 0).y + defaultMargins
                        myComboBoxEntityStart.visible = true
                    }
                }

            }
        }//Row Entity start

        //-----------------
        /*
        //Row Entity Finish
        Row {
            width: parent.width - defaultMargins

            Item {
                width: parent.width / 2
                height: btnSize
                Text {
                    id: textFinish
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Finish:")
                }
            }
            Rectangle {
                id: btnFinish
                width: parent.width / 3
                height: btnSize
                radius: defaultMargins
                Text {
                    id: finishInput
                    anchors.centerIn: parent
                    text: entityState(myComboBoxEntityStart.itemText)
                }
                MouseArea {
                    id: mouseAreaFinish
                    anchors.fill: parent
                    onClicked: {
                        myComboBoxEntityStart.x = mouseAreaStart.mapToItem(root, 0, 0).x + defaultMargins
                        myComboBoxEntityStart.y = mouseAreaStart.mapToItem(root, 0, 0).y + defaultMargins
                        myComboBoxEntityStart.visible = true
                    }
                }

            }
        }//Row Entity Finish
        */


    }//Column



    function entityState(entityEnum){
        if (entityEnum === Events.Pressed) return "Press"
        else if (entityEnum === Events.Released) return "Release"
        else if (entityEnum === Events.MouseChanged) return "Move"
        else return "";
    }

}

