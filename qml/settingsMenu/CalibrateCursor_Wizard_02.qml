import QtQuick 2.3
import "../components"

Rectangle {
    width: mainRoot.width
    height: mainRoot.height
    color:  colorsUI.currentStyle.mainColor

    Image {
        id: imgCalibrate
        anchors.top: parent.top
        anchors.margins: defaultMargins
        width: parent.width*0.5
        fillMode: Image.PreserveAspectFit

        anchors.horizontalCenter: parent.horizontalCenter
        source: "qrc:/images/calibrateCursor.png"
        mipmap: true
    }

    Text {
        anchors.top: imgCalibrate.bottom
        anchors.topMargin: defaultMargins
        //anchors.left: parent.left
        //anchors.right: parent.right
        //anchors.margins: defaultMargins*2
        width: parent.width-defaultMargins*2
        anchors.centerIn: parent
        elide: Text.ElideMiddle
        wrapMode: Text.WordWrap
        //width: parent.width
        font.bold: true
        text: qsTr("\n\nIn touch devices, the cursor should not be under the finger. "
                    + "Cursor should have a small distance from the finger so you can see it. "
                    + "\nClick " + okCancel.btnOkLabel + " and a image of the cursor will appear, "
                    + "click on the screen at a small distance from the cursor to calibrate it.")
    }



    signal btnOk()
    signal btnCancel()
    ButtonBox {
        id: okCancel
        anchors.bottom: parent.bottom
        btnOkLabel: qsTr("Next")
        onOk: parent.btnOk()
        onCancel: parent.btnCancel()
    }
}
