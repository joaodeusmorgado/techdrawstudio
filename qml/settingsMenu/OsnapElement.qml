import QtQuick 2.0
import "../components"


Item {
    id: root

    height: btnSize

    property string iconName: "osnap_Rectangle.png"
    property alias label: checkBtn.label
    property alias checked: checkBtn.checked

    Image {
        id: img
        source: "qrc:/images/osnap/"+iconName
    }

    MyCheckButton {
        id: checkBtn
        anchors.left: img.right
        anchors.leftMargin: defaultMargins
        anchors.verticalCenter: img.verticalCenter
        checked: true
    }
}
