import QtQuick 2.12
import QtQuick3D //1.15
import Entity_CircleThick 1.0
import LineTexture 1.0
import "../qmlEnums"

EntityTemplate {
    id: root
    property alias center: circle.center
    property alias pointInRadius: circle.pointInRadius
    property alias radius: circle.radius
    property alias count: circle.count
    property alias picked: circle.picked
    property alias thick: circle.thick
    property alias pickInterval: circle.pickInterval


    // commands ---------------------
    property vector3d pCenterAux

    onCommandCprStart: function(pos) {
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){
            pCenterAux = circle.center
            commandInitialPos = pos
        }
        if (activeCommand === Commands.Copy)
            parent.addCircle(circle.center, circle.radius)

        if (activeCommand === Commands.Offset) {
            console.log("Do a offset to the circle.....................................")
            mathLib.setOffset(offset);
            if (mathLib.offsetCircleCalculate(circle.center, circle.radius, pos)) {
                parent.addCircle(circle.center, mathLib.offsetCircleRadius())
            }
        }
    }

    onCommandCprUpdate: function(pos, cprCounter){

        if (activeCommand === Commands.Rotate && cprCounter === 1){

            if (osnapON)
                rotateStartPoint = osnapPoint
            else
                rotateStartPoint = pos
            rotateStartAngle = mathLib.angleFromPoints(commandInitialPos, rotateStartPoint)
            pCenterAux = circle.center
        }
    }

    onCommandMtm: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){
            commandUpdatePosition = pos
            distAux = lib.sub3D(commandUpdatePosition, commandInitialPos)
            circle.center = mathLib.sum3D(pCenterAux, distAux)
        }

        if (activeCommand === Commands.Rotate && cprCounter === 2){
            if (osnapON)
                pos = osnapPoint

            rotateUpdateAngle = mathLib.angleFromPoints(commandInitialPos, pos)
            var angle = rotateUpdateAngle - rotateStartAngle
            circle.center = mathLib.rotate2DFromPoint(commandInitialPos, angle, pCenterAux)


        }

    }

    onCommandCprFinish: function(pos){

        if (activeCommand === Commands.Selection) {

            if (commandInitialPos.x < pos.x) {

                if (mathLib.circleInsideRectangle(circle.center, circle.radius, commandInitialPos, pos)) {
                    picked = true
                }
            }
            if (commandInitialPos.x > pos.x) {

                if (mathLib.circleIntersectRectangle(circle.center, circle.radius, commandInitialPos, pos))
                    picked = true
            }

        }


    }


    // commands ---------------------

    geometry: EntityCircleThick {
        id: circle
        center: Qt.vector3d(100, 0, 0)
        //pointInRadius: Qt.vector3d(0, 0, 0)
        radius: 55
        onPickedChanged: function(newPicked) {root.pickCounter(newPicked)}
        onHighlightedChanged: {
            //console.log("circle onHighlightedChanged: "+highlighted)
            if (highlighted && pickable && activeCommand === Commands.None) {
                picked = !picked
            }
        }
    }
    // copy, move ---------------------



    materials: DefaultMaterial {
        id: material
        //lineWidth: 4.0;
        lighting:  DefaultMaterial.NoLighting
        //diffuseColor: line.isPicked ? "blue" : "red"
        diffuseMap: Texture {
            id: tex
            textureData: LineTexture {
                id: lineTex
                color1: circle.highlighted || circle.picked ? colorHighlighted : root.color1
                color2: circle.highlighted || circle.picked ? colorHighlighted : root.color2
                //onColor1Changed: console.log("color1: "+ color1)
            }
            magFilter: Texture.Nearest
            minFilter: Texture.Nearest
            mappingMode: Texture.UV
            scaleU: 32
            scaleV: 1
        }
    }

    Square {
        id: squareCenter
        center: circle.center
        visible: circle.picked || osnapON_circleCenter
    }

    Square {
        id: squarePy
        center: Qt.vector3d(circle.center.x, circle.center.y+circle.radius, circle.center.z)
        visible: circle.picked || osnapON_circlePy
    }
    Square {
        id: squarePy_
        center: Qt.vector3d(circle.center.x, circle.center.y-circle.radius, circle.center.z)
        visible: circle.picked || osnapON_circlePy_
    }

    Square {
        id: squarePx
        center: Qt.vector3d(circle.center.x+circle.radius, circle.center.y, circle.center.z)
        visible: circle.picked || osnapON_circlePx
    }

    Square {
        id: squarePx_
        center: Qt.vector3d(circle.center.x-circle.radius, circle.center.y, circle.center.z)
        visible: circle.picked || osnapON_circlePx_
    }

    function receiveEvent(evt, p: vector3d, myParent){

        //console.log("circle istart receive event p: "+p)

        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {

            //console.log("circle istart receive event p: "+p)
            circle.center = p
            circle.pointInRadius = p

            if (osnapON) {
                circle.center = circle.pointInRadius = osnapPoint
            }
            else {
                circle.center = circle.pointInRadius = p
            }

            root.visible = true
            return EntitiesQML.Start
        }

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            circle.pointInRadius = p
            return EntitiesQML.Update
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {

            if (osnapON) {
                circle.pointInRadius = osnapPoint
            }

            root.visible = false
            myParent.addCircle(circle.center, circle.radius);
            return EntitiesQML.Finish
        }

        return -1;
    }

    property bool osnapON_circleCenter: false
    property bool osnapON_circlePy: false
    property bool osnapON_circlePy_: false
    property bool osnapON_circlePx: false
    property bool osnapON_circlePx_: false

    onOsnapON_circleCenterChanged: osnapON = osnapON_circleCenter
    onOsnapON_circlePxChanged: osnapON = osnapON_circlePx
    onOsnapON_circlePx_Changed: osnapON = osnapON_circlePx_
    onOsnapON_circlePyChanged: osnapON = osnapON_circlePy
    onOsnapON_circlePy_Changed: osnapON = osnapON_circlePy_

    function checkOsnap(mp: vector3d) {
        if (!checkingOsnap)
            return;

        if (mathLib.distance3DDelta(mp, squareCenter.center, osnapDistance)) {
            osnapON_circleCenter = true
            osnapPoint = squareCenter.center
            return;
        }
        else
            osnapON_circleCenter = false

        if (mathLib.distance3DDelta(mp, squarePx.center, osnapDistance)) {
            osnapON_circlePx = true
            osnapPoint = squarePx.center
            return;
        }
        else
            osnapON_circlePx = false

        if (mathLib.distance3DDelta(mp, squarePx_.center, osnapDistance)) {
            osnapON_circlePx_ = true
            osnapPoint = squarePx_.center
            return;
        }
        else
            osnapON_circlePx_ = false

        if (mathLib.distance3DDelta(mp, squarePy.center, osnapDistance)) {
            osnapON_circlePy = true
            osnapPoint = squarePy.center
            return;
        }
        else
            osnapON_circlePy = false

        if (mathLib.distance3DDelta(mp, squarePy_.center, osnapDistance)) {
            osnapON_circlePy_ = true
            osnapPoint = squarePy_.center
            return;
        }
        else
            osnapON_circlePy_ = false

    }

    function highLight(mousePoint: vector3d) {
        var dist = mathLib.distance3D(center, mousePoint)
        if ( dist  <  (radius + thick + pickInterval) && (dist > (radius - thick - pickInterval) ) ) {
            circle.highlighted = true
        }
        else {
            circle.highlighted = false
        }
    }

    onClick: function(pos) {
        //console.log("entity template click pos: " + pos)
        console.log("circle click pos: " + pos)
        if (activeCommand !== Commands.None)
            return;

        var dist = mathLib.distance3D(center, pos)
        if ( dist  <  (radius + thick + pickInterval) && (dist > (radius - thick - pickInterval) ) ) {
            circle.picked = !circle.picked
        }

    }

}
