import QtQuick // 2.12
import QtQuick3D //1.15
import Entity_PolyLineThick 1.0
import LineTexture 1.0
import "../qmlEnums"

EntityTemplate {
    id: rootPolyLineThick
//    property alias p0: line.p0
  //  property alias p1: line.p1
    property alias picked: polyline.picked
    property alias thick: polyline.thick
    property alias pickInterval: polyline.pickInterval

    property alias pickPoint: polyline.pickPoint
    property alias zoomScale: tex.scaleU
    property alias pointsList: polyline.pointsList

    /*onPickedChanged: function(picked){
        if (!picked)
            return
        if (activeCommand === Commands.Fillet || activeCommand === Commands.Extend || activeCommand === Commands.Trim) {
            console.log("sendInfo: "+rootPolyLineThick)
            sendInfo(rootPolyLineThick)
        }
    }*/

    // commands---------------------
    property vector3d lineP0Aux
    property vector3d lineP1Aux
    property vector3d nodePosAux

    onCommandCprStart: function(pos) {
        if (!picked)
            return
        //console.log("onCommandCprStart activeCommand:"+activeCommand)
        if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Scale){
            /*lineP0Aux = line.p0
            lineP1Aux = line.p1
            nodePosAux = rootPolyLineThick.position*/
            //commandInitialPos = pos
        }

        if (activeCommand === Commands.Copy) {
            parent.addLine(line.p0, line.p1)
            console.log("copy polyline----------------------------------------------------")
        }

        if (activeCommand === Commands.Offset) {
            /*console.log("Do a offset to the line.....................................")
            mathLib.setOffset(offset);
            mathLib.offsetLineCalculate(line.p0, line.p1, pos)
            console.log("line.p0: "+line.p0)
            console.log("line.p1: "+line.p1)
            console.log("mathlib.offsetLineP0(): "+ mathLib.offsetLineP0())
            console.log("mathlib.offsetLineP1(): "+ mathLib.offsetLineP1())
            parent.addLine(mathLib.offsetLineP0(), mathLib.offsetLineP1())
            */
        }
    }

    onCommandCprUpdate: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Rotate && cprCounter === 1){
/*
            if (osnapON)
                rotateStartPoint = osnapPoint
            else
                rotateStartPoint = pos
            rotateStartAngle = mathLib.angleFromPoints(commandInitialPos, rotateStartPoint)
            lineP0Aux = line.p0
            lineP1Aux = line.p1
            */
        }
    }//signal commandCprUpdate(pos: vector3d, cprCounter: int)

    onCommandMtm: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){

            commandUpdatePosition = pos
            distAux = mathLib.sub3D(commandUpdatePosition, commandInitialPos)
            distAuxNode = mathLib.sub3D(commandUpdatePosition, commandInitialPos)
            rootPolyLineThick.position = mathLib.sum3D(nodePosAux, distAuxNode)

        }

        if (activeCommand === Commands.Scale ){
/*
            var dist = (pos.y - scaleStartPoint.y)
            var sca = (pos.y - scaleStartPoint.y)/50

            line.p0 = lib.scale2DfromPoint(scaleStartPoint, sca, lineP0Aux)
            line.p1 = lib.scale2DfromPoint(scaleStartPoint, sca, lineP1Aux)
            */
        }


        if (activeCommand === Commands.Rotate && cprCounter === 2){

            //console.log("set rotate update point")
/*
            if (osnapON)
                pos = osnapPoint

            rotateUpdateAngle = mathLib.angleFromPoints(commandInitialPos, pos)

            var angle = rotateUpdateAngle - rotateStartAngle


            line.p0 = mathLib.rotate2DFromPoint(commandInitialPos, angle, lineP0Aux)
            line.p1 = mathLib.rotate2DFromPoint(commandInitialPos, angle, lineP1Aux)
*/
        }
    }

    onCommandCprFinish: function(pos){


        /*if (activeCommand === Commands.Selection) {

            if (commandInitialPos.x < pos.x) {

                if (mathLib.segmentFullyInsideRectangle(line.p0, line.p1, commandInitialPos, pos)) {
                    picked = true
                }
            }

            if (commandInitialPos.x > pos.x) {

                if (mathLib.segmentFullyInsideRectangle(line.p0, line.p1, commandInitialPos, pos)) {
                    picked = true
                }
                //if (mathLib.segmentIntersectRectangle(line.p0, line.p1, commandInitialPos, pos))
                if (mathLib.checkLineRectangleIntersection(line.p0, line.p1, commandInitialPos, pos)){
                    picked = true
                }
            }
        }*/

        if (!picked)
            return
        console.log("onCommandCprFinish....................................................")
       /* if (activeCommand === Commands.Copy || activeCommand === Commands.Move) {
            line.updateP0P1(mathLib.sum3D(lineP0Aux, distAux), mathLib.sum3D(lineP1Aux, distAux))
            rootPolyLineThick.position = Qt.vector3d(0,0,0)
        }*/

    }

    //commands---------------------------

  //  property alias instancing: modelLine.instancing

    property bool pickableCommands: activeCommand === Commands.None || activeCommand === Commands.Fillet
                                    || activeCommand === Commands.Extend || activeCommand === Commands.Trim
  //  Model {
     //     id: modelLine
    geometry: EntityPolyLineThick {
        id: polyline
        pickInterval: thick + 4
        onPickedChanged: function(newPicked) {rootPolyLineThick.pickCounter(newPicked)}
        onHighlightedChanged: {
        //console.log("line onHighlightedChanged: "+highlighted)
        //  console.log("modelLine.pickable: "+modelLine.pickable)
        //console.log("activeCommand: "+activeCommand)
        //if (highlighted && modelLine.pickable && activeCommand === Commands.None) {
            if (highlighted && pickable && pickableCommands ) {
                picked = !picked
            }
        }
    }//EntityLineThick

    materials: DefaultMaterial {
        id: material
        //lineWidth: 4.0;
        lighting:  DefaultMaterial.NoLighting
        //diffuseColor: line.isPicked ? "blue" : "red"
        diffuseMap: Texture {
            id: tex
            textureData: LineTexture {
                id: lineTex
                color1: polyline.highlighted ? colorHighlighted : polyline.picked ? colorPicked : rootPolyLineThick.color1
                color2: polyline.highlighted ? colorHighlighted : polyline.picked ? colorPicked : rootPolyLineThick.color1
                //color2: line.highlighted || line.picked ? colorHighlighted : rootPolyLineThick.color2
            }
            magFilter: Texture.Nearest
            minFilter: Texture.Nearest
            mappingMode: Texture.UV
            scaleU: 4
            scaleV: 1
        }
    }//DefaultMaterial

/*    Square {
        id: squareP0
        center: line.p0
        visible: (line.picked || squareP0_OsnapON) && activeCommand !== Commands.Fillet
    }

    Square {
        id: squareP1
        center: line.p1
        visible: (line.picked || squareP1_OsnapON) && activeCommand !== Commands.Fillet
    }

    Square {
        id: squareMid
        center: Qt.vector3d(line.p0.x*0.5+line.p1.x*0.5, line.p0.y*0.5+line.p1.y*0.5, line.p0.z*0.5+line.p1.z*0.5)
        visible: (line.picked || squareMid_OsnapON) && activeCommand !== Commands.Fillet
    }

    Triangle {
        id: triangleP0
        pos: line.p1
        angleP0: line.p0
        angleP1: line.p1
        visible: line.picked && activeCommand !== Commands.Fillet
    }

    Triangle {
        id: triangleP1
        pos: line.p0
        angleP0: line.p1
        angleP1: line.p0
        visible: line.picked && activeCommand !== Commands.Fillet
    }*/

    function receiveEvent(evt, p: vector3d, myParent){

       // console.log("receiveEvent()-------------------------------------------------------------------")
        //console.log("////////////////////////////////////////////////////////")
        //console.log("EntitiesQML.Start: "+EntitiesQML.Start)
        //console.log("entityCreationDef.isStart(evt): "+entityCreationDef.isStart(evt))



        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {
            console.log("entityCreationDef.isStart: ",p, polyline.size)

            //console.log("EntitiesQML.Start: "+EntitiesQML.Start)

            //set mouseWorldPosition to p to avoid a bug in android / mobile
            //where drawing a line would always default to osnapPoint
            mouseWorldPosition = p
            if (osnapON) {
                //console.log("starting line with osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = osnapPoint
                polyline.addPn(osnapPoint);
                polyline.addPn(osnapPoint);
            }
            else {
                //console.log("starting line without osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = p
                polyline.addPn(p);
                polyline.addPn(p);
            }
            entityCreationDef.entityEndAtUpdate()

            //console.log("start line---------------------: "+line.p0)

            rootPolyLineThick.visible = true
            return EntitiesQML.Start
        }//isStart


        if (entityCreationDef.isNext(evt) === EntitiesQML.Next) {
            console.log("entityCreationDef.isNext: ",p, polyline.size)
          //  mouseWorldPosition = p //TODO: check if this is needed
            if (osnapON) {
                //console.log("starting line with osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = osnapPoint
                polyline.addPn(osnapPoint);
                polyline.addPn(osnapPoint);
            }
            else {
                //console.log("starting line without osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = p
                polyline.addPn(p);
                polyline.addPn(p);
            }
            entityCreationDef.entityEndAtUpdate()
            return EntitiesQML.Next;
        }//isNext



        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            console.log("update PolyLine", p, polyline.size)

            if (orthoMode) {
                /*if (mathLib.distance2D(p0.x, p.x) > mathLib.distance2D(p0.y, p.y))
                    //line.p1 = Qt.vector3d(p.x, line.p0.y, line.p1.z)
                    //line.updateP0P1(line.p0, Qt.vector3d(p.x, line.p0.y, line.p1.z));
                    polyline.updateLastPoint(Qt.vector3d(p.x, line.p0.y, line.p1.z))
                else
                    //line.p1 = Qt.vector3d(line.p0.x, p.y, line.p1.z)
                    line.updateP0P1(line.p0, Qt.vector3d(line.p0.x, p.y, line.p1.z));
                */

                //todo: fix orthoMode, for now just update the point
                polyline.updateLastPoint(p)
            }
            else {
                //line.p1 = p
                polyline.updateLastPoint(p)
            }//if (orthoMode)

            return EntitiesQML.Update
        }//isUpdate

        if (entityCreationDef.isFinishPolylines(evt) === EntitiesQML.Finish) {
            console.log("polyline entityCreationDef.isFinish: "+p)

            mouseWorldPosition = p

            console.log("polyline.pointsList[i]:",polyline.pointsList, polyline.size)
            polyline.removeLastPoint()
            polyline.removeLastPoint()
            console.log("polyline.removeLastPoint()",polyline.pointsList, polyline.size)

            //entityCreationDef.seClickPressCounterTo1()//Polyline is a especial case, lets "reset", so next cpr is also a isStart

            //only create a polyline if size is has at least two points
            if (polyline.size){
                console.log("create a polyline")
                parent.addPolyline(polyline.pointsList);
            }
            else
                console.log("skip polyline")


            polyline.picked = false
            rootPolyLineThick.visible = false
            polyline.reset()
            //line.p0 = line.p1 = Qt.vector3d(0, 0, 0)
            return EntitiesQML.Finish

        }//isFinish

        console.log("no Polyline event caught", p)
        return -1;

    }//function receiveEvent(evt, p: vector3d, myParent)

    property bool squareP0_OsnapON: false
    property bool squareP1_OsnapON: false
    property bool squareMid_OsnapON: false

    onSquareP0_OsnapONChanged: {
        //console.log("onSquareP0_OsnapONChanged: "+squareP0_OsnapON)
        osnapON = squareP0_OsnapON
    }
    onSquareP1_OsnapONChanged: {
        //console.log("onSquareP1_OsnapONChanged: "+squareP1_OsnapON)
        osnapON = squareP1_OsnapON
    }
    onSquareMid_OsnapONChanged: {
        //console.log("onSquareMid_OsnapONChanged: "+squareMid_OsnapON)
        osnapON = squareMid_OsnapON
    }


    function checkOsnap(mp: vector3d) {
        //console.log(this + " checkOsnap() point"+osnapPoint)
        if (!checkingOsnap)
            return;

        /*
        if (mathLib.distance3DDelta(mp, squareP0.center, osnapDistance)) {
            squareP0_OsnapON = true
            osnapPoint = squareP0.center
            return;
        }
        else {
            squareP0_OsnapON = false
        }

        if (mathLib.distance3DDelta(mp, squareP1.center, osnapDistance)) {
            //console.log("checkOsnap line id:" +rootPolyLineThick)
            squareP1_OsnapON = true
            osnapPoint = squareP1.center
            return;
        }
        else
            squareP1_OsnapON = false

        if (mathLib.distance3DDelta(mp, squareMid.center, osnapDistance)) {
            squareMid_OsnapON = true
            //console.log("distance3DDelta squareP0.Mid true")
            osnapPoint = squareMid.center
            return;
        }
        else
            squareMid_OsnapON = false

        */

    }//function checkOsnap(mp: vector3d)

    function highLight(pos: vector3d): bool {//mp - mouse position

        polyline.highlighted = checkPoinInPolyline(pos)
        /*if (!polyline.inside_Pick(pos)) {
            polyline.highlighted = false;
            return false;
        }

        //polyline.pointsList
        for (var i=0;i<polyline.size-1; i++){

            var isHighlighted = mathLib.checkPointInLine(polyline.pointsList[i], polyline.pointsList[i+1],
                                               pos, polyline.pickInterval)
            if (isHighlighted){
                polyline.highlighted = isHighlighted
                return isHighlighted;
            }
            //console.log("polyline.size:",polyline.size)
            //console.log("polyline.pointsList[i]:",polyline.pointsList)
            //console.log("polyline.pointsList[i]:",polyline.pointsList[i], i)
        }

        polyline.highlighted = false
        */
        //pickedPos = pos

    } //function highLight(mp)

    onClick: function(pos) {
        //console.log("entity template click pos: " + pos)
        //console.log("linethick click pos: " + pos)

        if (!polyline.inside_Pick(pos)) {
            return false;
        }

        if (checkPoinInPolyline(pos)){
            polyline.picked = !polyline.picked
            //pickedPos = pos
        }
    }

    function checkPoinInPolyline(pos: vector3d): bool{

        if (!polyline.inside_Pick(pos)) {
            //polyline.highlighted = false;
            return false;
        }

        //polyline.pointsList
        for (var i=0;i<polyline.size-1; i++){

            var isHighlighted = mathLib.checkPointInLine(polyline.pointsList[i], polyline.pointsList[i+1],
                                               pos, polyline.pickInterval)
            if (isHighlighted){
                //polyline.highlighted = isHighlighted
                return true
            }
            //console.log("polyline.size:",polyline.size)
            //console.log("polyline.pointsList[i]:",polyline.pointsList)
            //console.log("polyline.pointsList[i]:",polyline.pointsList[i], i)
        }

        return false
    }

   // property vector3d intersec

    /*onExecuteComm2: function(otherEntity){
        console.log("EXECUTE FILLET---------------------------------------------------------------------")
        //console.log("otherEntity.pickedPos: "+otherEntity.pickedPos)
        console.log("otherEntity.p0: "+otherEntity.p0)
        console.log("otherEntity.p1: "+otherEntity.p1)
    }*/

}
