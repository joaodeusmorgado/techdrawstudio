import QtQuick 2.12
import QtQuick3D //1.15
import CursorCross 1.0
import CursorEnum 1.0

Node {

    property alias center: cursor.center
    property alias squareDist: cursor.squareDist
    property alias lineDist: cursor.lineDist

    property alias colorHor: cursor.colorHor
    property alias colorVert: cursor.colorVert
    property alias colorRect: cursor.colorRect
    property alias cursorType: cursor.cursorType

    Component.onCompleted: {
        console.log("CursorEnum.normal in cursor: "+CursorEnum.normal)
        console.log("CursorEnum.rectangle in cursor: "+CursorEnum.rectangle)
    }

    Model {
        id: idCursorCross
        geometry: CursorCross {
            id: cursor
            //center: Qt.vector3d(10, 10, 0)
            //squareDist: 5 / mainCamera.zoomCamera
            //lineDist: 50 / mainCamera.zoomCamera
            //onCenterChanged: console.log("onCenterChanged: "+center)
            //colorVert: "orange"
           // cursorType: CursorEnum.rectangle

        }
        materials: PrincipledMaterial {
            id: material
            //lineWidth: 3
            lighting: PrincipledMaterial.NoLighting
            vertexColorsEnabled: true
        }
    }
}
