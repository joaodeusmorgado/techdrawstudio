import QtQuick 2.0
import "../qmlEnums"

Item {

    //entityStart should use EventsQML.Press on desktop
    //and EventsQML.Release on mobile touch plataforms
    //property int entityStart: (Qt.platform.os === "android" || Qt.platform.os === "ios" || Qt.platform.os === "qnx" )
      //                        ?  EventsQML.Release : EventsQML.Press//EventsQML.Press EventsQML.Click
    property int entityStart: EventsQML.Press
    //onEntityStartChanged: console.log("onEntityStartChanged: "+entityStart)
    property int entityUpdate: EventsQML.MouseMove
    property int entityFinish: EventsQML.Release//EventsQML.Click//EventsQML.Release
    property int entityFinishPolylines: EventsQML.ReleaseRightClick

    property int entityClickPressCounter: 0

    //entityEndAt: most entity creation should end at 1, for example, line starts with click 0, end at click 1
    //polyline is a special case, can have many clicks
    property int entityEndAt: 1 //most entity creation should end at 1, for example, line starts with click 0, end at click 1


    function isStart(eventType) {
        //console.log("EntityCreationDef check isStart: "+ eventType + " entityClickPressCounter: "+entityClickPressCounter)
        if ( (entityStart === eventType) && (entityClickPressCounter === 0) )
        {
            entityEndAt = 1
            //console.log("EntityCreationDef isStart ok: "+ eventType)
            entityClickPressCounter++;
            return EntitiesQML.Start;
        }
        //console.log("EntityCreationDef isStart failled: "+ eventType)
        return -1;
    }

    function isUpdate(eventType){
        console.log("isUpdate: "+ eventType)
        console.log("entityClickPressCounter: "+ entityClickPressCounter)
        console.log("entityEndAt: "+ entityEndAt)

        if ( (entityUpdate === eventType) && (entityClickPressCounter > 0) ){
            //console.log("EntityCreationDef isUpdate ok: "+eventType)
            return EntitiesQML.Update;
        }
        console.log("EntityCreationDef isUpdate failled: "+eventType)
        return -1;
    }

    //used for Polylines
    function isNext(eventType) {
        if ( (entityStart === eventType) && (entityClickPressCounter < entityEndAt)){
            //console.log("EntityCreationDef isFinish ok: "+ eventType)
            return EntitiesQML.Next;
        }
        //console.log("EntityCreationDef isNext failled: "+ eventType)
        return -1;
    }

    //used for polylines
    function isFinishPolylines(eventType) {
        if (entityFinishPolylines === eventType) {
            //console.log("EntityCreationDef isFinish ok: "+ eventType)
            entityClickPressCounter = 0;
            entityEndAt = 1
            return EntitiesQML.Finish;
        }
        //console.log("EntityCreationDef isFinish failled: "+ eventType)
        return -1;

    }

    function isFinish(eventType){
        if ( (entityFinish === eventType) && (entityClickPressCounter === entityEndAt)){
            //console.log("EntityCreationDef isFinish ok: "+ eventType)
            entityClickPressCounter = 0;
            return EntitiesQML.Finish;
        }
        //console.log("EntityCreationDef isFinish failled: "+ eventType)
        return -1;
    }

    function isPolylineFinish(eventType) {

    }

    //entityEndAtUpdate: increase entityEndAt - used for Polylines
    function entityEndAtUpdate(){
        entityEndAt++
    }

    /*function seClickPressCounterTo1() {
        entityClickPressCounter = 1;
    }*/

}
