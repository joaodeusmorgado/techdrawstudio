import QtQuick 2.12
import QtQuick3D

EntityTemplate {

    property alias image: image
    property alias source: image.source

    Item {
        width: image.sourceSize.width
        height: image.sourceSize.height
        anchors.centerIn: parent
        Image {
            id: image
            mipmap: true
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }
}
