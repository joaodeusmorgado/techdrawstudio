import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Circle 1.0

Node {

    property alias center: circle.center
    property alias pointInRadius: circle.pointInRadius
    property alias radius: circle.radius
    property alias baseColor: material.baseColor

    Model {
         geometry: EntityCircle {
             id: circle
             center: Qt.vector3d(100, 0, 0)
             //pointInRadius: Qt.vector3d(0, 0, 0)
             radius: 55
         }
         materials: PrincipledMaterial {
             id: material
             //lineWidth: 8.0
             baseColor: "yellow"
             lighting: PrincipledMaterial.NoLighting
         }
         pickable: true
         property bool isPicked: false
     }

}
