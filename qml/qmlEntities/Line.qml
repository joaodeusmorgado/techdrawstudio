import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Line 1.0

Node {

    //Note: Deprecated this and used LineThick instead
    //because lineWidth not always work, depending on the underlying graphics API
    property alias p0: line.p0
    property alias p1: line.p1
    property alias lineWidth: material.lineWidth
    property alias baseColor: material.diffuseColor
    property alias pickable: modelLine.pickable

    Model {
        id: modelLine
         geometry: EntityLine {
             id: line
             p0: Qt.vector3d(-55, -55, 50)
             p1: Qt.vector3d(55, 55, 50)
         }
         materials: DefaultMaterial {
             id: material
             diffuseColor: line.isPicked ? "blue" : "red"
             lighting: DefaultMaterial.NoLighting
         }
        /*materials: PrincipledMaterial {
             id: material
             baseColor: line.isPicked ? "blue" : "red"
             lighting: PrincipledMaterial.NoLighting
         }*/
         pickable: true
         property bool isPicked: false
     }
}
