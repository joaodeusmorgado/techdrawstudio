import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Rectangle 1.0

EntityTemplate {
    id: root
    property alias p0: re.p0
    property alias p1: re.p1
    property alias p2: re.p2
    property alias p3: re.p3
    //property alias baseColor: isByLayerColor ? layerColor : noLayerColor
    property alias picked: re.picked
    property alias thick: re.thick
    property alias pickable: modelRect.pickable


    Model {
        id: modelRect
         geometry: EntityRectangle {
             id: re
             p0: Qt.vector3d(0, 0, 0)
             p3: Qt.vector3d(55, 55, 0)
             onHighlightedChanged: {
                 if (highlighted && modelRect.pickable && activeCommand === Commands.None) {
                     picked = !picked
                 }
             }
         }
         materials: PrincipledMaterial {
             id: material
             baseColor: re.highlighted || re.picked ? colorHighlighted : color1
             lighting: PrincipledMaterial.NoLighting
         }
         pickable: true
         //property bool isPicked: false
     }

    Square {
        id: squareP0
        center: re.p0
        visible: re.picked
    }

    function receiveEvent(evt, p, myParent){
        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {
            re.p0 = p
            re.p1 = p
            re.p2 = p
            re.p3 = p
            root.visible = true
            return EntitiesQML.Start
        }

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            re.p1 = Qt.vector3d(p.x, re.p0.y, re.p0.z) //check z value
            re.p2 = p
            re.p3 = Qt.vector3d(re.p0.x, p.y, re.p0.z) //check z value
            return EntitiesQML.Update
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {
            visible = false
            myParent.addRectangle();
            return EntitiesQML.Finish
        }
        return -1;
    }


    function highLight(mousePoint) {
        //console.log("function highLight(mousePoint) in Rectangle old-------------------------------------")
        re.highlighted = !re.highlighted
        //TODO: change this to a proper highlight function, just checking if it works
    }
}
