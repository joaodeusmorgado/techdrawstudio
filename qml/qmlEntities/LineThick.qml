import QtQuick 2.12
import QtQuick3D //1.15
import Entity_LineThick 1.0
import LineTexture 1.0
//import QMLEnumEvents 1.0
import "../qmlEnums"

EntityTemplate {
    id: rootLineThick
    property alias p0: line.p0
    property alias p1: line.p1
    property alias picked: line.picked
    property alias thick: line.thick
    property alias pickInterval: line.pickInterval

    property alias pickPoint: line.pickPoint
    property alias zoomScale: tex.scaleU

    onPickedChanged: /*function(picked)*/{
        if (!picked)
            return

        if (activeCommand === Commands.Fillet || activeCommand === Commands.Extend || activeCommand === Commands.Trim) {
            console.log("sendInfo: "+rootLineThick)
            sendInfo(rootLineThick)
        }
    }

    // commands---------------------
    property vector3d lineP0Aux
    property vector3d lineP1Aux
    property vector3d nodePosAux

    onCommandCprStart: function(pos) {
        if (!picked)
            return
        //console.log("onCommandCprStart activeCommand:"+activeCommand)
        if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Scale){
            lineP0Aux = line.p0
            lineP1Aux = line.p1
            nodePosAux = rootLineThick.position
            //commandInitialPos = pos
        }

        if (activeCommand === Commands.Copy) {
            parent.addLine(line.p0, line.p1)
            console.log("copy line--------------------------------------------------------")
        }

        if (activeCommand === Commands.Offset) {
            console.log("Do a offset to the line.....................................")
            mathLib.setOffset(offset);
            mathLib.offsetLineCalculate(line.p0, line.p1, pos)
            console.log("line.p0: "+line.p0)
            console.log("line.p1: "+line.p1)
            console.log("mathlib.offsetLineP0(): "+ mathLib.offsetLineP0())
            console.log("mathlib.offsetLineP1(): "+ mathLib.offsetLineP1())
            parent.addLine(mathLib.offsetLineP0(), mathLib.offsetLineP1())
        }
    }

    onCommandCprUpdate: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Rotate && cprCounter === 1){

            if (osnapON)
                rotateStartPoint = osnapPoint
            else
                rotateStartPoint = pos
            rotateStartAngle = mathLib.angleFromPoints(commandInitialPos, rotateStartPoint)
            lineP0Aux = line.p0
            lineP1Aux = line.p1
            //nodePosAux = rootLineThick.position
            //console.log("set rotate start point cprCounter: "+cprCounter)
        }

        /*if (activeCommand === Commands.Scale && cprCounter === 1) {
            if (osnapON)
                scaleStartPoint = osnapPoint
            else
                scaleStartPoint = pos
            lineP0Aux = line.p0
            lineP1Aux = line.p1
        }*/

    }//signal commandCprUpdate(pos: vector3d, cprCounter: int)

    onCommandMtm: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){

            commandUpdatePosition = pos

            distAux = mathLib.sub3D(commandUpdatePosition, commandInitialPos)
            distAuxNode = mathLib.sub3D(commandUpdatePosition, commandInitialPos)

            rootLineThick.position = mathLib.sum3D(nodePosAux, distAuxNode)
            //line.p0 = mathLib.sum3D(lineP0Aux, distAux)
            //line.p1 = mathLib.sum3D(lineP1Aux, distAux)

            //line.updateP0P1(mathLib.sum3D(lineP0Aux, distAux), mathLib.sum3D(lineP1Aux, distAux))
        }

        if (activeCommand === Commands.Scale ){

            var dist = (pos.y - scaleStartPoint.y)
            var sca = (pos.y - scaleStartPoint.y)/50

            line.p0 = lib.scale2DfromPoint(scaleStartPoint, sca, lineP0Aux)
            line.p1 = lib.scale2DfromPoint(scaleStartPoint, sca, lineP1Aux)
        }


        if (activeCommand === Commands.Rotate && cprCounter === 2){

            //console.log("set rotate update point")

            if (osnapON)
                pos = osnapPoint

            rotateUpdateAngle = mathLib.angleFromPoints(commandInitialPos, pos)

            var angle = rotateUpdateAngle - rotateStartAngle


            line.p0 = mathLib.rotate2DFromPoint(commandInitialPos, angle, lineP0Aux)
            line.p1 = mathLib.rotate2DFromPoint(commandInitialPos, angle, lineP1Aux)

        }
    }

    onCommandCprFinish: function(pos){


        if (activeCommand === Commands.Selection) {

            if (commandInitialPos.x < pos.x) {

                if (mathLib.segmentFullyInsideRectangle(line.p0, line.p1, commandInitialPos, pos)) {
                    picked = true
                }
            }

            if (commandInitialPos.x > pos.x) {

                if (mathLib.segmentFullyInsideRectangle(line.p0, line.p1, commandInitialPos, pos)) {
                    picked = true
                }
                //if (mathLib.segmentIntersectRectangle(line.p0, line.p1, commandInitialPos, pos))
                if (mathLib.checkLineRectangleIntersection(line.p0, line.p1, commandInitialPos, pos)){
                    picked = true
                }
            }
        }

        if (!picked)
            return
        console.log("onCommandCprFinish....................................................")

        //line.p0 = mathLib.sum3D(lineP0Aux, distAux)
        //line.p1 = mathLib.sum3D(lineP1Aux, distAux)

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move) {
            line.updateP0P1(mathLib.sum3D(lineP0Aux, distAux), mathLib.sum3D(lineP1Aux, distAux))
            rootLineThick.position = Qt.vector3d(0,0,0)
        }


    }

    //commands---------------------------

  //  property alias instancing: modelLine.instancing

    property bool pickableCommands: activeCommand === Commands.None || activeCommand === Commands.Fillet
                                    || activeCommand === Commands.Extend || activeCommand === Commands.Trim
  //  Model {
     //     id: modelLine
    geometry: EntityLineThick {
        id: line
        pickInterval: thick + 4
        onPickedChanged: function(newPicked) {rootLineThick.pickCounter(newPicked)}
        onHighlightedChanged: {
        //console.log("line onHighlightedChanged: "+highlighted)
        //  console.log("modelLine.pickable: "+modelLine.pickable)
        //console.log("activeCommand: "+activeCommand)
        //if (highlighted && modelLine.pickable && activeCommand === Commands.None) {
            if (highlighted && pickable && pickableCommands ) {
                picked = !picked
            }
        }
    }//EntityLineThick

    materials: DefaultMaterial {
        id: material
        //lineWidth: 4.0;
        lighting:  DefaultMaterial.NoLighting
        //diffuseColor: line.isPicked ? "blue" : "red"
        diffuseMap: Texture {
            id: tex
            textureData: LineTexture {
                id: lineTex
                color1: line.highlighted ? colorHighlighted : line.picked ? colorPicked : rootLineThick.color1
                color2: line.highlighted ? colorHighlighted : line.picked ? colorPicked : rootLineThick.color1
                //color2: line.highlighted || line.picked ? colorHighlighted : rootLineThick.color2
            }
            magFilter: Texture.Nearest
            minFilter: Texture.Nearest
            mappingMode: Texture.UV
            scaleU: 4
            scaleV: 1
        }
    }//DefaultMaterial

    Square {
        id: squareP0
        center: line.p0
        visible: (line.picked || squareP0_OsnapON) && activeCommand !== Commands.Fillet
    }

    Square {
        id: squareP1
        center: line.p1
        visible: (line.picked || squareP1_OsnapON) && activeCommand !== Commands.Fillet
    }

    Square {
        id: squareMid
        center: Qt.vector3d(line.p0.x*0.5+line.p1.x*0.5, line.p0.y*0.5+line.p1.y*0.5, line.p0.z*0.5+line.p1.z*0.5)
        visible: (line.picked || squareMid_OsnapON) && activeCommand !== Commands.Fillet
    }

    Triangle {
        id: triangleP0
        pos: line.p1
        angleP0: line.p0
        angleP1: line.p1
        visible: line.picked && activeCommand !== Commands.Fillet
    }

    Triangle {
        id: triangleP1
        pos: line.p0
        angleP0: line.p1
        angleP1: line.p0
        visible: line.picked && activeCommand !== Commands.Fillet
    }

    function receiveEvent(evt, p: vector3d, myParent){

       // console.log("receiveEvent()-------------------------------------------------------------------")
        //console.log("////////////////////////////////////////////////////////")
        //console.log("EntitiesQML.Start: "+EntitiesQML.Start)
        //console.log("entityCreationDef.isStart(evt): "+entityCreationDef.isStart(evt))



        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {
            //console.log("entityCreationDef.isStart: "+p)
            //console.log("EntitiesQML.Start: "+EntitiesQML.Start)

            //set mouseWorldPosition to p to avoid a bug in android / mobile
            //where drawing a line would always default to osnapPoint
            mouseWorldPosition = p


            if (osnapON) {
                //console.log("starting line with osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = osnapPoint
                line.updateP0P1(osnapPoint, osnapPoint);
            }
            else {
                //console.log("starting line without osnapON - p:" + p + " osnapPoint: "+osnapPoint)
                //line.p0 = line.p1 = p
                line.updateP0P1(p, p);
            }
            //console.log("start line---------------------: "+line.p0)

            rootLineThick.visible = true
            return EntitiesQML.Start
        }//isStart

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            //console.log("updateline-----------------------")

            if (orthoMode) {
                if (mathLib.distance2D(p0.x, p.x) > mathLib.distance2D(p0.y, p.y))
                    //line.p1 = Qt.vector3d(p.x, line.p0.y, line.p1.z)
                    line.updateP0P1(line.p0, Qt.vector3d(p.x, line.p0.y, line.p1.z));
                else
                    //line.p1 = Qt.vector3d(line.p0.x, p.y, line.p1.z)
                    line.updateP0P1(line.p0, Qt.vector3d(line.p0.x, p.y, line.p1.z));
            }
            else {
                //line.p1 = p
                line.updateP0P1(line.p0, p);
            }//if (orthoMode)

            return EntitiesQML.Update
        }//isUpdate

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {
            //console.log("entityCreationDef.isFinish: "+p)
            //console.log("EntitiesQML.Finish: "+EntitiesQML.Finish)

            if (line.p0 == line.p1)
                return

           if (osnapON) {
               line.p1 = osnapPoint
           }
           osnapON = false

           //visible = false
           parent.addLine(line.p0, line.p1);
           line.picked = false
           //picked = false
           rootLineThick.visible = false
           //line.p0 = line.p1 = Qt.vector3d(0, 0, 0)
           return EntitiesQML.Finish

        }//isFinish

        //console.log("no line event caught -------------------------------------")
        return -1;

    }//function receiveEvent(evt, p: vector3d, myParent)

    property bool squareP0_OsnapON: false
    property bool squareP1_OsnapON: false
    property bool squareMid_OsnapON: false

    onSquareP0_OsnapONChanged: {
        //console.log("onSquareP0_OsnapONChanged: "+squareP0_OsnapON)
        osnapON = squareP0_OsnapON
    }
    onSquareP1_OsnapONChanged: {
        //console.log("onSquareP1_OsnapONChanged: "+squareP1_OsnapON)
        osnapON = squareP1_OsnapON
    }
    onSquareMid_OsnapONChanged: {
        //console.log("onSquareMid_OsnapONChanged: "+squareMid_OsnapON)
        osnapON = squareMid_OsnapON
    }


    function checkOsnap(mp: vector3d) {
        //console.log(this + " checkOsnap() point"+osnapPoint)
        if (!checkingOsnap)
            return;

        if (mathLib.distance3DDelta(mp, squareP0.center, osnapDistance)) {
            squareP0_OsnapON = true
            osnapPoint = squareP0.center
            return;
        }
        else {
            squareP0_OsnapON = false
        }

        if (mathLib.distance3DDelta(mp, squareP1.center, osnapDistance)) {
            //console.log("checkOsnap line id:" +rootLineThick)
            squareP1_OsnapON = true
            osnapPoint = squareP1.center
            return;
        }
        else
            squareP1_OsnapON = false

        if (mathLib.distance3DDelta(mp, squareMid.center, osnapDistance)) {
            squareMid_OsnapON = true
            //console.log("distance3DDelta squareP0.Mid true")
            osnapPoint = squareMid.center
            return;
        }
        else
            squareMid_OsnapON = false

    }//function checkOsnap(mp: vector3d)

    function highLight(pos: vector3d){//mp - mouse position
        if (!line.inside_Pick(pos)) {
            line.highlighted = false;
            return;
        }
        line.highlighted = mathLib.checkPointInLine(p0, p1, pos, line.pickInterval)
        //pickedPos = pos


        /*
        TODO: check how to get distance when moving the mouse over the line
        if (activeCommand === Commands.Length){
            //console.log("onClick on Commands.Length --------------------------------------")
            var len = mathLib.distance3D(line.p0, line.p1)
            console.log("LineThick is gonna send Text ----------------------: ", len)
            console.log("line length: ", len)
            sendText(len)
        }*/
    } //function highLight(mp)

    onClick: function(pos) {
        //console.log("entity template click pos: " + pos)
        //console.log("linethick click pos: " + pos)

        //console.log("onClick --------------------------------------")

        if (activeCommand === Commands.Length){
            if (mathLib.checkPointInLine(p0, p1, pos, line.pickInterval)){
                console.log("onClick on Commands.Length --------------------------------------")
                var len = mathLib.distance3D(line.p0, line.p1)
                console.log("LineThick is gonna send Text ----------------------: ", len)
                console.log("line length: ", len)
                sendText(len)
            }
        }


        console.log("onClick line thick.......")
        if (activeCommand !== Commands.None)
            return;

        if (!line.inside_Pick(pos)) {
            return false;
        }

        if (mathLib.checkPointInLine(p0, p1, pos, line.pickInterval)){
            line.picked = !line.picked
            //pickedPos = pos
        }
    }

    /*onGenericCommand: function(command) {

        if (command === Commands.Length) {
            var len = mathLib.distance3D(line.p0, line.p1)
            console.log("line.p0: ", line.p0)
            console.log("line.p1: ", line.p1)
            console.log("line length: ", len)
            sendText(len)
            //return len
        }
    }*/

   // property vector3d intersec

    /*onExecuteComm2: function(otherEntity){
        console.log("EXECUTE FILLET---------------------------------------------------------------------")
        //console.log("otherEntity.pickedPos: "+otherEntity.pickedPos)
        console.log("otherEntity.p0: "+otherEntity.p0)
        console.log("otherEntity.p1: "+otherEntity.p1)
    }*/

}
