import QtQuick 2.12
import QtQuick3D //1.15
import Entity_RectangleThick 1.0
import "../qmlEnums"

EntityTemplate {
    id: root
    property alias p0: re.p0
    property alias p1: re.p1
    property alias p2: re.p2
    property alias p3: re.p3
    property alias picked: re.picked
    property alias thick: re.thick
    property alias pickInterval: re.pickInterval
  //  property alias pickable: modelRect.pickable

    // commands ---------------------
    property vector3d rectP0aux
    property vector3d rectP1aux
    property vector3d rectP2aux
    property vector3d rectP3aux

    onCommandCprStart: function(pos) {
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){
            rectP0aux = re.p0
            rectP1aux = re.p1
            rectP2aux = re.p2
            rectP3aux = re.p3
            commandInitialPos = pos
        }

        if (activeCommand === Commands.Copy)
            parent.addRectangle(re.p0, re.p1, re.p2, re.p3)

        if (activeCommand === Commands.Offset) {
            console.log("Do a offset to the rectangle.....................................")
            mathLib.setOffset(offset);
           // mathLib.offsetLineCalculate(line.p0, line.p1, pos)
            //console.log("line.p0: "+line.p0)
            //console.log("line.p1: "+line.p1)
            //console.log("mathlib.offsetLineP0(): "+ mathLib.offsetLineP0())
            //console.log("mathlib.offsetLineP1(): "+ mathLib.offsetLineP1())
            //parent.addLine(mathLib.offsetLineP0(), mathLib.offsetLineP1())
        }
    }

    onCommandCprUpdate: function(pos, cprCounter){

        if (activeCommand === Commands.Rotate && cprCounter === 1){

            if (osnapON)
                rotateStartPoint = osnapPoint
            else
                rotateStartPoint = pos
            rotateStartAngle = mathLib.angleFromPoints(commandInitialPos, rotateStartPoint)
            rectP0aux = re.p0
            rectP1aux = re.p1
            rectP2aux = re.p2
            rectP3aux = re.p3           
        }

    }//signal commandCprUpdate(pos: vector3d, cprCounter: int)

    onCommandMtm: function(pos, cprCounter){
        if (!picked)
            return

        if (activeCommand === Commands.Copy || activeCommand === Commands.Move){
            commandUpdatePosition = pos
            distAux = mathLib.sub3D(commandUpdatePosition, commandInitialPos)
            re.p0 = mathLib.sum3D(rectP0aux, distAux)
            re.p1 = mathLib.sum3D(rectP1aux, distAux)
            re.p2 = mathLib.sum3D(rectP2aux, distAux)
            re.p3 = mathLib.sum3D(rectP3aux, distAux)
        }

        if (activeCommand === Commands.Rotate && cprCounter === 2){

            //console.log("set rotate update point")
            if (osnapON)
                pos = osnapPoint

            rotateUpdateAngle = mathLib.angleFromPoints(commandInitialPos, pos)

            var angle = rotateUpdateAngle - rotateStartAngle

            re.p0 = mathLib.rotate2DFromPoint(commandInitialPos, angle, rectP0aux)
            re.p1 = mathLib.rotate2DFromPoint(commandInitialPos, angle, rectP1aux)
            re.p2 = mathLib.rotate2DFromPoint(commandInitialPos, angle, rectP2aux)
            re.p3 = mathLib.rotate2DFromPoint(commandInitialPos, angle, rectP3aux)
        }

    }
    // commands ---------------------

    geometry: EntityRectangleThick {
        id: re
        p0: Qt.vector3d(0, 0, 0)
        p3: Qt.vector3d(55, 55, 0)
        onPickedChanged: function(newPicked) {root.pickCounter(newPicked)}
        pickInterval: thick + 4
        onHighlightedChanged: {
             //if (highlighted && modelRect.pickable && activeCommand === Commands.None) {
            if (highlighted && pickable && activeCommand === Commands.None) {
                picked = !picked
            }
        }
    }

    materials: PrincipledMaterial {
        id: material
        baseColor: re.highlighted || re.picked ? colorHighlighted : color1
        lighting: PrincipledMaterial.NoLighting
        cullMode: Material.NoCulling
    }
    //pickable: true
         //property bool isPicked: false
    // }//Model

    Square {
        id: rect_squareP0
        center: re.p0
        visible: re.picked || osnapON_rect_r0
    }

    Square {
        id: rect_squareP1
        center: re.p1
        visible: re.picked || osnapON_rect_r1
    }

    Square {
        id: rect_squareP2
        center: re.p2
        visible: re.picked || osnapON_rect_r2
    }

    Square {
        id: rect_squareP3
        center: re.p3
        visible: re.picked || osnapON_rect_r3
    }

    function receiveEvent(evt, p, myParent){

        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {

            if (osnapON) {
                re.p0 = re.p1 = re.p2 = re.p3 = osnapPoint
            }
            else {
                re.p0 = re.p1 = re.p2 = re.p3 = p
            }
            root.visible = true
            return EntitiesQML.Start
        }

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            re.p1 = Qt.vector3d(p.x, re.p0.y, re.p0.z) //check z value
            re.p2 = p
            re.p3 = Qt.vector3d(re.p0.x, p.y, re.p0.z) //check z value
            return EntitiesQML.Update
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {

            if (osnapON) {
                re.p1 = Qt.vector3d(osnapPoint.x, re.p0.y, re.p0.z) //check z value
                re.p2 = osnapPoint
                re.p3 = Qt.vector3d(re.p0.x, osnapPoint.y, re.p0.z) //check z value
            }

            visible = false
            parent.addRectangle(re.p0, re.p1, re.p2, re.p3);
            return EntitiesQML.Finish
        }//isFinish

        return -1;
    }//receiveEvent

    property bool osnapON_rect_r0: false
    property bool osnapON_rect_r1: false
    property bool osnapON_rect_r2: false
    property bool osnapON_rect_r3: false

    onOsnapON_rect_r0Changed: osnapON = osnapON_rect_r0
    onOsnapON_rect_r1Changed: osnapON = osnapON_rect_r1
    onOsnapON_rect_r2Changed: osnapON = osnapON_rect_r2
    onOsnapON_rect_r3Changed: osnapON = osnapON_rect_r3

    function checkOsnap(mp: vector3d) {
        //console.log(this + " checkOsnap() point"+osnapPoint)
        if (!checkingOsnap)
            return;

        if (mathLib.distance3DDelta(mp, rect_squareP0.center, osnapDistance)) {
            osnapON_rect_r0 = true
            osnapPoint = rect_squareP0.center
            return;
        }
        else {
            osnapON_rect_r0 = false
        }

        if (mathLib.distance3DDelta(mp, rect_squareP1.center, osnapDistance)) {
            osnapON_rect_r1 = true
            osnapPoint = rect_squareP1.center
            return;
        }
        else {
            osnapON_rect_r1 = false
        }

        if (mathLib.distance3DDelta(mp, rect_squareP2.center, osnapDistance)) {
            osnapON_rect_r2 = true
            osnapPoint = rect_squareP2.center
            return;
        }
        else {
            osnapON_rect_r2 = false
        }

        if (mathLib.distance3DDelta(mp, rect_squareP3.center, osnapDistance)) {
            osnapON_rect_r3 = true
            osnapPoint = rect_squareP3.center
            return;
        }
        else {
            osnapON_rect_r3 = false
        }

    }

    function highLight(mousePoint: vector3d) {
        //console.log("function highLight(mousePoint) in RectangleThick-------------------------------------")
        //re.highlighted = !re.highlighted

        if (!re.inside_Pick(mousePoint)) {
            re.highlighted = false;
            return;
        }

        if ( mathLib.checkPointInLine(p0, p1, mousePoint, re.pickInterval)
                || mathLib.checkPointInLine(p1, p2, mousePoint, re.pickInterval)
                || mathLib.checkPointInLine(p2, p3, mousePoint, re.pickInterval)
                || mathLib.checkPointInLine(p3, p0, mousePoint, re.pickInterval)
                )
            re.highlighted = true
        else
            re.highlighted = false
    }//function highLight(mousePoint: vector3d)

    onClick: function(pos) {
        //console.log("entity template click pos: " + pos)
        console.log("rect click pos: " + pos)
        if (activeCommand !== Commands.None)
            return;

        if (!re.inside_Pick(pos)) {
            return false;
        }
        console.log("rect click pos return true")

        if ( mathLib.checkPointInLine(p0, p1, pos, re.pickInterval)
                || mathLib.checkPointInLine(p1, p2, pos, re.pickInterval)
                || mathLib.checkPointInLine(p2, p3, pos, re.pickInterval)
                || mathLib.checkPointInLine(p3, p0, pos, re.pickInterval)
                )
            re.picked = !re.picked
    }
}
