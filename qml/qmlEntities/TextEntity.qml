import QtQuick 2.0
import QtQuick3D //1.15

EntityTemplate {
    id: root
    property alias text: tex.text
    property alias fontpixelSize: tex.font.pixelSize
    //property vector3d p0

    Text {
        id: tex
        //anchors.centerIn: parent
        //width: 300
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignJustify
        font.pixelSize: 14
        color: highLighting ? colorHighlighted : "yellow"
        //style: Text.Raised
        text: ""
    }

    property bool highLighting: false
    property vector3d mousePosition

    onMousePositionChanged: {
        //console.log("picking:" + mousePosition)
        highLight(mousePosition)
    }


    function highLight(mp: vector3d) {

        if ( mp.x <= root.x+tex.width &&  mp.x >= root.x &&
             //mp.y <= root.y+tex.height &&  mp.y >= root.y
                mp.y >= root.y &&  mp.y <= root.y+tex.height

                )
        {
            highLighting = true
            //console.log("highLight:" + highLighting)
        }
        else {
            highLighting = false
            //console.log("highLight:" + highLighting)
        }

    }


    function receiveEvent(evt, p, myParent){
        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {
            //p0 = p
            root.position = p
            //root.visible = true
            return EntitiesQML.Start
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {
            root.visible = false
            //myParent.addText();
            return EntitiesQML.Finish
        }
        return -1;
    }
}
