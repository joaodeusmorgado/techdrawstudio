import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Rectangle 1.0
import Entity_LineThick 1.0
import "../"
//Triangle is used for osnap

Node {
    id: root
    property vector3d pos

    property real d: 2*mm / mainCamera.zoomCamera
    property real d2: d * 0.5 // half dist
    property real h: Math.sqrt(d*d + d2*d2)
    property real hOffset: defaultMargins * 2 / mainCamera.zoomCamera
    property int layerNumber: 0//change later
    property real thick: 2
    property color colorSquare: "lightblue"

    //get the angle from two points angleP0, angleP1
    //theese points are the entity line points
    property vector3d angleP0
    property vector3d angleP1
    //property vector3d zero: Qt.vector(0,0,0)
    property real angle: mathLib.angleFromPoints(angleP0, angleP1)

   /*
              t0
           d2 | \ d
              |  \
   pos----pos0|    t1
      hOffset |  /
           d2 | / d
              t2
    */

    property vector3d t0: Qt.vector3d(hOffset, d2, 0)//top point
    property vector3d t1: Qt.vector3d(hOffset + h, 0, 0)//horizontal point
    property vector3d t2: Qt.vector3d(hOffset, -d2, 0)//bottom point

    property vector3d t0Rotated: mathLib.rotate2d(t0, angle)
    property vector3d t1Rotated: mathLib.rotate2d(t1, angle)
    property vector3d t2Rotated: mathLib.rotate2d(t2, angle)

    Model {
        id: l0
         geometry: EntityLineThick {
             id: line0
             p0: mathLib.sum3D(pos, t0Rotated)
             p1: mathLib.sum3D(pos, t2Rotated)
             thick: root.thick / mainCamera.zoomCamera
         }
         materials: DefaultMaterial {
             diffuseColor: colorSquare
             lighting: DefaultMaterial.NoLighting
         }
    }

    Model {
        id: l1
         geometry: EntityLineThick {
             id: line1
             p0: mathLib.sum3D(pos, t0Rotated)
             p1: mathLib.sum3D(pos, t1Rotated)
             thick: root.thick / mainCamera.zoomCamera
         }
         materials: DefaultMaterial {
             diffuseColor: colorSquare
             lighting: DefaultMaterial.NoLighting
         }
     }

    Model {
        id: l2
         geometry: EntityLineThick {
             id: line2
             p0: mathLib.sum3D(pos, t2Rotated)
             p1: mathLib.sum3D(pos, t1Rotated)
             thick: root.thick / mainCamera.zoomCamera
         }
         materials: DefaultMaterial {
             diffuseColor: colorSquare
             lighting: DefaultMaterial.NoLighting
         }
     }

}
