import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Square 1.0


//Square is used for osnap

Node {
    id: root
    property alias center: squa.center
    property alias squareDist: squa.squareDist
    property alias colorSquare: mat.diffuseColor

    //property color colorSquare: "lightblue"

    Model {
         geometry: EntitySquare {
             id: squa
             //center: root.center
             squareDist: mm / mainCamera.zoomCamera
         }
         materials: DefaultMaterial {
             id: mat
             diffuseColor: "lightblue"
             lighting: DefaultMaterial.NoLighting
         }
     }
}
