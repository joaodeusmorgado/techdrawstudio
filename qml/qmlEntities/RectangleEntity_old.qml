import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Rectangle 1.0

Node {
    id: root
    /*
    property vector3d p0
    property vector3d p3

    property color layerColor: "white"
    property color noLayerColor: "white"
    property bool isByLayerColor: false
    property int layerNumber: 0
    property real thick: 1

    property bool pickable: false

    property vector3d mousePosition
    onMousePositionChanged: {
        //console.log("picking:" + mousePosition)
        l0.highLight(mousePosition)
        l1.highLight(mousePosition)
        l2.highLight(mousePosition)
        l3.highLight(mousePosition)
    }

    LineThick {
        id: l0
        p0: root.p0
        p1: Qt.vector3d(root.p3.x , root.p0.y, root.p0.z)
        layerColor: root.layerColor
        noLayerColor: root.noLayerColor
        isByLayerColor: root.isByLayerColor
        layerNumber: root.layerNumber
        thick: root.thick
        mousePosition: root.mousePosition
        pickable: root.pickable
    }

    LineThick {
        id: l1
        p0: root.p0
        p1: Qt.vector3d(root.p0.x, root.p3.y, root.p0.z)
        layerColor: root.layerColor
        noLayerColor: root.noLayerColor
        isByLayerColor: root.isByLayerColor
        layerNumber: root.layerNumber
        thick: root.thick
        mousePosition: root.mousePosition
        pickable: root.pickable
    }

    LineThick {
        id: l2
        p0: Qt.vector3d(root.p3.x , root.p0.y, root.p0.z)
        p1: root.p3
        layerColor: root.layerColor
        noLayerColor: root.noLayerColor
        isByLayerColor: root.isByLayerColor
        layerNumber: root.layerNumber
        thick: root.thick
        mousePosition: root.mousePosition
        pickable: root.pickable
    }

    LineThick {
        id: l3
        p0: Qt.vector3d(root.p0.x, root.p3.y, root.p0.z)
        p1: root.p3
        layerColor: root.layerColor
        noLayerColor: root.noLayerColor
        isByLayerColor: root.isByLayerColor
        layerNumber: root.layerNumber
        thick: root.thick
        mousePosition: root.mousePosition
        pickable: root.pickable
    }

    function receiveEvent(evt, p, myParent){
        if (entityCreationDef.isStart(evt) === EntitiesQML.Start) {
            p0 = p
            p3 = p
            root.visible = true
            return EntitiesQML.Start
        }

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update) {
            p3 = p
            return EntitiesQML.Update
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish) {
            root.visible = false
            myParent.addRectangle();
            return EntitiesQML.Finish
        }
        return -1;
    }
*/
}
