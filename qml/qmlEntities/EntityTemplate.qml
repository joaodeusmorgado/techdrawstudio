import QtQuick 2.0
import QtQuick3D
import QtQml
import "../qmlEnums"

//Node {
Model {
    id: root

    property color color1: isByLayerColor ? layerColor : noLayerColor
    property color color2: isByLayerColor ? layerColor : noLayerColor
    property color layerColor: "red"
    property color noLayerColor: "red"
    property color colorHighlighted: "#FF5733"
    property color colorPicked: "orange"
    property bool isByLayerColor: false
    property int layerNumber: 0
    property vector3d mousePosition
    //property vector3d pickedPos

    //used for comands copy, move, rotate
    property vector3d commandInitialPos
    property vector3d commandUpdatePosition
    property vector3d distAux
    property vector3d distAuxNode


    //used for command rotate
    property vector3d rotateStartPoint
    property vector3d rotateUpdatePoint
    property real rotateStartAngle
    property real rotateUpdateAngle

    //used for command scale
    property vector3d scaleStartPoint
    property vector3d scaleUpdatePoint

    //used for commands2 - commands that interact with other entities, like Fillet, Extend, ...
    property int comm2InteractionCount: 0
    property vector3d otherEntityPosition

    visible: layerEditor.layersModel.get(layerNumber).layerVisible

    //define signal handlers in the derived classes (for example LineThick, CircleThick, ...)
    signal click(pos: vector3d)
    signal clickAtStart()//for initialization
    signal commandCprStart(pos: vector3d)
    signal commandCprUpdate(pos: vector3d, cprCounter: int)
    signal commandCprFinish(pos: vector3d)
    signal commandMtm(pos: vector3d, cprCounter: int)
    signal genericCommand(command: int)//for example: Delete, UnPick
    signal requestInfo()//to be requested from commands like fillet, ...
    signal sendInfo(thisEntity: var)//send this entity info for commands like fillet, ...
    signal sendText(str: var)
   // signal executeComm2(otherEntity: var)//apply command2, for example fillet, extend, ...

    signal prepareToDestroy()
    onPrepareToDestroy: {
        if (picked){
            picked = false
            countEntities--
            root.destroy()
        }
    }

    onRequestInfo: {
        sendInfo(root)
    }

    onGenericCommand: function(command) {

        if (command === Commands.UnPick) {
            picked = false
        }

        if (command === Commands.Delete) {
            prepareToDestroy()
        }

    }

    onClick: function(pos) {
        //highLight(pos)

    }

    //ma.clickPos.connect(newLine.click)
    Connections {
        id: connect
        target: ma
        function onClickPos(pos){
            click(pos)
            //console.log("onClickPos: "+ pos)
        }
        enabled: false
    }

    //this connection is a hack to avoid that the line when created, starts picked
    // a click in the line will pick it, another click will unpick it
    //since the lines are created with clicks, the first click will make the line picked, we dont want that
    property int connectAtSecondClick: 0
    property alias connecAtStartEnabled: connecAtStart.enabled
    Connections{
        id: connecAtStart
        target: ma
        function onClickPos(pos){
            if (connectAtSecondClick === 1) {
                connect.enabled = true
                connecAtStart.enabled = false
            }
            connectAtSecondClick++
        }
        enabled: false
    }


    onCommandCprStart: function(pos) {

        if (osnapON)
            commandInitialPos = osnapPoint
        else
            commandInitialPos = pos

        if (activeCommand === Commands.Scale){
            scaleStartPoint = pos
            //console.log("set rotate center point pos: "+pos)
        }
    }

    //signal pick() is used to increment the number of picked entities
    // if picked entites gretar than 0, show the menu Commands
    signal pickCounter(bool bOnOff)

    onMousePositionChanged: {
        //console.log("onMousePositionChanged: " + this + " picking:" + mousePosition)

        checkOsnap(mousePosition)
        highLight(mousePosition)
    }

    function checkOsnap(mp: vector3d) {
        console.log("function checkOsnap(mp: vector3d) - EntityTemplate.qml")
    }

    function highLight(mp: vector3d){//mp - mouse position
        console.log("function highLight(mousePoint) in template-------------------------------------")
    }


}
