import QtQuick 2.12
import QtQuick3D //1.15
import Entity_Line 1.0
import LineTexture 1.0

Node {

    //Note: Deprecated this and used LineThick instead
    property alias p0: line.p0
    property alias p1: line.p1
    property alias color1: lineTex.color1
    property alias color2: lineTex.color2
    property alias lineWidth: material.lineWidth
    property alias pickable: modelLine.pickable

    Model {
        id: modelLine
        geometry: EntityLine {
            id: line
            p0: Qt.vector3d(-55, -55, 50)
            p1: Qt.vector3d(55, 55, 50)
        }

        materials: DefaultMaterial {
            id: material
            lineWidth: 10.0;
            lighting:  DefaultMaterial.NoLighting
            diffuseMap: Texture {
             textureData: LineTexture {
                 id: lineTex
                 //color1: "blue"
                 //color2: "white"
             }
             magFilter: Texture.Nearest
             minFilter: Texture.Nearest
             mappingMode: Texture.UV
             scaleU: 8
             scaleV: 1
            }
        }

        pickable: true
        property bool isPicked: false
     }
}
