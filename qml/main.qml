import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick3D //1.15
import QtQuick.Controls 2.14
import QtQuick3D.Helpers 1.15
import QtCore
//import Qt.labs.settings 1.0
//import Entity_Line 1.0
//import Entity_Circle 1.0
//import Entity_LineThick 1.0
import MathLib 1.0
import "qmlEntities"
import "components"
import "pagesUI"
//import LineTexture 1.0
//import QMLEnumEvents 1.0

//ApplicationWindow {
ApplicationWindow {
    id: mainRoot
    visible: true
    //width: 1080//Screen.width
    //height: 720//Screen.height
    width: 800
    height: 600
    property real maxWidthHeight: width > height ? width : height

    property int listEditorWidth: 54*mm//320
    property int listEditorHeight: 58*mm//380

    title: qsTr("TechDrawStudio")

    //visibility: Qt.WindowMaximized

    //Label {
        //text: "OpenGL: " + OpenGLInfo.majorVersion + ' ' + OpenGLInfo.minorVersion + OpenGLInfo.profile
    //}

    /*
      https://doc.qt.io/qt-6/qml-qtqml-qt.html#platform-prop
    Qt.platform.os values
    "android" - Android
    "ios" - iOS
    "tvos" - tvOS
    "linux" - Linux
    "osx" - macOS
    "qnx" - QNX (since Qt 5.9.3)
    "unix" - Other Unix-based OS
    "windows" - Windows
    "wasm" - WebAssembly
*/

    property int countEntities

    //global properties
    property real calibrationFactor: 1//.5
    property real mm: Screen.pixelDensity * calibrationFactor
    property real defaultMargins: 2 * mm
    property real btnSizeSmall: 4 * mm
    property real btnSize: 7 * mm
    property real btnBorderWidth: btnSize*0.05
    property real btnHeight: 7 * mm
    property real btn2Size: 10 * mm

    //degreeRadGrad = Math.PI / 180 // degree
    //degreeRadGrad = 1 // rad
    property int isDegreeRadGrad // todo: add editor for this
    property real degreeRadGrad: Math.PI/180

    Component.onCompleted: {
        //showMaximized() // same as visibility: Qt.WindowMaximized

        console.log("Qt.platform.os: "+Qt.platform.os)

        if (Qt.platform.os === "windows" || Qt.platform.os === "linux" || Qt.platform.os === "osx") {
            calibrationFactor = 1.5
        }
    }

    EntityCreationDef{
        id: entityCreationDef
    }

    ColorsUI {
        id: colorsUI
    }

    MainView {
        id: mainView
        anchors.fill: parent
    }

    MyLibrary {
        id: lib
    }

    MathLib {
        id: mathLib
    }

    Settings {
        property alias fingerCursorDist: mainView.fingerCursorDist
        property alias entityStart: entityCreationDef.entityStart
        property alias entityUpdate: entityCreationDef.entityUpdate
        property alias entityFinish: entityCreationDef.entityFinish
    }

}
