import QtQuick 2.15
import QtQuick3D
import "../"
import "../qmlEntities"
import "../qmlEnums"
/*
How this should work: create a Property Command_Copy,
connect the signals startCopy(), updateCopy(), endCopy()
with all entities (for example lines, circles, rectangles) equivalent signals
*/



/*
Generic command class

commands are started, updated or finished with clicks, press, or release events, and also mousemoves, touchmoves
cpr - cliks, press or relase events are cpr (click, press release)
mtm - mouse moves ou touch moves are mtm (mouse, touch moves)

Example1:
command move: select start point->update->select finish point
cpr 0 - start move
mtm - update position
cpr 1 - finish move
copy is a cprLevel 1, requires 2 cpr (0,1) - use 0 base index

Example2:
command rotate: select rotation base point -> select start rotation position -> update -> end rotation position
cpr 0 - set rotation base point
cpr 1 - set start rotation position
mtm - update rotation
cpr 2 - finish rotation
rotation is a cprLevel 2, requires 3 cpr (0,1,2) - use 0 base index

Example3:
command offset
cpr 0 - set side of the entity offset
offset is a cprLevel 0, requires only one cpr(0)
*/

Node {
    id: commandsEntities

    property int cprCounter: 0
    property int cpr: EventsQML.Press
    property int cprFinish: EventsQML.Release

    property int cprLevel: (activeCommand === Commands.Move || activeCommand === Commands.Copy
                            || activeCommand === Commands.Scale || activeCommand === Commands.Selection) ? 1 :
                            ( activeCommand === Commands.Rotate  ? 2 :
                            ( activeCommand === Commands.Offset  ? 0 : -1 ) )

    //signal startCommand(initialPosition: vector3d, command: int)
    //signal updateCommand(initialPosition: vector3d, command: int)
    //signal endCommand()

    signal commandCprStart(pos: vector3d)
    signal commandCprUpdate(pos: vector3d, cprCounter: int)
    signal commandCprFinish(pos: vector3d)
    signal commandMtm(pos: vector3d, cprCounter: int)

    function receiveEvent(evt: int, p: vector3d, command){
        //console.log("receiveEvent: command"+ command)

        if (evt === cpr ) {
            console.log("command cpr start / update ok: "+command)

            if (cprCounter === 0) {
                console.log("command cpr start ok: "+command)

                if (osnapON)
                    p = osnapPoint

                if (activeCommand === Commands.Selection){
                    rectSelection.p0 = rectSelection.p1 = rectSelection.p2 = rectSelection.p3 = p
                    commandCpr1.center = p
                    commandCpr1.visible = true
                    rectSelection.visible = true
                }

                if (cprLevel === 0) { //used for offset
                    commandCprStart(p)
                    return 1;
                }

                cprCounter++
                //startCommand(p, command)//to delete
                commandCprStart(p)
                commandCpr0.center = p
                commandCpr0.visible = true



                return 1;
            }

            if (cprCounter < cprLevel) {
                console.log("command cpr update ok: "+command)
                if (osnapON)
                    p = osnapPoint
                commandCprUpdate(p, cprCounter)
                commandCpr1.center = p
                commandCpr1.visible = true
                cprCounter++
                return 1;
            }

        }

        if (evt === EventsQML.MouseMove && (cprCounter > 0)) {
            //if (orthoMode) {
            console.log("command update ok: " + command)
            if (osnapON)
                p = osnapPoint
            //updateCommand(p, command)//to delete

            if (activeCommand === Commands.Selection) {
                commandCpr1.center = p

                if ( rectSelection.p0.x < rectSelection.p2.x )
                    rectSelection.color1 = "red"
                else
                    rectSelection.color1 = "green"

                rectSelection.p2 = p
                rectSelection.p1 = mathLib.rectangleP0P2GetP1(rectSelection.p0, rectSelection.p2)
                rectSelection.p3 = mathLib.rectangleP0P2GetP3(rectSelection.p0, rectSelection.p2)
            }

            if (activeCommand === Commands.Rotate){
                commandCpr1.center = p
            }

            commandMtm(p, cprCounter)
            return 1;
        }

        if (evt === cprFinish && (cprCounter === cprLevel)) {
            console.log("command Finish ok: " + command)
            if (osnapON)
                p = osnapPoint
            cprCounter = 0
            //endCommand()//to delete

            if (activeCommand === Commands.Selection){
                rectSelection.visible = false
            }
            commandCprFinish(p)
            commandCpr0.visible = false
            commandCpr1.visible = false
            return 1;
        }
        return -1;
    }

    Square {
        id: commandCpr0
        //p0
        colorSquare: "lightgreen"
        visible: false
    }

    Square {
        id: commandCpr1
        //p0
        colorSquare: "red"
        visible: false
    }

    RectangleThick {
        id: rectSelection

        //thick = Qt.binding(function() { return lineThickEditor.currentLineThick / mainCamera.zoomCamera })
        thick: 2 / mainCamera.zoomCamera

        /*p0: Qt.vector3d(0, 0, 0)
        p1: Qt.vector3d(50, 0, 0)
        p2: Qt.vector3d(50, 50, 0)
        p3: Qt.vector3d(0, 50, 0)
        */
        visible: false
    }

    /*function receiveEvent(evt, p: vector3d, command){
        //console.log("receiveEvent: command"+ command)

        if (entityCreationDef.isStart(evt) === EntitiesQML.Start
                && (cprCounter === 0)) {
            //console.log("command start ok: "+command)
            cprCounter++
            startCommand(p, command)
            return 1;
        }

        if (entityCreationDef.isUpdate(evt) === EntitiesQML.Update
                && (cprCounter === 1)) {
            //if (orthoMode) {
            //console.log("command update ok: " + command)
            updateCommand(p, command)
            return 1;
        }

        if (entityCreationDef.isFinish(evt) === EntitiesQML.Finish
                && (cprCounter === 1)) {
            //console.log("command Finish ok: " + command)
            cprCounter = 0
            endCommand()
            return 1;
        }
        return -1;
    }*/


}
