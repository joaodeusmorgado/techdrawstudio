﻿import QtQuick 2.15
import QtQuick3D
import "../qmlEnums"
import "../qmlEntities"

Node {

    property int cprCounter: 0
    property int cpr: EventsQML.Press
    property int cprFinish: EventsQML.Release

    //property int cprLevel: (activeCommand === Commands.Fillet ? 2 : 0)
    property int activeCommand_: rootMainView.activeCommand

    onActiveCommand_Changed: {

        if (activeCommand === Commands.None) {
            commandsOutput.text = ""
            squareCommand2.visible = false
            return
        }


        if (activeCommand === Commands.Fillet){
            interActionCount = 0
            rootMainView.genericCommand(Commands.UnPick) //unpick all picked entities
            commandsOutput.text = "Fillet: select first line"
        }

        if (activeCommand === Commands.Extend){
            interActionCount = 0
            rootMainView.genericCommand(Commands.UnPick) //unpick all picked entities
            commandsOutput.text = "Extend: select first line"
        }

        if (activeCommand === Commands.Trim){
            interActionCount = 0
            rootMainView.genericCommand(Commands.UnPick) //unpick all picked entities
            commandsOutput.text = "Trim: select first line"
        }

        if (activeCommand === Commands.Length){
            interActionCount = 0
            console.log("commands2  -----------------------.........")
            rootMainView.genericCommand(Commands.Length) //unpick all picked entities
            commandsOutput.text = "Length: select line"
        }


        //commands2.commandActivated(activeCommand)

    }

    property int interActionCount: 0
    property var entityOne
    property vector3d entityOnePickedPos
    property var entityTwo
    property vector3d entityTwoPickedPos

    signal startCommand(command: int)
    signal requestSeletedEntitesInfo()
    signal receiveEntitiesInfo(entityInfo: var)
    signal executeComm2(pos: vector3d)
   // signal sendDataToCmdTextArea(str: string)
    //signal sendEntitiesInfo(interactionCount: int, otherEntityPos: vector3d)

    onReceiveEntitiesInfo: function(entityInfo){
        console.log("onReceiveEntitiesInfo: "+entityInfo)

        if (activeCommand === Commands.Trim) {
            if (interActionCount === 0) {
                commandsOutput.text = "Trim: select reference line"
                entityOne = entityInfo
                //entityOnePickedPos = entityInfo.pickedPos
                interActionCount++
                return
            }


            if (interActionCount === 1) {
                console.log("commands 2 Commands.Trim-----------------------------------------------------")
                commandsOutput.text = "Trim: select trim line"
                entityTwo = entityInfo
                if (mathLib.trimCalculate(entityOne.p0, entityOne.p1, entityTwo.p0, entityTwo.p1,
                                          entityTwo.pickPoint) )
                {
                    entityTwo.p0 = mathLib.trimLine_r0();
                    entityTwo.p1 = mathLib.trimLine_r1();
                }
                interActionCount = 0
                return
            }

        }

        if (activeCommand === Commands.Extend) {

            if (interActionCount === 0) {
                commandsOutput.text = "Extent: select extend reference line"
                entityOne = entityInfo
                //entityOnePickedPos = entityInfo.pickedPos
                interActionCount++
                mathLib.setExtendReferenceLine(entityOne.p0, entityOne.p1)
                return
            }

            if (interActionCount === 1) {
                console.log("commands 2 Commands.Extend-----------------------------------------------------")
                commandsOutput.text = "Extent: select extended lines"
                entityTwo = entityInfo
                if (mathLib.setExtendLine(entityTwo.p0, entityTwo.p1) )
                {
                    entityTwo.p0 = mathLib.extendLine_p0();
                    entityTwo.p1 = mathLib.extendLine_p1();
                }
                return
            }

        }//if (activeCommand === Commands.Extende)

        if (activeCommand === Commands.Fillet){

            if (interActionCount === 0) {
                commandsOutput.text = "Fillet: select second line"
                entityOne = entityInfo
                //entityOnePickedPos = entityInfo.pickedPos
                interActionCount++
                return
            }

            if (interActionCount === 1) {
                commandsOutput.text = "Fillet: select first line"

                entityTwo = entityInfo
                //entityTwoPickedPos = entityInfo.pickedPos


                if (mathLib.filletCalculate(entityOne.p0, entityOne.p1, entityOne.pickPoint,
                                            entityTwo.p0, entityTwo.p1, entityTwo.pickPoint)) {


                    console.log("entityOne.p0: "+entityOne.p0)
                    entityOne.p0 = mathLib.fillet_l0();
                    console.log("entityOne.p0: "+entityOne.p0)

                    console.log("entityOne.p1: "+entityOne.p1)
                    entityOne.p1 = mathLib.fillet_l1();
                    console.log("entityOne.p1: "+entityOne.p1)

                    console.log("entityTwo.p0: "+entityTwo.p0)
                    entityTwo.p0 = mathLib.fillet_r0();
                    console.log("entityTwo.p0: "+entityTwo.p0)

                    console.log("entityTwo.p1: "+entityTwo.p1)
                    entityTwo.p1 = mathLib.fillet_r1();
                    console.log("entityTwo.p1: "+entityTwo.p1)

                    entityOne.picked = false
                    entityTwo.picked = false


                    activeCommand = Commands.None
                }
                interActionCount = 0
                return
            }//if (interActionCount === 1)
        }//if (activeCommand === Commands.Fillet)

        /*if (activeCommand === Commands.Length){

            console.log("commands2 Commands.Length")


            entityOne = entityInfo

            var len = mathLib.distance3D(entityOne.p0, entityOne.p1)
            console.log("line length: ", len)
            console.log("sendDataToCmdTextArea(len): ", len)
            sendDataToCmdTextArea(len)
            return
        }*/


    }//onReceiveEntitiesInfo

    Square {
        id: squareCommand2
        colorSquare: "lightgreen"
        visible: false
    }

   //signal commandActivated(activeComm: int)//call this when the user press the command button, for example, the Fillet MenuButtonsTemplate

    onStartCommand: function(command) {

        if (command === Commands.Fillet || command === Commands.Fillet) {
            requestSeletedEntitesInfo()
        }
    }

    function receiveEvent(evt: int, p: vector3d, command){

        if (evt === cpr ) {
            console.log("command2 cpr start / update ok: "+command)

            if (cprCounter === 0) {
                console.log("command cpr start ok: "+command)

                if (osnapON)
                    p = osnapPoint


                cprCounter++
                //startCommand(p, command)//to delete


                return 1;
            }
        }



    }//function receiveEvent(evt: int, p: vector3d, command)
}
