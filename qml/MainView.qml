import QtQuick //2.12
import QtQuick.Window 2.12
import QtQuick3D //1.15
import QtQuick.Controls 2.14
import QtQuick3D.Helpers //1.15
import QtQuick.Dialogs
import QtCore
import QtQuick.Pdf
import CursorEnum
import "qmlEntities"
import "entitiesEditors"
import "components"
import "pagesUI"
import "settingsMenu"
import "commands"
import "qmlEnums"

Item {
    id: rootMainView

    property int activeCommand: Commands.None

    property var lineEditor: lineEditorMobile
    property var rectangleEditor: rectangleEditorMobile
    property var circleEditor: circleEditorMobile
    property var offsetEditor: offsetEditorMobile
    property var entitiesPropertiesEditor: entitiesPropertiesEditorMobile

    property var mySettings: mySettingsMobile
    property var blocksList: blocksListMobile

    property real fingerCursorDist: 0//10 * mm
    property color color4NewEntities: colorPic.isByLayerColor ? layerEditor.layersModel.get(layerEditor.currentLayer).layerColor
                                                              : colorPic.selectedColor
    property bool orthoMode: menu.ortho


    signal genericCommand(command: int)

    property int pickedEntitiesCount: 0
    onPickedEntitiesCountChanged: {
        pickedEntitiesCount === 0 ? menuCommands.modelMenu.clear() : showMenuCommands()
    }

    signal pickedEntitiesChanged(bool bOnOff)

    onPickedEntitiesChanged: function(bOnOff){
        bOnOff ? pickedEntitiesCount++ : pickedEntitiesCount--
        console.log("pickedEntitiesCount: "+pickedEntitiesCount)
    }

    Component.onCompleted: {
        //lineEditor = lineEditorMobile
        if (Qt.platform.os === "windows" || Qt.platform.os === "linux" || Qt.platform.os === "osx") {
            lineEditor = lineEditorDesktop
            rectangleEditor = rectangleEditorDesktop
            offsetEditor = offsetEditorDesktop
            circleEditor = circleEditorDesktop
            entitiesPropertiesEditor = entitiesPropertiesEditorDesktop
            mySettings = mySettingsDesktop
            blocksList = blocksListDesktop
        }

        showMenuPreferences()

        //color4NewEntities = Qt.binding(function() { return colorPic.isByLayerColor ? layerEditor.layersModel.get(layerEditor.currentLayer).layerColor
       //                                                                            : colorPic.selectedColor })

        //height = Qt.binding(function() { return width * 3 })

        for (var i = 0; i < 0; ++i) {
            layer0.addRandomLine()
            layer0.createRandomCircle()
        }

        console.log("CursorEnum.normal: ",CursorEnum.normal)
        console.log("CursorEnum.rectangle: ",CursorEnum.rectangle)
    }

    /*WasdController {
        controlledObject: mainCamera
    }*/




    View3D {
        id: view
        visible: true
        //anchors.fill: parent
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        //height: parent.height * 0.85
        height: textEditor.visible ? parent.height * 0.85 : parent.height
        //renderMode: View3D.Underlay

        environment: SceneEnvironment {
            backgroundMode: SceneEnvironment.Color
            clearColor: "black"
            antialiasingMode: Qt.platform.os === "windows" || Qt.platform.os === "linux" || Qt.platform.os === "osx" ?
                                  SceneEnvironment.MSAA : SceneEnvironment.NoAA
            //SceneEnvironment.MSAA
            //SceneEnvironment.SSAA
            //SceneEnvironment.ProgressiveAA
            //antialiasingQuality: SceneEnvironment.High
            antialiasingQuality: SceneEnvironment.VeryHigh
        } //environment: SceneEnvironment

        //importScene: layer0

        camera: OrthographicCamera{
            id: mainCamera
            z: 200
            horizontalMagnification: zoomCamera
            verticalMagnification: zoomCamera

            property real zoomCamera: 1.0
            onZoomCameraChanged: console.log("zoomCamera:"+zoomCamera)


            property vector3d posBeforeZoom
            property vector3d posAfterZoom
            property vector3d posDifZoom

            property vector3d posBeforeZoomScreen
            property vector3d posAfterZoomScreen
            property vector3d posDifZoomScreen


            function zoom(delta){
                posBeforeZoom = view.mapTo3DScene(Qt.vector3d(ma.mouseX, ma.mouseY-fingerCursorDist, 0))
                posBeforeZoomScreen = view.mapFrom3DScene(Qt.vector3d(ma.mouseX, ma.mouseY-fingerCursorDist, 0))

                if (delta >0 )
                    zoomCamera *=  1.5
                else
                    zoomCamera /=  1.5

                posAfterZoom = view.mapTo3DScene(Qt.vector3d(ma.mouseX, ma.mouseY-fingerCursorDist, 0))
                posAfterZoomScreen = view.mapFrom3DScene(Qt.vector3d(ma.mouseX, ma.mouseY-fingerCursorDist, 0))

                posDifZoom = Qt.vector3d(posAfterZoom.x - posBeforeZoom.x, posAfterZoom.y - posBeforeZoom.y
                                      , posAfterZoom.z - posBeforeZoom.z)

                posDifZoomScreen = Qt.vector3d(posAfterZoomScreen.x - posBeforeZoomScreen.x, posAfterZoomScreen.y - posBeforeZoomScreen.y
                                               , posAfterZoomScreen.z - posBeforeZoomScreen.z)

                layer0.position.x += posDifZoom.x
                layer0.position.y += posDifZoom.y

                //dummyCamPosition.x += posDifZoomScreen.x
                //dummyCamPosition.y += posDifZoomScreen.y

                console.log("posDifZoomScreen: "+posDifZoomScreen)
                //console.log("dummyCamPosition.position: "+dummyCamPosition.x)

                //cursorPosWorld3D.x = posAfterZoom.x - posDifZoom.x
                //cursorPosWorld3D.y = posAfterZoom.y - posDifZoom.y

            }//function zoom(delta)
        }//OrthographicCamera

        /*CursorCross {
            id: cursorCross
            center: cursorPosWorld3D
            //center.x: cursorPosWorld3D.x
            //center.y: cursorPosWorld3D.y
           // lineDist: 10*mm
        }*/

        Cursor {
            id: cursorCross
            //center: cursorPosWorld3D
            position: cursorPosWorld3D
            squareDist: 5 / mainCamera.zoomCamera
            lineDist: maxWidthHeight * 0.05 / mainCamera.zoomCamera
            //cursorType: (activeCommand === Commands.None) ? CursorEnum.normal : CursorEnum.rectangle // enum buggy, not working ??????
            cursorType: (activeCommand === Commands.None) ? 0 : 1

            //center: mouseWorldPosition
            //center: Qt.vector3d(cursorPosWorld3D.x, cursorPosWorld3D.y, 0)
            //colorRect: "orange"
        }

        InstanceList {
            id: listEntitiesInstance
            instances: [ lineEntry1, lineEntry2 ]
        }

        InstanceListEntry {
            id: lineEntry1
            property vector3d p0: Qt.vector3d(0,0,0)
            property vector3d p1: Qt.vector3d(50,0,0)
            property string color: "red"
            position: Qt.vector3d(-50,-50,0)
        }

        InstanceListEntry {
            id: lineEntry2
            property vector3d p0: Qt.vector3d(0,50,0)
            property vector3d p1: Qt.vector3d(50,0,0)
            property string color: "blue"
            position: Qt.vector3d(10,10,0)
        }

        Node {
            id: layer0StarPanPosition // use Nodes to simulate Layers ???
        }

        Node {
            id: layer0
            x: 150
            y: 150
            //scale: Qt.vector3d(0.5,0.5,0.5)
            //scale: Qt.vector3d(2,2,2)
            //onCountChanged: console.log("layer0.count: "+ count)

            /*Model {
                source: "#Sphere"
                materials: DefaultMaterial {
                    diffuseColor: "red"
                }
            }*/

            RandomInstancing {
                id: randomTable
                instanceCount: 0
                position: InstanceRange { from: Qt.vector3d(-400, -400, -200); to: Qt.vector3d(200, 200, 200) }
                //p0: InstanceRange { from: Qt.vector3d(-200, -200, -200); to: Qt.vector3d(200, 200, 200) }
                //p1: InstanceRange { from: Qt.vector3d(-200, -200, -200); to: Qt.vector3d(200, 200, 200) }
                //color: InstanceRange { from: Qt.rgba(0.1, 0.1, 0.1, 1); to: Qt.rgba(1, 1, 1, 1)}
                //color: "red"
            }

            // testing instancing performance

            LineThick {
                instancing: randomTable
            }

            LineThick {
                instancing: listEntitiesInstance
            }

            MyImage {
                id: image

            }

            PdfDocument {
                id: doc
                //source: Qt.resolvedUrl(rootMainView.source)
                onPasswordRequired: passwordDialog.open()
            }
            Rectangle {
                id: paper
                width: imagePdf.width
                height: imagePdf.height
                PdfPageImage {
                    id: imagePdf
                    document: doc
                    currentFrame: 0
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit

                    width: sourceSize.width
                    height: sourceSize.height
                }
            }

            Node {
                Item {
                    width: imaged.sourceSize.width
                    height: imaged.sourceSize.height
                    anchors.centerIn: parent
                    Image {
                        id: imaged
                        mipmap: true
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                    }
                }
            }

            onXChanged: console.log("layer0 onXChanged: "+x)
            onYChanged: console.log("layer0 onYChanged: "+y)

            property real range: 200
            //property var listEntities: []
            property int count

            Block {
                id: blockTest
                LineThick {
                    p0: Qt.vector3d(0, 0, 0)
                    p1: Qt.vector3d(50, -150, 0)

                }
                visible: false
            }



            //dynamic objects
            function createRandomCircle() {
                var shapeComponent = Qt.createComponent("qrc:/qml/qmlEntities/Circle.qml");
                //let bullet = shapeComponent.createObject(space, {rotation: starship.rotation,position: starship.scenePosition,d: camera.front});
                let newCircle = shapeComponent.createObject(layer0)
                newCircle.center = Qt.vector3d((2 * Math.random() * range) - range,
                                               (2 * Math.random() * range) - range,0)
                newCircle.radius = (2 * Math.random() * range) - range
                newCircle.baseColor = Qt.hsva(Math.random(), 1.0, 1.0, 1.0)
                //newCircle.zoomScale = 30 * mainCamera.zoomCamera

                //listEntities.push(newCircle);
                //count = listEntities.length
                countEntities++
            }


            //! [adding]
            function addRandomLine()
            {
                var xPos = (2 * Math.random() * range) - range;

                var newEntity = Qt.createComponent("qrc:/qml/qmlEntities/LineThick.qml");
                let instance = newEntity.createObject(layer0);
                instance.color1 = Qt.hsva(Math.random(), 1.0, 1.0, 1.0)
                instance.color2 = Qt.hsva(Math.random(), 1.0, 1.0, 1.0)
                instance.p0 = Qt.vector3d( (2 * Math.random() * range) - range,
                                          (2 * Math.random() * range) - range, 0)
                instance.p1 = Qt.vector3d( (2 * Math.random() * range) - range,
                                          (2 * Math.random() * range) - range, 0)
                instance.thick = Qt.binding(function() { return 10 / mainCamera.zoomCamera })
                //instance.thick = 10 / mainCamera.zoomCamera
                instance.zoomScale = Qt.binding(function() { return 2 * mainCamera.zoomCamera })

                //listEntities.push(instance);
                //count = listEntities.length
                countEntities++
            }

            function addText(myText) {
                var entityText = Qt.createComponent("qrc:/qml/qmlEntities/TextEntity.qml")
                let newText = entityText.createObject(layer0)
                newText.text = myText
                newText.fontpixelSize = textInputAux.font.pixelSize
                //console.log("textTemplate (x,y) =  ("+textTemplate.x + ", "+textTemplate.y + ")")
                //var aux = ma.getCursorPosWorld3DXY(textTemplate.x, textTemplate.y, 0)
                //newText.x = aux.x
                //newText.y = aux.y
                //console.log("textTemplate World(x,y) =  ("+textTemplate.x + ", "+textTemplate.y + ")")

                textInputAux.text = ""
                textInputAux.focus = false
                textInputAux.visible = false
                newText.position = text3DPosition//textTemplate.position //Qt.vector3d(textTemplate.x, textTemplate.y, textTemplate.z)
                newText.mousePosition = Qt.binding(function(){return mouseWorldPosition})

                //listEntities.push(newText);
                //count = listEntities.length
                countEntities++
            }

            function addRectangle(p0: vector3d, p1: vector3d, p2: vector3d, p3: vector3d) {
                var entityRectangle = Qt.createComponent("qrc:/qml/qmlEntities/RectangleThick.qml")
                let newRectangle = entityRectangle.createObject(layer0)
                newRectangle.isByLayerColor = colorPic.isByLayerColor
                newRectangle.layerNumber = layerEditor.currentLayer
                newRectangle.layerColor = Qt.binding(function() { return layerEditor.layersModel.get(newRectangle.layerNumber).layerColor })
                newRectangle.noLayerColor = colorPic.selectedColor //Qt.binding(function() { return colorPic.selectedColor })
                newRectangle.p0 = p0
                newRectangle.p1 = p1
                newRectangle.p2 = p2
                newRectangle.p3 = p3
                newRectangle.thick = Qt.binding(function() { return lineThickEditor.currentLineThick / mainCamera.zoomCamera })
                newRectangle.mousePosition = Qt.binding(function(){return mouseWorldPosition})
                newRectangle.pickable = Qt.binding(function(){return isModePickingActive})
                newRectangle.pickCounter.connect( rootMainView.pickedEntitiesChanged )
                genericCommand.connect(newRectangle.genericCommand)

                //commands.startCommand.connect(newRectangle.startCommand)
                //commands.updateCommand.connect(newRectangle.updateCommand)
                //commands.endCommand.connect(newRectangle.endCommand)

                commands.commandCprStart.connect(newRectangle.commandCprStart)
                commands.commandCprUpdate.connect(newRectangle.commandCprUpdate)
                commands.commandCprFinish.connect(newRectangle.commandCprFinish)
                commands.commandMtm.connect(newRectangle.commandMtm)

                newRectangle.connecAtStartEnabled = true

                //listEntities.push(newRectangle);
                //count = listEntities.length
                countEntities++
            }

            function addCircle(center: vector3d, radius: real) {
                var entityCircle = Qt.createComponent("qrc:/qml/qmlEntities/CircleThick.qml")
                let newCircle = entityCircle.createObject(layer0)
                newCircle.center = center
                newCircle.radius = radius

                newCircle.isByLayerColor = colorPic.isByLayerColor
                newCircle.layerNumber = layerEditor.currentLayer
                newCircle.layerColor = Qt.binding(function() { return layerEditor.layersModel.get(newCircle.layerNumber).layerColor })
                newCircle.noLayerColor = colorPic.selectedColor //Qt.binding(function() { return colorPic.selectedColor })
                newCircle.thick = Qt.binding(function() { return lineThickEditor.currentLineThick / mainCamera.zoomCamera })
                newCircle.mousePosition = Qt.binding(function(){return mouseWorldPosition})
                newCircle.pickable = Qt.binding(function(){return isModePickingActive})
                newCircle.pickCounter.connect( rootMainView.pickedEntitiesChanged )
                genericCommand.connect(newCircle.genericCommand)

                //commands.startCommand.connect(newCircle.startCommand)
                //commands.updateCommand.connect(newCircle.updateCommand)
                //commands.endCommand.connect(newCircle.endCommand)

                commands.commandCprStart.connect(newCircle.commandCprStart)
                commands.commandCprUpdate.connect(newCircle.commandCprUpdate)
                commands.commandCprFinish.connect(newCircle.commandCprFinish)
                commands.commandMtm.connect(newCircle.commandMtm)

                newCircle.connecAtStartEnabled = true

                //listEntities.push(newCircle);
                //count = listEntities.length
                countEntities++
            }

            function addLine(p0: vector3d, p1: vector3d){
                var entityLine = Qt.createComponent("qrc:/qml/qmlEntities/LineThick.qml")
                let newLine = entityLine.createObject(layer0)

                newLine.isByLayerColor = colorPic.isByLayerColor
                newLine.layerNumber = layerEditor.currentLayer
                newLine.layerColor = Qt.binding(function() { return layerEditor.layersModel.get(newLine.layerNumber).layerColor })
                newLine.noLayerColor = colorPic.selectedColor //Qt.binding(function() { return colorPic.selectedColor })

                //newLine.color1 = Qt.binding(function() { return color4NewEntities })
                //newLine.color2 = Qt.binding(function() { return color4NewEntities })
                        //layerEditor.layersModel.get(layerEditor.currentLayer).layerColor
                //newLine.color2 = lineTemplate.color2
                newLine.p0 = p0
                newLine.p1 = p1

                //console.log("Add line -------------------------------------------------------------------------------------------------------")
                //console.log("line creation p0:"+newLine.p0)
                //console.log("line creation p1:"+newLine.p1)


                newLine.pickInterval = Qt.binding(function() { return ((lineThickEditor.currentLineThick) / mainCamera.zoomCamera)+4 })//thick + 4
                newLine.thick = Qt.binding(function() { return lineThickEditor.currentLineThick / mainCamera.zoomCamera })
                //newLine.thick = Qt.binding(function() { return lineThickEditor.currentLineThick })

                newLine.zoomScale = Qt.binding(function() { return 1 * mainCamera.zoomCamera })
                //newLine.visible = true

                newLine.mousePosition = Qt.binding(function(){return mouseWorldPosition})
                newLine.pickable = Qt.binding(function(){return isModePickingActive})
                //genericCommand.connect(newLine.genericCommand)

                //commands.startCommand.connect(newLine.startCommand)
                //commands.updateCommand.connect(newLine.updateCommand)
                //commands.endCommand.connect(newLine.endCommand)

                commands.commandCprStart.connect(newLine.commandCprStart)
                commands.commandCprUpdate.connect(newLine.commandCprUpdate)
                commands.commandCprFinish.connect(newLine.commandCprFinish)
                commands.commandMtm.connect(newLine.commandMtm)
                newLine.connecAtStartEnabled = true
                newLine.sendInfo.connect(commands2.receiveEntitiesInfo)
                newLine.sendText.connect(textEditor.addText)
                //commands2.executeComm2.connect(newLine.executeComm2)


                //listEntities.push(newLine);
                //count = listEntities.length
                countEntities++
                //console.log("listEntities.length: "+listEntities.length)
                newLine.pickCounter.connect( rootMainView.pickedEntitiesChanged )

                //the following code was replaced with connections in EntityTemplate.qml
                //ma.clickPos.connect(newLine.click)
                //a bit buggy but it more or less work,
                // this avoids that the line will be picked when created
                //newLine.picked = osnapON ? true : false
                //newLine.picked = orthoMode ? false : true
                //newLine.picked = orthoMode ? (osnapON ? true : false) : true
            }

            function addPolyline(pointsList: list<vector3d>) {

                console.log("pointsList", pointsList)

                var entityPolyline = Qt.createComponent("qrc:/qml/qmlEntities/PolyLineThick.qml")
                let newPolyline = entityPolyline.createObject(layer0)

                newPolyline.isByLayerColor = colorPic.isByLayerColor
                newPolyline.layerNumber = layerEditor.currentLayer
                newPolyline.layerColor = Qt.binding(function() { return layerEditor.layersModel.get(newPolyline.layerNumber).layerColor })
                newPolyline.noLayerColor = colorPic.selectedColor //Qt.binding(function() { return colorPic.selectedColor })

                newPolyline.pointsList = pointsList

                newPolyline.pickInterval = Qt.binding(function() { return ((lineThickEditor.currentLineThick) / mainCamera.zoomCamera)+4 })//thick + 4
                newPolyline.thick = Qt.binding(function() { return lineThickEditor.currentLineThick / mainCamera.zoomCamera })
                //newLine.thick = Qt.binding(function() { return lineThickEditor.currentLineThick })

                newPolyline.zoomScale = Qt.binding(function() { return 1 * mainCamera.zoomCamera })
                //newLine.visible = true

                newPolyline.mousePosition = Qt.binding(function(){return mouseWorldPosition})
                newPolyline.pickable = Qt.binding(function(){return isModePickingActive})
                genericCommand.connect(newPolyline.genericCommand)


                commands.commandCprStart.connect(newPolyline.commandCprStart)
                commands.commandCprUpdate.connect(newPolyline.commandCprUpdate)
                commands.commandCprFinish.connect(newPolyline.commandCprFinish)
                commands.commandMtm.connect(newPolyline.commandMtm)
                newPolyline.connecAtStartEnabled = true
                newPolyline.sendInfo.connect(commands2.receiveEntitiesInfo)
                //commands2.executeComm2.connect(newLine.executeComm2)


                //listEntities.push(newLine);
                //count = listEntities.length
                countEntities++
                //console.log("listEntities.length: "+listEntities.length)
                newPolyline.pickCounter.connect( rootMainView.pickedEntitiesChanged )

            }

            //! [adding]
            CircleThick {
                id: circleTemplate
                color1: color4NewEntities
                color2: color4NewEntities //highlighted ? "yellow" : "red"
                visible: false
                thick: lineThickEditor.currentLineThick

                //mousePosition: mouseWorldPosition // triggers picking


                //picked: true
            }

            LineThick {
                id: lineTemplate
                objectName: "lineTemplate"
                p0: Qt.vector3d(0, 0, 0)
                p1: Qt.vector3d(50, 150, 0)
                thick: lineThickEditor.currentLineThick / mainCamera.zoomCamera
                color1: color4NewEntities
                color2: color4NewEntities
                zoomScale: 10 * mainCamera.zoomCamera
                visible: false
                //mousePosition: mouseWorldPosition // triggers picking
            }

            RectangleThick {
                id: rectangleTemplate
                p0: Qt.vector3d(0, 0, 0)
                p1: Qt.vector3d(50, 0, 0)
                p2: Qt.vector3d(50, 50, 0)
                p3: Qt.vector3d(0, 50, 0)
                thick: lineThickEditor.currentLineThick / mainCamera.zoomCamera
                layerColor: color4NewEntities
                noLayerColor: colorPic.selectedColor
                //newLine.layerColor = Qt.binding(function() { return layerEditor.layersModel.get(newLine.layerNumber).layerColor })
                //newLine.noLayerColor = colorPic.selectedColor //Qt.binding(function() { return colorPic.selectedColor })
                visible: false
            }

            PolyLineThick {
                id: polyLineTemplate
            }

            CommandsEntites {
                id: commands
            }

            Commands2 {
                id: commands2

                /*onSendDataToCmdTextArea: function(str){
                    console.log("onSendDataToCmdTextArea ------------------", str)
                    textEditor.addText(str)
                }*/
            }

            /*TextEntity {
                id: textTemplate
            }*/


            //! [removing]
            function removeShape()
            {
                /*if (listEntities.length > 0) {
                    let instance = listEntities.pop();
                    instance.destroy();
                    count = listEntities.length
                }*/
            }
            //! [removing]


            //! [light]
            DirectionalLight {
                //eulerRotation.x: -45
               // eulerRotation.y: 70
            }
            //! [light]


            /*CircleThick {
                id: cThick
                center: Qt.vector3d(-50, 0, 0)
                //pointInRadius: Qt.vector3d(0, 0, 0)
                radius: 100
                color1: "red"
                color2: "orange"
                scaleU: 12
                thick: 1 / mainCamera.zoomCamera
                onThickChanged: console.log("circle thick: "+cThick.thick)
                count: 100
                //visible: true
                //scale: Qt.vector3d(3,3,3)
            }*/

            Line {
                p0: Qt.vector3d(-55, 55, 0)
                p1: Qt.vector3d(55, -55, 0)
                baseColor: "orange"
                visible: false
                objectName: "Line"
                //pickable: true
            }

            LineThick {
                id: lineBlue
                p0: Qt.vector3d(0,-300,0)
                p1: Qt.vector3d(150, 150, 0)
                color1: picked ? "purple" : "orange"
                //color2: isPicked ? "orange" : "purple"
                //color1: "orange"
                color2: "green"
                thick: 6 / mainCamera.zoomCamera
                //picked: isPicked

                property bool isPicked

                objectName: "LineThick...................."
                //visible: false
                pickable: true
                zoomScale: 6 * mainCamera.zoomCamera
                visible: false
            }

            LineThick {
                p0: Qt.vector3d(-50, -150, 0)
                p1: Qt.vector3d(50, 150, 0)
                thick: 2 / mainCamera.zoomCamera
                color1: "red"
                color2: "blue"
                zoomScale: 10 * mainCamera.zoomCamera
                visible: false
            }

        }//Node: layer0

        /*AxisHelper {
            id: axis
            enableAxisLines: true
            enableXYGrid: false
            enableXZGrid: false
            enableYZGrid: false
        }*/

    }//View3D


    Keys.onPressed: {
        textEditor.cmdLine.focus = true
    }

    CmdTextList {
        id: textEditor
        width: parent.width
        height: parent.height * 0.15
        anchors.bottom: parent.bottom
    }

    Column {
        //visible: false
        anchors.bottom: view.bottom
       // anchors.horizontalCenter: parent.horizontalCenter
        Row {
            //anchors.bottom: parent.bottom
            //anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20

            Text {
                id: screenPosition
                color: "white"
                font.pointSize: 12
                text: "Screen Position: " + ma.mouseX + "; "+ma.mouseY
            }


            Text {
                id: scenePosition
                color: "white"
                font.pointSize: 12
                text: "World Position: " + mouseWorldPosition
            }

            Text {
                id: countLabel
                color: "white"
                font.pointSize: 12
                text: "Models in Scene: " + countEntities //layer0.count
            }
        }//Row
        Text {
            id: commandsOutput
            color: "white"
            font.pointSize: 12
            text: ""
        }
    }



    property vector3d screenPos3D
    property vector3d cursorPosWorld3D
    property vector3d mouseWorldPosition

    property vector2d panStartPosition
    property vector2d panMovePosition
    property vector2d layer0StarPanPosition
    property bool isPressed: false

    property bool isModePickingActive: isPressed// todo: change this to work also on mobile

    property vector3d osnapPoint
    //onOsnapPointChanged: console.log("osnapPoint: "+osnapPoint)

    property real osnapDistance: 3*mm / mainCamera.zoomCamera
    property bool checkingOsnap: true

    property bool osnapON: false
    //onOsnapONChanged: console.log("onOsnapONChanged: "+osnapON)

    //! [mouse area]
    MouseArea {
        id: ma
        anchors.fill: view
        cursorShape: containsMouse ? Qt.BlankCursor : Qt.ArrowCursor

        signal clickPos(pos: vector3d)
        //onClickPos: (pos)=>{
        onClickPos: function(pos){
            console.log("onClickPos: "+ pos)
        }

        function getCursorPosWorld3DNode(){

            //update cursor
            /*var aux = Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0)
            var aux2 = view.mapTo3DScene(aux)
            cursorPosWorld3D = Qt.vector3d(aux2.x, aux2.y, 0)
            //end cursor update

            //var p = view.mapTo3DScene( Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0))
            var p = Qt.vector3d(p.x - layer0.x, p.y - layer0.y, 0)
            //console.log("getCursorPosWorld3DNode() p: "+p)
            return p*/



            //update cursor
           // var aux = Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0)
            var aux = view.mapTo3DScene(Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0))
            cursorPosWorld3D = Qt.vector3d(aux.x, aux.y, 0)
            //end cursor update

            //var p = view.mapTo3DScene( Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0))
            return  Qt.vector3d(aux.x - layer0.x, aux.y - layer0.y, 0)
            //console.log("getCursorPosWorld3DNode() p: "+p)
            //return p



            /*//var aux = Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0)
            cursorPosWorld3D = view.mapTo3DScene(Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0))
            //end cursor update

            //var p = view.mapTo3DScene( Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0))
            var p = Qt.vector3d(cursorPosWorld3D.x - layer0.x, cursorPosWorld3D.y - layer0.y, 0)
            //console.log("getCursorPosWorld3DNode() p: "+p)
            return p*/


        }

        hoverEnabled: true

        onWheel: function(wheel){
            //console.log("angleDelta: " + wheel.angleDelta)
            //console.log("pixelDelta: " + wheel.pixelDelta)
            mainCamera.zoom(wheel.angleDelta.y)

            updateCursor()
        }

        //used for pan with the mouse center button
        acceptedButtons: Qt.AllButtons

        //onPressed: (mouse)=> {
        onPressed: function(mouse) {
            isPressed = true
            bPickOnMouseMove = true

            /*if (mouse.button === Qt.RightButton) {
                activeCommand = Commands.None;
                return;
            }*/

            //console.log("MouseArea onPressed-----activeCommand: "+activeCommand)
            //console.log("activeCommand: "+activeCommand)


            //if ((activeCommand === Commands.Pan  && isPressed ) || pressedButtons === Qt.MiddleButton)
            if (activeCommand === Commands.Pan || pressedButtons === Qt.MiddleButton) {
                console.log("onPressed -----------------------------------")
                console.log("onPressed layer0.x: " + layer0.x)

                panStartPosition = Qt.vector2d(mouseX, mouseY)
                panMovePosition = Qt.vector2d(mouseX, mouseY)
                layer0StarPanPosition.x = layer0.x
                layer0StarPanPosition.y = layer0.y
                console.log("onPressed layer0StarPanPosition.x: " + layer0StarPanPosition.x)
                return
            }

            if (activeCommand === Commands.Line)
                lineTemplate.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), layer0)
            if (activeCommand === Commands.Circle)
                circleTemplate.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Rectangle)
                rectangleTemplate.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Polyline)
                polyLineTemplate.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Rotate || activeCommand === Commands.Scale
                || activeCommand === Commands.Selection || activeCommand === Commands.UnPick
                || activeCommand === Commands.Offset)
                    commands.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), activeCommand)

            //if (activeCommand === Commands.Fillet)
              //  commands2.receiveEvent(EventsQML.Press, getCursorPosWorld3DNode(), activeCommand);

        }

        //onReleased: (mouse)=> {
        onReleased: function(mouse) {
            isPressed = false

            if (mouse.button === Qt.RightButton) {
                console.log("activeCommand", activeCommand)
                console.log("Commands.Polyline", Commands.Polyline)

                if (activeCommand === Commands.Polyline) {
                    if (polyLineTemplate.receiveEvent(EventsQML.ReleaseRightClick, getCursorPosWorld3DNode(), layer0) === EntitiesQML.Finish)
                        menuPolylineEnd.clearMenu()
                }

                activeCommand = Commands.None;
                return;
            }

            //console.log("MouseArea onReleased--- activeCommand_ "+activeCommand)

            if (activeCommand === Commands.Line)
                lineTemplate.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Circle)
                circleTemplate.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Rectangle)
                rectangleTemplate.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Polyline)
                polyLineTemplate.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Text)
                setText(mouseX, mouseY-fingerCursorDist, getCursorPosWorld3DNode())
                //textTemplate.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Rotate || activeCommand === Commands.Scale
                || activeCommand === Commands.Selection || activeCommand === Commands.UnPick
                || activeCommand === Commands.Offset)
                    commands.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), activeCommand);

            //if (activeCommand === Commands.Fillet)
              //  commands2.receiveEvent(EventsQML.Release, getCursorPosWorld3DNode(), activeCommand);



        }

        //onClicked: (mouse)=> {
        onClicked: function(mouse) {

            /*if (mouse.button === Qt.RightButton) {
                activeCommand = Commands.None;
                return;
            }*/

            clickPos(getCursorPosWorld3DNode())


            //console.log("MouseArea onClicked---------------------------------------------")



            if (activeCommand === Commands.Line)
                lineTemplate.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Circle)
                circleTemplate.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Rectangle)
                rectangleTemplate.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Polyline)
                polyLineTemplate.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), layer0)


            if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Rotate || activeCommand === Commands.Scale
                || activeCommand === Commands.Selection || activeCommand === Commands.UnPick
                || activeCommand === Commands.Offset)
                    commands.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), activeCommand)

            //if (activeCommand === Commands.Fillet)
              //  commands2.receiveEvent(EventsQML.Click, getCursorPosWorld3DNode(), activeCommand);
        }

//        onMouseXChanged: (mouse)=> {
        onMouseXChanged: function (mouse) {

            /*if (mouse.button === Qt.RightButton) {
                activeCommand = Commands.None;
                return
            }*/

            //screenPos3D = Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0)
            //console.log("MouseArea onMouseXChanged------activeCommand: "+activeCommand)

            mouseWorldPosition = getCursorPosWorld3DNode()
            //console.log("mouseX, mouseY: "+mouseX+" : "+mouseY)
            //mouseWorldPosition = Qt.vector3d(mouseX, mouseY, 0)
            //cursorPosWorld3D = mouseWorldPosition = Qt.vector3d(mouseX, mouseY, 0)
            //cursorCross.center = cursorPosWorld3D

            //console.log("mouseWorldPosition: "+mouseWorldPosition)


            if (activeCommand === Commands.None) {
                //console.log("Commands.None------------------------------------")
                //pickEntities(getCursorPosWorld3DNode())
            }

            //updateCursor()

            if ((activeCommand === Commands.Pan  && isPressed ) || pressedButtons === Qt.MiddleButton) {
                layer0.x = layer0StarPanPosition.x  +(- panStartPosition.x + mouseX) / mainCamera.zoomCamera
                layer0.y = layer0StarPanPosition.y + (panStartPosition.y - mouseY ) / mainCamera.zoomCamera
                return
            }

            if (activeCommand === Commands.Line)
                lineTemplate.receiveEvent(EventsQML.MouseMove, mouseWorldPosition, layer0)

            if (activeCommand === Commands.Circle)
                circleTemplate.receiveEvent(EventsQML.MouseMove, mouseWorldPosition, layer0)

            if (activeCommand === Commands.Rectangle)
                rectangleTemplate.receiveEvent(EventsQML.MouseMove, mouseWorldPosition, layer0)

            if (activeCommand === Commands.Polyline)
                polyLineTemplate.receiveEvent(EventsQML.MouseMove, getCursorPosWorld3DNode(), layer0)

            if (activeCommand === Commands.Copy || activeCommand === Commands.Move
                || activeCommand === Commands.Rotate || activeCommand === Commands.Scale
                || activeCommand === Commands.Selection)
                    commands.receiveEvent(EventsQML.MouseMove, mouseWorldPosition, activeCommand)

            //if (activeCommand === Commands.Fillet)
              //  commands2.receiveEvent(EventsQML.MouseMove, getCursorPosWorld3DNode(), activeCommand);

        }
        onMouseYChanged: (mouse)=> {
            //console.log("MouseArea onMouseYChanged---------------------------------------------")
            //mouseWorldPosition = getCursorPosWorld3DNode()

            //updateCursor()
        }

        //to delete and put inside getCursorPosWorld3DNode() ???
        function updateCursor(){
            var aux = Qt.vector3d(mouseX, mouseY-fingerCursorDist, 0)
            cursorPosWorld3D = view.mapTo3DScene(aux)
        }

    }//MouseArea


    //top menus-----------------------
    Flickable {
        id: flick
        anchors.top: parent.top
        width: parent.width // should be the size of items that fit in the screen
        contentWidth: parent.width//-defaultMargins*2
        x: defaultMargins
        anchors.topMargin: -btnHeight
        height: (btnHeight + defaultMargins) * 2
        contentHeight: menuCommands.visible ? (btnHeight + defaultMargins) * 4 : (btnHeight + defaultMargins) * 3

        contentY: btnHeight + defaultMargins

        onMovingChanged: {
            if (!moving) {//moving stopped
                height = menuCommands.visible ? (btnHeight + defaultMargins) * 3 : (btnHeight + defaultMargins) * 2 + btnHeight - contentY
                contentHeight = menuCommands.visible ? (btnHeight + defaultMargins) * 4 : (btnHeight + defaultMargins) * 3  + btnHeight - contentY
            }
        }

        Column {
            width: parent.width
            height: (btnHeight + defaultMargins) * 3
            spacing: defaultMargins

            Item {
                id: dummyItem
                width: btnSize
                height: btnHeight
            }

            MenuButtonsTemplate {
                id: menuPreferences

                onBtnClick: function(btnSignal){
                    if (btnSignal === Commands.OpenDlgImages)
                        fileDialogImage.open()

                    if (btnSignal === Commands.OpenDlgPdfs)
                        fileDialogPdf.open()

                    if (btnSignal === Commands.Save2Pdf) {
                        console.log("Save 2 Pdf..............................")
                        fileDlgSave2Pdf.open()
                        drawer.screenShot()
                    }

                }
            }

            MenuHybrid {
                id: menu
                width: parent.width

                btnColorPickerColor: color4NewEntities //colorPic.selectedColor

                btnLayerEditorColor: layerEditor.currentLayerColor

                btnLayerName: qsTr("Layer:\n") + layerEditor.layersModel.get(layerEditor.currentLayer).layerName

                btnColorPickerText: qsTr("Color:\n")+colorPic.selectedColorName

                btnLineThick: "LineThick:\n" + (lineThickEditor.currentLineThick-1)

                onBtnCorlorDlg: colorPic.visible = true

                onBtnDoubleClick: function(btnSignal){
                   if (btnSignal === Commands.Circle)
                      circleEditor.visible = true
                   if (btnSignal === Commands.Rectangle)
                      rectangleEditor.visible = true
                   if (btnSignal === Commands.Line)
                      lineEditor.visible = true

               }

               onBtnLayerEditor: layerEditor.visible = true
               onBtnLayers: layerSelect.visible = true
               onBtnLineThickEditor: lineThickEditor.visible = true

               onBtnSettings: mySettings.visible = true

               onBtnBlocksList: blocksList.visible = !blocksList.visible

               //TODO: replace above signals with the following onBtnClick generic signal
               //and change MenuHybrid to MenuButtonsTemplate
               onBtnClick: function(btnSignal){
                   if (btnSignal === Commands.Hamburger) {
                       copyArea.visible = true
                   }
               }

               onMenuGenericCommand: function(command){
                   genericCommand(command)

               }

            } //MenuHybrid


            MenuButtonsTemplate {
                id: menuCommands
                //width: parent.width
                //width: menuCommands.modelMenu.count * btnSize
                //anchors.top: menu.bottom
                //anchors.left: parent.left
                //anchors.margins: defaultMargins
                visible: menuCommands.modelMenu.count

                onBtnClick: function(btnSignal){
                    //currentButton = currentButton === typeName ? Commands.None : type
                    activeCommand = currentButton

                    if (btnSignal === Commands.Delete) {
                        genericCommand(btnSignal)
                        activeCommand = Commands.None
                        //hideMenuSecondary()
                    }
                }
                onBtnDoubleClick: function(btnSignal){
                    if (btnSignal === Commands.Offset) {
                        offsetEditor.visible = true
                    }
                }
            }//MenuCommands


            MenuButtonsTemplate {
                id: menuPolylineEnd

                visible: activeCommand === Commands.Polyline
                onVisibleChanged: {
                    visible ? menuPolylineEnd.modelMenu.append({"textName": "End", "typeName": Commands.PolylineExit})
                            : menuPolylineEnd.modelMenu.clear()


                    /*if (visible)
                        menuPolylineEnd.modelMenu.append({"textName": "End", "typeName": Commands.PolylineExit})
                    else
                        menuPolylineEnd.modelMenu.clear()
                    */
                }

                onBtnClick: function(btnSignal){

                    if (btnSignal === Commands.PolylineExit){
                        polyLineTemplate.receiveEvent(EventsQML.ReleaseRightClick, ma.getCursorPosWorld3DNode(), layer0)
                        menuPolylineEnd.clearMenu()
                        activeCommand = Commands.None
                    }

                }
            }//MenuButtonsTemplate

        }//Column

    }//Flickable


    CopyArea {
        id: copyArea
        visible: false
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onOk: visible = false
    }

    MySettingsDesktop {
        id: mySettingsDesktop
        width: 60*mm
        height: 85*mm
        visible: false
    }

    MySettingsMobile {
        id: mySettingsMobile
        visible: false
        anchors.fill: parent
    }

    BlocksListDesktop {
        id: blocksListDesktop

    }

    BlocksListMobile {
        id: blocksListMobile

    }

    ColorPicker {
        id: colorPic
        visible: false
        onOk: visible = false
        onCancel: visible = false
    }

    LayersEditor {
        id: layerEditor
        visible: false
        onCancel: visible = false
        onOk: visible = false
        onCurrentLayerColorChanged: {
            console.log("layerEditor.currentLayerColor: "+layerEditor.currentLayerColor)
            console.log("colorPic.modelColors.get(0).modelColor: "+colorPic.modelColors.get(0).modelColor)
            colorPic.modelColors.get(0).modelColor = layerEditor.currentLayerColor

            //menu.btnColorPickerColor = layerEditor.currentLayerColor
        }
    }

    LayerSelect{
        id: layerSelect
        visible: false

       onOk: layerSelect.visible = false
       onCancel: layerSelect.visible = false

    }

    LineThickEditor {
        id: lineThickEditor
        visible: false
        onCancel: visible = false
        onOk: {
            visible = false
        }
    }

    CircleEditorDesktop {
        id: circleEditorDesktop
        //visible: false
        circleEditor.onCancel: visible = false
        circleEditor.onOk: {
            visible = false
            circleTemplate.center = circleEditor.circleCenter
            circleTemplate.radius = circleEditor.circleRadius
            layer0.addCircle(circleTemplate.center, circleTemplate.radius)
        }
    }

    CircleEditorMobile {
        id: circleEditorMobile
        visible: false
        circleEditor.onCancel: visible = false
        circleEditor.onOk: {
            visible = false
            circleTemplate.center = circleEditor.circleCenter
            circleTemplate.radius = circleEditor.circleRadius
            layer0.addCircle(circleTemplate.center, circleTemplate.radius)
        }
    }

    property real offset: 5 //TODO: create a Item with global properties
    OffsetEditorDesktop {
        id: offsetEditorDesktop
        //visible: false
        offsetEd.onCancel: visible = false
        offsetEd.onOk: {
            offset = offsetEd.offsetValue
            visible = false
        }
    }


    OffSetEditorMobile {
        id: offsetEditorMobile
        visible: false
        offsetEd.onCancel: visible = false
        offsetEd.onOk: {
            offset = offsetEd.offsetValue
            visible = false
        }
    }

    RectangleEditorMobile {
        id: rectangleEditorMobile
        visible: false
        rectangleEditor.onCancel: visible = false
        rectangleEditor.onOk: {
            visible = false
            if (rectangleEditor.btnP1P2) {

                rectangleTemplate.p0 = rectangleEditor.p0
                rectangleTemplate.p3 = rectangleEditor.p3
                //layer0.addRectangle() //fix the number of arguments
            }
            if (rectangleEditor.btnSquareCenterSideLenght) {
                rectangleTemplate.p0 = rectangleEditor.squareP0
                rectangleTemplate.p3 = rectangleEditor.squareP3
                //layer0.addRectangle() //fix the number of arguments
            }
        }
    }

    RectangleEditorDesktop {
        id: rectangleEditorDesktop
        visible: false
        rectangleEditor.onCancel: visible = false
        rectangleEditor.onOk: {
            visible = false
            if (rectangleEditor.btnP1P2.checked) {
                layer0.addRectangle(rectangleEditor.p0, rectangleEditor.p1,
                                            rectangleEditor.p2, rectangleEditor.p3)
            }
            if (rectangleEditor.btnSquareCenterSideLenght.checked) {
                layer0.addRectangle(rectangleEditor.squareP0, rectangleEditor.squareP1,
                                            rectangleEditor.squareP2, rectangleEditor.squareP3)
            }
        }
    }

    LineEditorMobile {
        id: lineEditorMobile
        visible: false
        lineEditor.onOk: {
            visible = false

            if (lineEditor.btnP1P2.checked) {
                lineTemplate.p0 = lineEditor.p1
                lineTemplate.p1 = lineEditor.p2
                console.log("p1: "+lineEditor.p1)
                console.log("p2: "+lineEditor.p2)
                layer0.addLine(lineTemplate.p0, lineTemplate.p1)
            }
            if (lineEditor.btnP1DistAngle.checked) {
                lineTemplate.p0 = lineEditor.p
                lineTemplate.p1 = lineEditor.paux
                console.log("p: "+lineEditor.p)
                console.log("paux2: "+lineEditor.paux)
                layer0.addLine(lineTemplate.p0, lineTemplate.p1)
            }
        }
        lineEditor.onCancel: {
            visible = false
        }

    }

    LineEditorDesktop {
        id: lineEditorDesktop
        visible: false
        lineEditor.onOk: {
            visible = false

            if (lineEditor.btnP1P2.checked) {
                lineTemplate.p0 = lineEditor.p1
                lineTemplate.p1 = lineEditor.p2
                console.log("p1: "+lineEditor.p1)
                console.log("p2: "+lineEditor.p2)
                layer0.addLine(lineTemplate.p0, lineTemplate.p1)
            }
            if (lineEditor.btnP1DistAngle.checked) {
                lineTemplate.p0 = lineEditor.p
                lineTemplate.p1 = lineEditor.paux
                console.log("p: "+lineEditor.p)
                console.log("paux2: "+lineEditor.paux)
                layer0.addLine(lineTemplate.p0, lineTemplate.p1)
            }
        }
        lineEditor.onCancel: {
            visible = false
        }
    }

    EntitiesPropertiesEditorMobile {
        id: entitiesPropertiesEditorMobile
        visible: false
    }

    EntitiesPropertiesEditorDesktop {
        id: entitiesPropertiesEditorDesktop
        visible: false

        entitiesPropertiesEd.onOk: {

            visible = false
        }
        entitiesPropertiesEd.onCancel: {
            visible = false
        }
    }

    //--------------------------------------

    TextInput {
        id: textInputAux
        //anchors.centerIn: parent
        width: mainRoot.btnSize * 2
        height: font.pixelSize
        color: "red"
        clip: true
        visible: false
        font.pixelSize: 24
        onEditingFinished: {
            console.log("onEditingFinished: ")
            layer0.addText(textInputAux.text)
        }
    }

    FileDialog {
        id: fileDlgSave2Pdf
        title: "Save to pdf file"
        nameFilters: [ "PDF(*.pdf)" ]
        currentFolder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        fileMode: FileDialog.SaveFile

        onAccepted: {
            console.log("vou guardar o ficheiro pdf .......................")
            console.log("currentFolder: "+currentFolder)
            console.log("selectedFile: "+selectedFile)
            console.log("console.log(currentFolder + selectedFile: )"+currentFolder + selectedFile)
            drawer.setFilePathAndName(selectedFile)
            drawer.screenShot()
        }
    }


    FileDialog {
        id: fileDialogImage
        title: "Open a Image file"
        nameFilters: [ "PNG(*.png)","JPG(*.jpg)", "All files(*.*)" ]
        currentFolder: StandardPaths.standardLocations(StandardPaths.PicturesLocation)[0]
        onAccepted: image.source = selectedFile
    }


    //Loads pdf files in to the view
    FileDialog {
        id: fileDialogPdf
        title: "Open a Pdf file"
        nameFilters: [ "PDF(*.pdf)", "All files(*.*)" ]
        currentFolder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        onAccepted: doc.source = selectedFile
    }

    property vector3d text3DPosition
    onText3DPositionChanged: {
        console.log("onText3DPositionChanged: "+text3DPosition)
        console.log("textInputAux.visible: "+textInputAux.visible)
        //if (textInputAux.visible)
          //  layer0.addText(textInputAux.text)
    }

    function setText(xx: real, yy: real, text3DPos: vector3d) {

        textInputAux.text = ""
        textInputAux.x = xx
        textInputAux.y = yy
        textInputAux.visible = true
        textInputAux.focus = true
        text3DPosition = text3DPos
        console.log("setText text3DPos: "+text3DPos)
    }

    /*function pickEntities(point) {

        //console.log("pickEntities: "+point)
        var listEntitiesLenght = layer0.listEntities.length

        //circleTemplate.picking(point)

        //for (var i = 0; i < listEntitiesLenght; i++ ) {

          //  layer0.listEntities[i].picking(point)
            //let instance = layer0.listEntities[i]
            //instance.picking(point)

        //}

    }*/

    /*function pick(mouse) {
        console.log("Screen Position: (" + mouse.x + ", " + mouse.y + ")")
        pickPosition.text = "Screen Position: (" + mouse.x + ", " + mouse.y + ")"

        var result = view3D.pick(mouse.x, mouse.y);

        scenePosition.text = "World Position: ("
                + result.scenePosition.x.toFixed(2) + ", "
                + result.scenePosition.y.toFixed(2) + ")";

        console.log("World Position: ("+ result.scenePosition.x.toFixed(2) + ", "
         + result.scenePosition.y.toFixed(2) + ")")

        if (result.objectHit) {
            var pickedObject = result.objectHit;
            // Toggle the isPicked property for the model
            pickedObject.isPicked = !pickedObject.isPicked;
            //! [pick specifics]
        }
    }*/

    property bool bPickOnMouseMove: false

    function pick(mouse) {
        if (!bPickOnMouseMove)
            return;

        var result = view.pick(mouse.x, mouse.y)
        if (result.objectHit) {
            var pickedObject = result.objectHit;
            pickedObject.isPicked = !pickedObject.isPicked;
            bPickOnMouseMove = false
        }
    }//function pick(mouse


    function showMenuCommands() {
        menuCommands.currentButton = Commands.None
        menuCommands.modelMenu.clear()
        menuCommands.modelMenu.append({"textName": "copy", "typeName": Commands.Copy})
        menuCommands.modelMenu.append({"textName": "move", "typeName": Commands.Move})
        menuCommands.modelMenu.append({"textName": "scale", "typeName": Commands.Scale})
        menuCommands.modelMenu.append({"textName": "rotate", "typeName": Commands.Rotate})
        menuCommands.modelMenu.append({"textName": "delete", "typeName": Commands.Delete})
        menuCommands.modelMenu.append({"textName": "block", "typeName": Commands.Block})
        menuCommands.modelMenu.append({"textName": "offset", "typeName": Commands.Offset})
    }

    function hideMenuCommands() {
        menuCommands.modelMenu.clear()
    }

    function showMenuPreferences() {

        menuPreferences.modelMenu.clear()
        menuPreferences.modelMenu.append({"textName": qsTr("Save\nas"), "typeName": Commands.SaveAs})
        menuPreferences.modelMenu.append({"textName": "Image", "typeName": Commands.OpenDlgImages})
        menuPreferences.modelMenu.append({"textName": "Pdf", "typeName": Commands.OpenDlgPdfs})
        menuPreferences.modelMenu.append({"textName": "2Pdf", "typeName": Commands.Save2Pdf})
    }

}
