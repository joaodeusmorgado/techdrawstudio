import QtQuick 2.15

Item {
    enum Commands {
        None,
        Line,
        Polyline,
        Spline,
        PolylineExit,
        Circle,
        Rectangle,
        Text,
        Ortho,
        Selection,
        UnPick,
        Pan,
        Delete,
        Move,
        Scale,
        Rotate,
        Block,
        BlocksList,
        Offset,
        Hamburger,
        CopyArea,
        Copy,
        Fillet,
        Extend,
        Trim,
        OpenDlgImages,
        OpenDlgPdfs,
        Save2Pdf,
        SaveAs,
        QueryLineProperties,
        Length,
        Distance
    }
}
