import QtQuick 2.15
//EventsQML
Item {
    enum MyEvents {
        Press,
        MouseMove,
        Release, //any mouse release
        Click,
        ReleaseRightClick //especifically right click release
    }
}
