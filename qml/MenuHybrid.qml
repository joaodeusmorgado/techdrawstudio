import QtQuick 2.0
import QtQuick.Controls
import "components"
import "qmlEntities"
import "qmlEnums"

Item {
    id: menuHybrid
    //width: mainRoot.width
    width: mainRoot.width < flickableWidth ? mainRoot.width : flickableWidth
    height: btnSize

    property real flickableWidth: m_row.width + defaultMargins * 2

    property bool ortho: false
    //property int activeCommand: Commands.None
    property string btnLayerName: "Layer:\n0"
    property string btnLineThick: "LineThick:\n0"

    property alias btnColorPickerColor: m_colorPicker.colorClear
    property alias btnColorPickerText: m_colorPicker.text

    property alias btnLayerEditorColor: m_layer.colorClear


    signal btnClick(btnSignal: int)//generic click
    signal btnDoubleClick(btnSignal: int)//generic doubleClick
    signal btnTextDoubleClick()
    signal btnCorlorDlg()
    signal btnProperties()
    signal btnLayers()
    signal btnLayerEditor()
    signal btnPolyline()
    signal btnLineThickEditor()
    signal btnBlocksList()
    signal btnSettings()
    signal btnViewMenu()
    signal menuGenericCommand(int command)


    Flickable {
        id: flick
        width: parent.width // should be the size of items that fit in the screen
        height: btnSize
        contentWidth: flickableWidth //m_row.width + defaultMargins * 2
        contentHeight: height

        rebound: Transition {
            NumberAnimation {
                properties: "x,y"
                duration: 1000
                easing.type: Easing.OutBounce
            }
        }


        Row {
            id: m_row
            spacing: defaultMargins / 2

            ButtonHybrid {
                id: m_line
                iconName: "line256.png"
                type: Commands.Line
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Line)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("line")
            }

            ButtonHybrid {
                id: m_polyline
                iconName: "polyline256.png"
                type: Commands.Polyline
                isSelected: activeCommand === type
                onButtonClick: {
                    activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                    //if (activeCommand === type)
                    btnPolyline()
                }
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("polyline")
            }


            ButtonHybrid {
                id: m_spline
                iconName: "spline.png"
                type: Commands.Spline
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("spline")
            }

            ButtonHybrid {
                id: m_circle
                iconName: "circle256.png"
                type: Commands.Circle
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Circle)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("circle")
            }

            ButtonHybrid {
                id: m_rectangle
                iconName: "rectangle256.png"
                type: Commands.Rectangle
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Rectangle)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("rectangle")
            }

            ButtonHybrid {
                id: m_text
                iconName: "text.png"
                type: Commands.Text
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnTextDoubleClick()
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("text")
            }



            ButtonHybrid {
                id: m_ortho
                iconName: "ortho.png"
                type: Commands.Ortho
                isSelected: ortho //activeCommand === type
                //colorClear: "yellow"
                colorPressed: "lightblue"
                onButtonClick: ortho = !ortho
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("ortho mode")
            }

            ButtonHybrid {
                id: m_delete
                iconName: "delete.png"
                type: Commands.Delete
                //isSelected: activeCommand === type
                onButtonClick: menuGenericCommand(Commands.Delete)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("Delete entites")
            }

            ButtonHybrid {
                id: m_selection
                iconName: "selection.png"
                //colorClear: "FloralWhite"
                type: Commands.Selection
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("Select entites")
            }

            ButtonHybrid {
                id: m_unpick
                iconName: "unSelect.png"
                type: Commands.UnPick
                onButtonClick: {
                    menuGenericCommand(Commands.UnPick)
                }
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("Unselect all entites")
            }

            ButtonHybrid {
                id: m_fillet
               // iconName: "selection.png"
                text: qsTr("fillet")
                type: Commands.Fillet
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Fillet)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("fillet: join two lines")
            }

            ButtonHybrid {
                id: m_extend
               // iconName: "selection.png"
                text: qsTr("extend")
                type: Commands.Extend
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Extend)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("extend: extend lines to reference line")
            }

            ButtonHybrid {
                id: m_trim
               // iconName: "selection.png"
                text: qsTr("trim")
                type: Commands.Trim
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                onButtonDoubleClick: btnDoubleClick(Commands.Trim)
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("trim: trim one line intersection with other")
            }

            ButtonHybrid {
                id: m_pan
                iconName: "pan.png"
                //colorClear: "FloralWhite"
                type: Commands.Pan
                isSelected: activeCommand === type
                onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("pan")
            }

            ButtonHybrid {
                id: m_layerEditor
                //width: btnSize*2
                iconName: "layer256.png"
                //colorClear: "FloralWhite"
                //type: "layerEditor"
                //isSelected: activeCommand === type
                //iconVisible: false
                //text: btnLayerName
                onButtonClick: btnLayerEditor()
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("layers editor")
            }

            ButtonHybrid {
                id: m_layer
                visible: true
                width: btnSize*2
                iconName: "layer256.png"
                //colorClear: "FloralWhite"
                //type: "layerEditor"
                //isSelected: activeCommand === type
                iconVisible: false
                text: btnLayerName
                onButtonClick: btnLayers()
            }

            ButtonHybrid {
                id: m_colorPicker
                width: btnSize*2
                iconName: "colorCircleAlphaBackground256.png"
                iconVisible: false
                colorClear: "lightblue"

                //onButtonClick: activeCommand !== type ? activeCommand = type : activeCommand = Commands.None
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("colors editor")

                onButtonClick: {
                    btnCorlorDlg()
                }
            }

            ButtonHybrid {
                id: m_lineThick
                width: btnSize*2
                iconName: "layer256.png"
                //colorClear: "FloralWhite"
                //type: "layerEditor"
                //isSelected: activeCommand === type
                iconVisible: false
                text: btnLineThick
                onButtonClick: btnLineThickEditor()
            }

            ButtonHybrid {
                id: m_blockList
                iconVisible: false
                type: Commands.BlocksList
                text: qsTr("Block")
                onButtonClick: btnBlocksList()
            }

            ButtonHybrid {
                id: m_settings
                iconName: "settings.png"
                //colorClear: "FloralWhite"
                //type: "settings"
                //isSelected: activeCommand === type
                onButtonClick: btnSettings()
                ToolTip.visible: containsMouse
                ToolTip.text: qsTr("settings")
            }

            ButtonHybrid {
                id: m_menuView
                iconName: "menuHamburger.png"
                //colorClear: "FloralWhite"
                type: Commands.Hamburger
                isSelected: activeCommand === type
                onButtonClick: {
                    btnViewMenu()
                    btnClick(Commands.Hamburger)
                }
            }

        }
    }

}
