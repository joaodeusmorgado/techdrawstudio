#ifndef MATHLIB_H
#define MATHLIB_H

#include <QObject>
#include <QVector3D>

class MathLib : public QObject
{
    Q_OBJECT
public:
    MathLib(QObject *parent = 0);

    Q_INVOKABLE QVector3D sum3D(const QVector3D &p0, const QVector3D &p1) const;
    Q_INVOKABLE QVector3D sub3D(const QVector3D &p0, const QVector3D &p1) const;

    Q_INVOKABLE bool distance3DDelta(const QVector3D &p1, const QVector3D &p2, const float &delta) const;
    Q_INVOKABLE float distance3D(const QVector3D &p1, const QVector3D &p2) const;

    Q_INVOKABLE float distance2D(const float &a, const float &b) const;

    Q_INVOKABLE bool checkPointInLine(const QVector3D &p0, const QVector3D &p1,
                                      const QVector3D &p, const float &delta) const;

    Q_INVOKABLE float angleFromPoints(const QVector3D &centerPoint, const QVector3D &radiusPoint) const;

    Q_INVOKABLE QVector3D rotate2d(const QVector3D &p, const float &angle) const;

    Q_INVOKABLE QVector3D rotate2DFromPoint(const QVector3D &center, const float &angle, const QVector3D &p) const;

    //some rectangles that are set with p0, p2, vertices p1 and p3 are set automatically
    // the following functions calculate p1 and p3
    Q_INVOKABLE QVector3D rectangleP0P2GetP1(const QVector3D &p0, const QVector3D &p2) const;
    Q_INVOKABLE QVector3D rectangleP0P2GetP3(const QVector3D &p0, const QVector3D &p2) const;

    /*
    p0------p1
    |       |
    |       |
    p3------p2
    */

    Q_INVOKABLE float average(const float &a, const float &b) const;

    Q_INVOKABLE bool segmentFullyInsideRectangle(const QVector3D &lineP0, const QVector3D &lineP1,
                                              const QVector3D &rectP0, const QVector3D &rectP2/*, QVector3D &p*/) const;

    Q_INVOKABLE bool checkSegmentsIntersection(const QVector3D &p0, const QVector3D &p1,
                                              const QVector3D &l0, const QVector3D &l1) const;


    //Q_INVOKABLE bool checkSegmentsIntersection(const QVector3D &p0, const QVector3D &p1,
      //                                        const QVector3D &l0, const QVector3D &l1, QVector3D &intersection) const;

    Q_INVOKABLE bool isLinesIntersection(const QVector3D &l0, const QVector3D &l1,
                                         const QVector3D &r0, const QVector3D &r1) const;

    Q_INVOKABLE QVector3D getLinesIntersection(const QVector3D &l0, const QVector3D &l1,
                                         const QVector3D &r0, const QVector3D &r1) const;

    Q_INVOKABLE bool checkLineRectangleIntersection(const QVector3D &l0, const QVector3D &l1,
                                              const QVector3D &r0, const QVector3D &r2) const;

    //Q_INVOKABLE bool segmentIntersectRectangle(const QVector3D &l0, const QVector3D &l1,
      //                                      const QVector3D &r0, const QVector3D &r2) const;

    Q_INVOKABLE bool circleInsideRectangle(const QVector3D &center, const float &radius,
                                                const QVector3D &r0, const QVector3D &r2) const;

    Q_INVOKABLE bool circleOutsideRectangle(const QVector3D &center, const float &radius,
                                                const QVector3D &r0, const QVector3D &r2) const;



    Q_INVOKABLE bool circleIntersectRectangle(const QVector3D &center, const float &radius,
                                                const QVector3D &r0, const QVector3D &r2) const;


    Q_INVOKABLE float rectXmin(const QVector3D &r0, const QVector3D &r2) const;
    Q_INVOKABLE float rectXmax(const QVector3D &r0, const QVector3D &r2) const;

    Q_INVOKABLE float rectYmin(const QVector3D &r0, const QVector3D &r2) const;
    Q_INVOKABLE float rectYmax(const QVector3D &r0, const QVector3D &r2) const;

    Q_INVOKABLE float min(const float &a, const float &b) const;
    Q_INVOKABLE float max(const float &a, const float &b) const;

    Q_INVOKABLE bool filletCalculate(const QVector3D &l0, const QVector3D &l1, const QVector3D &pickPointL,
                                        const QVector3D &r0, const QVector3D &r1, const QVector3D &pickPointR);

    Q_INVOKABLE QVector3D fillet_l0() const {return m_fillet_l0;}
    Q_INVOKABLE QVector3D fillet_l1() const {return m_fillet_l1;}
    Q_INVOKABLE QVector3D fillet_r0() const {return m_fillet_r0;}
    Q_INVOKABLE QVector3D fillet_r1() const {return m_fillet_r1;}
    Q_INVOKABLE QVector3D fillet_intersection() const {return m_fillet_intersection;}

    Q_INVOKABLE bool trimCalculate(const QVector3D &l0, const QVector3D &l1,
                                            const QVector3D &r0, const QVector3D &r1, const QVector3D &pickPointR);
    Q_INVOKABLE QVector3D trimLine_r0() const {return m_trim_lines_r0;}
    Q_INVOKABLE QVector3D trimLine_r1() const {return m_trim_lines_r1;}

    Q_INVOKABLE void setExtendReferenceLine(const QVector3D &p0, const QVector3D &p1);
    Q_INVOKABLE bool setExtendLine(const QVector3D &p0, const QVector3D &p1);
    Q_INVOKABLE QVector3D extendLine_p0() const {return m_extend_lines_p0;}
    Q_INVOKABLE QVector3D extendLine_p1() const {return m_extend_lines_p1;}

    Q_INVOKABLE void setOffset(const float &offset);
    Q_INVOKABLE void offsetLineCalculate(const QVector3D &p0, const QVector3D &p1, const QVector3D &side);
    Q_INVOKABLE QVector3D offsetLineP0(){return m_offsetLineP0;}
    Q_INVOKABLE QVector3D offsetLineP1(){return m_offsetLineP1;}
    Q_INVOKABLE bool offsetCircleCalculate(const QVector3D &center, const float &radius, const QVector3D &side);
    Q_INVOKABLE float offsetCircleRadius(){return m_offsetCircleRadius;}

private:
    QVector3D m_fillet_l0;
    QVector3D m_fillet_l1;
    QVector3D m_fillet_r0;
    QVector3D m_fillet_r1;
    QVector3D m_fillet_intersection;

    QVector3D m_trim_lines_r0;
    QVector3D m_trim_lines_r1;

    QVector3D m_extend_referenceLine_p0;
    QVector3D m_extend_referenceLine_p1;
    QVector3D m_extend_lines_p0;
    QVector3D m_extend_lines_p1;
    QVector3D m_extend_intersection;

    float m_offset;
    QVector3D m_offsetLineP0;
    QVector3D m_offsetLineP1;
    //QVector3D m_offsetCircleCenter;
    float m_offsetCircleRadius;

};

#endif // MATHLIB_H
