#include "entity_rectangle.h"

Entity_Rectangle::Entity_Rectangle()
{
    setCount(5);
}

void Entity_Rectangle::setP0(const QVector3D &p0)
{
    if (m_p0 == p0)
        return;
    m_p0 = p0;
   // m_p1.setY(m_p0.y());
    //m_p2.setX(m_p0.x());
    //setP1_from_P0_P3();
    //setP2_from_P0_P3();
    updateGeometry();
    emit p0Changed(m_p0);
    //qDebug()<<"Entity_Rectangle p0: "<<p0;
}

void Entity_Rectangle::setP0(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP0(v);
}

void Entity_Rectangle::setP1(const QVector3D &p1)
{
    if (m_p1 == p1)
        return;
    m_p1 = p1;
    updateGeometry();
    emit p1Changed(m_p1);
    //qDebug()<<"Entity_Rectangle p1: "<<p1;
}

void Entity_Rectangle::setP1(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP1(v);
}

void Entity_Rectangle::setP2(const QVector3D &p2)
{
    if (m_p2 == p2)
        return;
    m_p2 = p2;
    updateGeometry();
    emit p2Changed(p2);
}

void Entity_Rectangle::setP2(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP2(v);
}

void Entity_Rectangle::setP3(const QVector3D &p3)
{
    //qDebug()<<"Entity_Rectangle p3"<<p3;
    if (m_p3 == p3)
        return;
    m_p3 = p3;
    //m_p2.setY(m_p3.y());
    //m_p1.setX(m_p3.x());
    //setP1_from_P0_P3();
    //setP2_from_P0_P3();
    updateGeometry();
    emit p3Changed(m_p3);
}

void Entity_Rectangle::setP3(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP3(v);
}

//-----------------------------
/*
void Entity_Rectangle::setP1_from_P0_P3()
{
    setP1(m_p3.x(), m_p0.y(), m_p0.z());//todo: check if p1.z should be the average of po.z and p3.z
    qDebug()<<"Entity_Rectangle setP1_from_P0_P3: "<<m_p1;
}

void Entity_Rectangle::setP2_from_P0_P3()
{
    setP2(m_p0.x(), m_p3.y(), m_p3.z());//todo: check if p2.z should be the average of po.z and p3.z
    qDebug()<<"Entity_Rectangle setP2_from_P0_P3: "<<m_p2;
}*/


void Entity_Rectangle::updateGeometry()
{
    clear();

    m_vertexData.resize(m_count * 3 * sizeof (float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    *p++ = m_p0.x();
    *p++ = m_p0.y();
    *p++ = m_p0.z();

    *p++ = m_p1.x();
    *p++ = m_p1.y();
    *p++ = m_p1.z();

    *p++ = m_p2.x();
    *p++ = m_p2.y();
    *p++ = m_p2.z();

    *p++ = m_p3.x();
    *p++ = m_p3.y();
    *p++ = m_p3.z();

    *p++ = m_p0.x();
    *p++ = m_p0.y();
    *p++ = m_p0.z();

    setVertexData(m_vertexData);
    m_vertexData.clear();
    setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Lines);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Points);
    setStride(3 * sizeof(float));
    setBounds(m_p0, m_p2);
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    update();
}

