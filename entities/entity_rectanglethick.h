#ifndef ENTITY_RECTANGLETHICK_H
#define ENTITY_RECTANGLETHICK_H


#include <QObject>
#include "entity.h"

class Entity_RectangleThick : public Entity
{
    Q_OBJECT
    Q_PROPERTY(QVector3D p0 READ p0 WRITE setP0 NOTIFY p0Changed)
    Q_PROPERTY(QVector3D p1 READ p1 WRITE setP1 NOTIFY p1Changed)
    Q_PROPERTY(QVector3D p2 READ p2 WRITE setP2 NOTIFY p2Changed)
    Q_PROPERTY(QVector3D p3 READ p3 WRITE setP3 NOTIFY p3Changed)
public:
    Entity_RectangleThick();

    //~Entity_Line();
    int type()const override{return EntityType::Rectangle;}

    QVector3D p0() const {return m_p0;}
    QVector3D p1() const {return m_p1;}
    QVector3D p2() const {return m_p2;}
    QVector3D p3() const {return m_p3;}

public slots:
    //to create a new rectangle use setP0(), setP3()
    //when draggin a vertex to create a deformed rectangle, use updateP0(), updateP1(), ....
    void setP0(const QVector3D &p0);
    void setP0(const float &px, const float &py, const float &pz);
    void setP1(const QVector3D &p1);
    void setP1(const float &px, const float &py, const float &pz);
    void setP2(const QVector3D &p2);
    void setP2(const float &px, const float &py, const float &pz);
    void setP3(const QVector3D &p3);
    void setP3(const float &px, const float &py, const float &pz);
    void saveGeometryCorners() override;

    // we usually only specify 2 rectangle vertices, p0, and p3;
    // p1 and p2 are automatically created from p0 and p3 coordinates
    /*
    p0------p1
    |       |
    |       |
    p2------p3

    Update: Dont update p1 and p2 in here, change to
    p0------p1
    |       |
    |       |
    p3------p2

    and update p1 and p3 in entity creation, this will allow for rectangle deformation
*/


signals:
    void p0Changed(QVector3D newP0);
    void p1Changed(QVector3D newP1);
    void p2Changed(QVector3D newP2);
    void p3Changed(QVector3D newP3);

private:
    void updateGeometry() override;
    QVector3D m_p0;
    QVector3D m_p1;
    QVector3D m_p2;
    QVector3D m_p3;
};

#endif // ENTITY_RECTANGLETHICK_H
