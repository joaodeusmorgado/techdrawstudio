#ifndef ENTITY_LINE_H
#define ENTITY_LINE_H

#include <QObject>
#include "entity.h"

//Don't use this class, it uses primitive Lines and Line width is not support in all plataforms
//Use Entity_LineThick, that uses primitive TriangleStrip to draw a rectangle that can emulate
// a line with Width


class Entity_Line : public Entity
{
    Q_OBJECT
    Q_PROPERTY(QVector3D p0 READ p0 WRITE setP0 NOTIFY p0Changed)
    Q_PROPERTY(QVector3D p1 READ p1 WRITE setP1 NOTIFY p1Changed)
public:
    Entity_Line();

    int type()const override{return EntityType::Line;}

    QVector3D p0() const {return m_P0;}
    QVector3D p1() const {return m_P1;}

public slots:
    void setP0(const QVector3D &p0);
    void setP0(const float &px, const float &py, const float &pz);
    void setP1(const QVector3D &p1);
    void setP1(const float &px, const float &py, const float &pz);

signals:
    void p0Changed(QVector3D newP0);
    void p1Changed(QVector3D newP1);

private:
    void updateGeometry() override;
    QVector3D m_P0;
    QVector3D m_P1;
};

#endif // ENTITY_LINE_H
