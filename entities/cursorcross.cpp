#include "cursorcross.h"

CursorCross::CursorCross(QQuick3DObject *ptr) :
    QQuick3DGeometry(ptr),
    m_count(16),
    m_squareDist(5),
    m_lineDist(50),
    m_cursorType(Cursors::normal)
{
    m_colorHor.setRgbF(1,0,0,1);
    m_colorVert.setRgbF(0,0.4,1,1);
    m_colorRect.setRgbF(1,1,1,1);

    updateGeometry();
}

void CursorCross::setCenter(const QVector3D &center)
{
    if (m_center == center)
        return;
    m_center = center;
    updateGeometry();
    emit centerChanged(m_center);
}

void CursorCross::setCenter(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setCenter(v);
}

void CursorCross::setColorVert(const QColor &colorVert)
{
    if (m_colorVert == colorVert)
        return;
    m_colorVert = colorVert;
    updateGeometry();
    emit colorVertChanged(m_colorVert);
}

void CursorCross::setColorHor(const QColor &colorHor)
{
    if (m_colorHor == colorHor)
        return;
    m_colorHor = colorHor;
    updateGeometry();
    emit colorHorChanged(m_colorHor);
}

void CursorCross::setColorRect(const QColor &colorRect)
{
    if (m_colorRect == colorRect)
        return;
    m_colorRect = colorRect;
    updateGeometry();
    emit colorRectChanged(m_colorRect);
}

void CursorCross::setSquareDist(const float &squareDist)
{
    if (m_squareDist == squareDist)
        return;
    m_squareDist = squareDist;
    updateGeometry();
    emit squareDistChanged(m_squareDist);
}

void CursorCross::setLineDist(const float &lineDist)
{
    if (m_lineDist == lineDist)
        return;
    m_lineDist = lineDist;
    updateGeometry();
    emit cursorTypeChanged(m_lineDist);
}

void CursorCross::setCursorType(const int &cursorType)
{
    if (m_cursorType == cursorType)
        return;
    m_cursorType = cursorType;
    updateGeometry();
    emit cursorTypeChanged(m_cursorType);
}

void CursorCross::updateGeometry()
{
    clear();


    if (m_cursorType == Cursors::normal)
        m_count = 16;
    if (m_cursorType == Cursors::rectangle)
        m_count = 8;


    //qDebug()<<"CursorCross::updateGeometry()......................";
    //qDebug()<<"m_count * 7 * sizeof(float): "<<m_count * 7 * sizeof(float);
    m_vertexData.resize(m_count * 7 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());


    if (m_cursorType == Cursors::normal)
    {
        // line vert up
        *p++ = m_center.x();
        *p++ = m_center.y() + m_squareDist;
        *p++ = m_center.z();

        *p++ = m_colorVert.redF(); // color
        *p++ = m_colorVert.greenF(); // color
        *p++ = m_colorVert.blueF(); // color
        *p++ = m_colorVert.alphaF(); // color

        *p++ = m_center.x();
        *p++ = m_center.y() + m_squareDist + m_lineDist;
        *p++ = m_center.z();

        *p++ = m_colorVert.redF(); // color
        *p++ = m_colorVert.greenF(); // color
        *p++ = m_colorVert.blueF(); // color
        *p++ = m_colorVert.alphaF(); // color


        // line vert down
        *p++ = m_center.x();
        *p++ = m_center.y() - m_squareDist;
        *p++ = m_center.z();

        *p++ = m_colorVert.redF(); // color
        *p++ = m_colorVert.greenF(); // color
        *p++ = m_colorVert.blueF(); // color
        *p++ = m_colorVert.alphaF(); // color

        *p++ = m_center.x();
        *p++ = m_center.y() - m_squareDist - m_lineDist;
        *p++ = m_center.z();

        *p++ = m_colorVert.redF(); // color
        *p++ = m_colorVert.greenF(); // color
        *p++ = m_colorVert.blueF(); // color
        *p++ = m_colorVert.alphaF(); // color

        // line horizontal right
        *p++ = m_center.x() + m_squareDist;
        *p++ = m_center.y();
        *p++ = m_center.z();

        *p++ = m_colorHor.redF(); // color
        *p++ = m_colorHor.greenF(); // color
        *p++ = m_colorHor.blueF(); // color
        *p++ = m_colorHor.alphaF(); // color

        *p++ = m_center.x()  + m_squareDist + m_lineDist;
        *p++ = m_center.y();
        *p++ = m_center.z();

        *p++ = m_colorHor.redF(); // color
        *p++ = m_colorHor.greenF(); // color
        *p++ = m_colorHor.blueF(); // color
        *p++ = m_colorHor.alphaF(); // color

        // line horizontal left
        *p++ = m_center.x() - m_squareDist;
        *p++ = m_center.y();
        *p++ = m_center.z();

        *p++ = m_colorHor.redF(); // color
        *p++ = m_colorHor.greenF(); // color
        *p++ = m_colorHor.blueF(); // color
        *p++ = m_colorHor.alphaF(); // color

        *p++ = m_center.x()  - m_squareDist - m_lineDist;
        *p++ = m_center.y();
        *p++ = m_center.z();

        *p++ = m_colorHor.redF(); // color
        *p++ = m_colorHor.greenF(); // color
        *p++ = m_colorHor.blueF(); // color
        *p++ = m_colorHor.alphaF(); // color
    }



    // rectangle upper line
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    // rectangle lower line
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    // rectangle right line
    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    // rectangle left line
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    *p++ = m_colorRect.redF(); // color
    *p++ = m_colorRect.greenF(); // color
    *p++ = m_colorRect.blueF(); // color
    *p++ = m_colorRect.alphaF(); // color

    setVertexData(m_vertexData);
    m_vertexData.clear();
    setStride(7 * sizeof(float));
    setPrimitiveType(QQuick3DGeometry::PrimitiveType::Lines);

    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(QQuick3DGeometry::Attribute::ColorSemantic, 3 * sizeof(float), Attribute::F32Type);
    update();

}
