#include "entity_line.h"

Entity_Line::Entity_Line()
{
    setCount(2);
}

void Entity_Line::setP0(const QVector3D &p0)
{
    if (m_P0 == p0)
        return;
    m_P0 = p0;
    updateGeometry();
    emit p0Changed(p0);
}

void Entity_Line::setP0(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP0(v);
}

void Entity_Line::setP1(const QVector3D &p1)
{
    //qDebug()<<"Entity_Line::setP1(const QVector3D &p1)......................";
    //qDebug()<<"p1: "<<p1;
    //qDebug()<<"m_P1: "<<m_P1;
    if (m_P1 == p1)
        return;
    //qDebug()<<"m_P1 = p1";
    m_P1 = p1;
    updateGeometry();
    emit p1Changed(p1);
}

void Entity_Line::setP1(const float &px, const float &py, const float &pz)
{
    qDebug()<<"Entity_Line::setP1(const float &px, const float &py, const float &pz)......................";
    QVector3D v(px, py, pz);
    setP1(v);
}

void Entity_Line::updateGeometry()
{
    clear();

    qDebug()<<"Entity_Line::updateGeometry()......................";

    qDebug()<<"m_count * 5 * sizeof(float): "<<m_count * 5 * sizeof(float);
    m_vertexData.resize(m_count * 5 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    *p++ = m_P0.x();
    *p++ = m_P0.y();
    *p++ = m_P0.z();

    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = m_P1.x();
    *p++ = m_P1.y();
    *p++ = m_P1.z();

    *p++ = 1.0f; // U
    *p++ = 1.0f; // V

    qDebug()<<"m_P1.x(): "<<m_P1.x();
    setVertexData(m_vertexData);
    m_vertexData.clear();
    setStride(5 * sizeof(float));

    setPrimitiveType(QQuick3DGeometry::PrimitiveType::Lines);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Points);

    setBounds(m_P0, m_P1);
    //addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);

    setStride(5 * sizeof(float));
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(QQuick3DGeometry::Attribute::TexCoordSemantic, 3 * sizeof(float), Attribute::F32Type);
    update();
}

