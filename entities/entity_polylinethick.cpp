#include "entity_polylinethick.h"

Entity_PolyLineThick::Entity_PolyLineThick()
{
    //setCount(4);

    //Entity_LineThick::createGeometry();
    Entity_PolyLineThick::updateGeometry();

    //connect(this,SIGNAL(p0Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
    //connect(this,SIGNAL(p1Changed(QVector3D)), this, SLOT(saveGeometryCorners()));

}

void Entity_PolyLineThick::addPn(const QVector3D &p_)
{
    m_pointsList.append(p_);
    updateGeometry();
    emit sizeChanged(m_pointsList.size());
}

void Entity_PolyLineThick::setPointsList(QList<QVector3D> pointsList_)
{
    if (m_pointsList == pointsList_)
        return;
    m_pointsList = pointsList_;
    updateGeometry();
    emit pointsListChanged(m_pointsList);
}

/*void Entity_PolyLineThick::setPn(const int &index, const QVector3D &p_)
{
    if (m_pointsList.at(index) == p_)
        return;
    m_pointsList[index] = p_;
    updateGeometry();
    //emit p0Changed(p0);
}*/

void Entity_PolyLineThick::updateLastPoint(const QVector3D &p_)
{
    if (m_pointsList.isEmpty())
        return;
    int i = m_pointsList.size() - 1;
    m_pointsList[i] = p_;
    updateGeometry();
}

void Entity_PolyLineThick::removeLastPoint()
{
    if (!m_pointsList.isEmpty()){
        m_pointsList.removeLast();
        updateGeometry();
    }
}

void Entity_PolyLineThick::reset()
{
    m_pointsList.clear();
    updateGeometry();
}

void Entity_PolyLineThick::saveGeometryCorners()
{
    if (m_count == 0)
        return;

    setMinXRef(m_pointsList[0].x());
    setMinYRef(m_pointsList[0].y());
    setMinZRef(m_pointsList[0].z());

    setMaxXRef(m_pointsList[0].x());
    setMaxYRef(m_pointsList[0].y());
    setMaxZRef(m_pointsList[0].z());

    for(auto i = 0;i<m_pointsList.size()-1; i++){

        setMinX(m_pointsList[i].x());
        setMinX(m_pointsList[i+1].x());

        setMinY(m_pointsList[i].y());
        setMinY(m_pointsList[i+1].y());

        setMinZ(m_pointsList[i].z());
        setMinZ(m_pointsList[i+1].z());

        setMaxX(m_pointsList[i].x());
        setMaxX(m_pointsList[i+1].x());

        setMaxY(m_pointsList[i].y());
        setMaxY(m_pointsList[i+1].y());

        setMaxZ(m_pointsList[i].z());
        setMaxZ(m_pointsList[i+1].z());

    }

}

void Entity_PolyLineThick::updateGeometry()
{
    clear();

    int pointsCount = m_pointsList.size();
    if (pointsCount < 2)
        return;

    int size = (m_pointsList.size() -2) * 4 + 4;

    m_vertexData.resize(size * 5 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    for (int i = 1; i < pointsCount; i++)
    {
        QVector3D p0 = m_pointsList[i-1];

        QVector3D p1 = m_pointsList[i];

        float angle = atan2(p1.y() - p0.y(), p1.x() - p0.x()) + M_PI_2;

        float halfThick = m_thick * 0.5;

        float deltaX = halfThick * qCos(angle);
        float deltaY = halfThick * qSin(angle);

        QVector3D v0(p0.x() - deltaX, p0.y() - deltaY, p0.z());
        QVector3D v1(p1.x() - deltaX, p1.y() - deltaY, p1.z());
        QVector3D v2(p0.x() + deltaX, p0.y() + deltaY, p0.z());
        QVector3D v3(p1.x() + deltaX, p1.y() + deltaY, p1.z());

        // v2---------v3
        // p0---------p1
        // v0---------v1

        //type is in radians, if you need it in degrees just do angle * 180 / PI
        *p++ = v0.x();
        *p++ = v0.y();
        *p++ = v0.z();
        *p++ = 0.0f; // U
        *p++ = 0.0f; // V

        *p++ = v1.x();
        *p++ = v1.y();
        *p++ = v1.z();
        *p++ = 1.0f; // U
        *p++ = 0.0f; // V

        *p++ = v2.x();
        *p++ = v2.y();
        *p++ = v2.z();
        *p++ = 0.0f; // U
        *p++ = 1.0f; // V

        *p++ = v3.x();
        *p++ = v3.y();
        *p++ = v3.z();
        *p++ = 1.0f; // U
        *p++ = 1.0f; // V
    }

    setVertexData(m_vertexData);
    m_vertexData.clear();
    setStride(size * sizeof(float));

    setPrimitiveType(QQuick3DGeometry::PrimitiveType::TriangleStrip);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Triangles);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);

    //setBounds(p0, p3); //TODO: check the min, max values and if necessary invert them

    setStride(5 * sizeof(float));
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(QQuick3DGeometry::Attribute::TexCoordSemantic, 3 * sizeof(float), Attribute::F32Type);
    update();

    saveGeometryCorners();
}
