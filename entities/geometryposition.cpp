#include "geometryposition.h"

GeometryPosition::GeometryPosition() :
    m_minX(0),
    m_minY(0),
    m_minZ(0),
    m_maxX(0),
    m_maxY(0),
    m_maxZ(0)
{

}

void GeometryPosition::setMinX(const float &minX)
{
    if (m_minX > minX) m_minX = minX;
}

void GeometryPosition::setMinY(const float &minY)
{
    if (m_minY > minY) m_minY = minY;
}

void GeometryPosition::setMinZ(const float &minZ)
{
    if (m_minZ > minZ) m_minZ = minZ;
}

void GeometryPosition::setMaxX(const float &maxX)
{
    if (m_maxX < maxX) m_maxX = maxX;
}

void GeometryPosition::setMaxY(const float &maxY)
{
    if (m_maxY < maxY) m_maxY = maxY;
}

void GeometryPosition::setMaxZ(const float &maxZ)
{
    if (m_maxZ < maxZ) m_maxZ = maxZ;
}


QVector3D GeometryPosition::middlePoint()
{
    return QVector3D( (m_maxX+m_minX)/2, (m_maxY+m_minY)/2, (m_maxZ+m_minZ)/2);
}
