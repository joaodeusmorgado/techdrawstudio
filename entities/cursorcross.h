#ifndef CURSORCROSS_H
#define CURSORCROSS_H

#include <QQuick3DGeometry>
#include <QObject>
#include <QVector3D>
#include <QColor>

class CursorCross : public QQuick3DGeometry
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(QVector3D center READ center WRITE setCenter NOTIFY centerChanged)
    Q_PROPERTY(QColor colorVert READ colorVert WRITE setColorVert NOTIFY colorVertChanged)
    Q_PROPERTY(QColor colorHor READ colorHor WRITE setColorHor NOTIFY colorHorChanged)
    Q_PROPERTY(QColor colorRect READ colorRect WRITE setColorRect NOTIFY colorRectChanged)
    Q_PROPERTY(float squareDist READ squareDist WRITE setSquareDist NOTIFY squareDistChanged)
    Q_PROPERTY(float lineDist READ lineDist WRITE setLineDist NOTIFY lineDistChanged)
    Q_PROPERTY(int cursorType READ cursorType WRITE setCursorType NOTIFY cursorTypeChanged)

public:
    CursorCross(QQuick3DObject *ptr = nullptr);

    enum Cursors{ normal, rectangle, cross};
    Q_ENUM(Cursors)

    QVector3D center() const { return m_center; }
    QColor colorVert() const { return m_colorVert; }
    QColor colorHor() const { return m_colorHor; }
    QColor colorRect() const { return m_colorRect; }
    float squareDist() const { return m_squareDist; }
    float lineDist() const { return m_lineDist; }
    int cursorType() const { return m_cursorType; }

public slots:
    void setCenter(const QVector3D &center);
    void setCenter(const float &px, const float &py, const float &pz);
    void setColorVert(const QColor &colorVert);
    void setColorHor(const QColor &colorHor);
    void setColorRect(const QColor &colorRect);
    void setSquareDist(const float &squareDist);
    void setLineDist(const float &lineDist);
    void setCursorType(const int &cursorType);

signals:
    void centerChanged(QVector3D newCenter);
    void colorVertChanged(QColor newColorVert);
    void colorHorChanged(QColor newColorHor);
    void colorRectChanged(QColor newColorRect);
    void squareDistChanged(float newSquareDist);
    void lineDistChanged(float newLineDist);
    void cursorTypeChanged(int newCursorType);

public:
    void updateGeometry();

private:
    QByteArray m_vertexData;
    int m_count;
    QColor m_colorRect;
    QColor m_colorVert;
    QColor m_colorHor;
    QVector3D m_center;
    float m_squareDist;
    float m_lineDist;
    int m_cursorType;
};

#endif // CURSORCROSS_H
