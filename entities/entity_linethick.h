#ifndef ENTITY_LINETHICK_H
#define ENTITY_LINETHICK_H

#include <QObject>
#include "entity.h"

class Entity_LineThick : public Entity
{
    Q_OBJECT
    Q_PROPERTY(QVector3D p0 READ p0 WRITE setP0 NOTIFY p0Changed)
    Q_PROPERTY(QVector3D p1 READ p1 WRITE setP1 NOTIFY p1Changed)
    //Q_PROPERTY(float thick READ thick WRITE setThick NOTIFY thickChanged)

public:
    Entity_LineThick();

    int type()const override{return EntityType::LineThick;}

    QVector3D p0() const {return m_p0;}
    QVector3D p1() const {return m_p1;}
    //float thick() const {return m_thick;}

public slots:
    void setP0(const QVector3D &p0);
    void setP0(const float &px, const float &py, const float &pz);
    void setP1(const QVector3D &p1);
    void setP1(const float &px, const float &py, const float &pz);
     //void setThick(const float &thick_);
    void updateP0P1(const QVector3D &p0_, const QVector3D &p1_);
    void saveGeometryCorners() override;

signals:
    void p0Changed(QVector3D newP0);
    void p1Changed(QVector3D newP1);
   //void thickChanged(float newThick);

//private:
    //void createGeometry();
public:
    void updateGeometry() override;
    bool pick(const QVector3D &pickPoint) override;

private:
    QVector3D m_p0;
    QVector3D m_p1;



    //float m_thick;
};

#endif // ENTITY_LINETHICK_H
