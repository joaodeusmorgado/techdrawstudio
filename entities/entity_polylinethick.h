#ifndef ENTITY_POLYLINETHICK_H
#define ENTITY_POLYLINETHICK_H

#include "entity.h"
#include <QObject>

class Entity_PolyLineThick : public Entity
{
    Q_OBJECT
    Q_PROPERTY(QList<QVector3D> pointsList READ pointsList WRITE setPointsList NOTIFY pointsListChanged)
    Q_PROPERTY(int size READ size NOTIFY sizeChanged)
public:
    Entity_PolyLineThick();

    int type()const override{return EntityType::Polyline;}

    QVector3D pn(const int &index) const {return m_pointsList.at(index);}

    QList<QVector3D> pointsList(){return m_pointsList;}
    int size() const {return m_pointsList.size();}

public slots:
    void addPn(const QVector3D &p_);
    void setPointsList(QList<QVector3D> pointsList_);
    void updateLastPoint(const QVector3D &p_);
    void removeLastPoint();
    void reset();

    void saveGeometryCorners() override;

signals:
    void pointsListChanged(QList<QVector3D> newPointsList);
    void sizeChanged(int newSize);   // void p0Changed(QVector3D newP0);

public:
    void updateGeometry() override;
   // bool pick(const QVector3D &pickPoint) override;


private:
    QList<QVector3D> m_pointsList;

};

#endif // ENTITY_POLYLINETHICK_H
