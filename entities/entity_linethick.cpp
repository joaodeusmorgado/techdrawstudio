#include "entity_linethick.h"

Entity_LineThick::Entity_LineThick()
{
    setCount(4);

    //Entity_LineThick::createGeometry();
    Entity_LineThick::updateGeometry();

    connect(this,SIGNAL(p0Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
    connect(this,SIGNAL(p1Changed(QVector3D)), this, SLOT(saveGeometryCorners()));

}

void Entity_LineThick::setP0(const QVector3D &p0)
{
    if (m_p0 == p0)
        return;
    m_p0 = p0;
    updateGeometry();
    emit p0Changed(p0);
}

void Entity_LineThick::setP0(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP0(v);
}

void Entity_LineThick::setP1(const QVector3D &p1)
{
    if (m_p1 == p1)
        return;
    m_p1 = p1;
    updateGeometry();
    emit p1Changed(p1);
}

void Entity_LineThick::setP1(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP1(v);
}

void Entity_LineThick::updateP0P1(const QVector3D &p0_, const QVector3D &p1_)
{
    m_vertexData.resize(m_count * 5 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    m_p0 = p0_;
    m_p1 = p1_;
    emit p0Changed(p0_);
    emit p1Changed(p1_);

    float angle = atan2(m_p1.y() - m_p0.y(), m_p1.x() - m_p0.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI

    float halfThick = m_thick * 0.5;

    float deltaX = halfThick * qCos(angle);
    float deltaY = halfThick * qSin(angle);

    //qDebug()<<"dealtX"<<deltaX;
    //qDebug()<<"dealtY"<<deltaY;

    QVector3D p0(m_p0.x() - deltaX, m_p0.y() - deltaY, m_p0.z());
    QVector3D p1(m_p1.x() - deltaX, m_p1.y() - deltaY, m_p1.z());
    QVector3D p2(m_p0.x() + deltaX, m_p0.y() + deltaY, m_p0.z());
    QVector3D p3(m_p1.x() + deltaX, m_p1.y() + deltaY, m_p1.z());

/*
    p2---------p3
    m_P0-----m_P1
    p0---------p1

*/

       //type is in radians, if you need it in degrees just do angle * 180 / PI
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V

    setVertexData(m_vertexData);
    m_vertexData.clear();
    update();
}

void Entity_LineThick::saveGeometryCorners()
{
    if (m_count == 0)
        return;

    setMinXRef(m_p0.x());
    setMinX(m_p0.x());
    setMinX(m_p1.x());

    setMinYRef(m_p0.y());
    setMinY(m_p0.y());
    setMinY(m_p1.y());

    setMinZRef(m_p0.z());
    setMinZ(m_p0.z());
    setMinZ(m_p1.z());

    setMaxXRef(m_p0.x());
    setMaxX(m_p0.x());
    setMaxX(m_p1.x());

    setMaxYRef(m_p0.y());
    setMaxY(m_p0.y());
    setMaxY(m_p1.y());

    setMaxZRef(m_p0.z());
    setMaxZ(m_p0.z());
    setMaxZ(m_p1.z());
}

/*void Entity_LineThick::setThick(const float &thick_)
{
    if (m_thick == thick_)
        return;
    m_thick = thick_;
    emit thickChanged(m_thick);
}*/

void Entity_LineThick::updateGeometry()
{
    clear();

    m_vertexData.resize(m_count * 5 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    float angle = atan2(m_p1.y() - m_p0.y(), m_p1.x() - m_p0.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI

    float halfThick = m_thick * 0.5;

    float deltaX = halfThick * qCos(angle);
    float deltaY = halfThick * qSin(angle);

    //qDebug()<<"dealtX"<<deltaX;
    //qDebug()<<"dealtY"<<deltaY;


    QVector3D p0(m_p0.x() - deltaX, m_p0.y() - deltaY, m_p0.z());
    QVector3D p1(m_p1.x() - deltaX, m_p1.y() - deltaY, m_p1.z());
    QVector3D p2(m_p0.x() + deltaX, m_p0.y() + deltaY, m_p0.z());
    QVector3D p3(m_p1.x() + deltaX, m_p1.y() + deltaY, m_p1.z());


/*
    p2---------p3
    m_P0-----m_P1
    p0---------p1

*/


       //type is in radians, if you need it in degrees just do angle * 180 / PI
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V


    setVertexData(m_vertexData);
    m_vertexData.clear();

    setStride(5 * sizeof(float));

    setPrimitiveType(QQuick3DGeometry::PrimitiveType::TriangleStrip);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Triangles);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);

    setBounds(p0, p3); //TODO: check the min, max values and if necessary invert them

    setStride(5 * sizeof(float));
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(QQuick3DGeometry::Attribute::TexCoordSemantic, 3 * sizeof(float), Attribute::F32Type);
    update();
}



bool Entity_LineThick::pick(const QVector3D &pickPoint)
{
    if (!inside_Pick(pickPoint)) {
        setPicked(false);
        return false;
    }

    QVector3D p0aux;
    QVector3D p1aux;

    if (p0().x() < p1().x())
    {
        p0aux = p0();
        p1aux = p1();
    }
    else
    {
        p0aux = p1();
        p1aux = p0();
    }

    //check for vertical lines
    if ( fabs(p0aux.x() - p1aux.x()) < m_pickInterval )
    {
        //check if point is near the vertical line
        if ( fabs( (p0aux.x() + p1aux.x()) / 2 - pickPoint.x()) < m_pickInterval )
        {
            setPickPoint(pickPoint);
            setPicked(true);
            return true;
        }
    }

    //check for horizontal lines
    if ( fabs(p0aux.y() - p1aux.y()) < m_pickInterval )
    {
        //check if point is near the horizontal line
        if ( fabs( (p0aux.y() + p1aux.y()) / 2 - pickPoint.y()) < m_pickInterval )
        {
            setPickPoint(pickPoint);
            setPicked(true);
            return true;
        }
    }

    // check other lines than vertical
    float lineX = vertexXmax() - vertexXmin();
    float lineY = vertexYmax() - vertexYmin();
    float distX = pickPoint.x() - vertexXmin();
    float distY = distX * lineY / lineX;

    //line is from left to right and top to bottom
    if (p0aux.y() > p1aux.y())
    {
        if (fabs(vertexYmax()-distY - pickPoint.y()) <  m_pickInterval)
        {
            setPickPoint(pickPoint);
            setPicked(true);
            return true;
        }
    }
    else //line is from left to right and bottom to top
    {
        if (fabs(vertexYmin() + distY - pickPoint.y()) < m_pickInterval)
        {
            setPickPoint(pickPoint);
            setPicked(true);
            return true;
        }
    }

    setPicked(false);
    return false;
}
