#include "entity_circlethick.h"
#include <math.h>

Entity_CircleThick::Entity_CircleThick()
    : m_pointInRadius(0.00001f,0.00001f,0.00001f)//use a diferent value then zero to avoid a inititalization bug, where
    // in setPointInRadius() the updateRadiusFromPoint() will no be called if the user set defined m_pointInRadius to zero
{
    setCount(100);
}

void Entity_CircleThick::setCenter(const QVector3D &center)
{
    if (m_center == center)
        return;
    m_center = center;
    updateGeometry();
    emit centerChanged(m_center);
}

void Entity_CircleThick::setCenter(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setCenter(v);
}

void Entity_CircleThick::setPointInRadius(const QVector3D &pointInRadius)
{
    //qDebug()<<"pointInRadius: "<<pointInRadius;
    //qDebug()<<"m_pointInRadius: "<<m_pointInRadius;
    if (m_pointInRadius == pointInRadius)
        return;
    m_pointInRadius = pointInRadius;
    updateRadiusFromPoint();
    //updateGeometry(); //dont update here, updateRadiusFromPoint() calls setRadius() that will call updateGeometry() there
    emit pointInRadiusChanged(m_pointInRadius);
}

void Entity_CircleThick::setPointInRadius(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setPointInRadius(v);
}

void Entity_CircleThick::setRadius(float radius)
{
    if (m_radius == radius)
        return;
    m_radius = radius;
    updateGeometry();
    emit radiusChanged(m_radius);
}

void Entity_CircleThick::updateGeometry()
{
    clear();
    //qDebug()<<"circle thick update geometry.................";

    m_vertexData.resize( (m_count+1) * 2 * 5 * sizeof (float) );

    float *p = reinterpret_cast<float *>(m_vertexData.data());
    //float m_radius = radius();
    float angle = 0;
    float halfThick = m_thick * 0.5;
    float inverseCount = 1.0f / m_count;
    float auxU; //aux for U texture coordinate

    for(int i = 0; i <= m_count; angle += 2*M_PI/m_count)
    {
        *p++ = m_center.x() + (m_radius + halfThick) * sin(angle);
        *p++ = m_center.y() + (m_radius + halfThick) * cos(angle);
        *p++ = m_center.z();
        auxU = inverseCount * i ; // U
        *p++ = auxU ; // U
        *p++ = 1.0f; // V

        *p++ = m_center.x() + (m_radius - halfThick) * sin(angle);
        *p++ = m_center.y() + (m_radius - halfThick) * cos(angle);
        *p++ = m_center.z();
        *p++ = auxU; // U
        *p++ = 0.0f; // V

        //*p++ = m_center.x() + m_radius * sin(angle);
        //*p++ = m_center.y() + m_radius * cos(angle);
        //*p++ = m_center.z(); // TODO: check if should be m_pointInRadius.z()
        i++;

    }

    setVertexData(m_vertexData);
    m_vertexData.clear();

    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);
    setPrimitiveType(QQuick3DGeometry::PrimitiveType::TriangleStrip);
    //setStride(3 * sizeof(float));
    // TODO: add setBounds() for picking()
    //addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);

    setStride(5 * sizeof(float));
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    addAttribute(QQuick3DGeometry::Attribute::TexCoordSemantic, 3 * sizeof(float), Attribute::F32Type);
    update();
}

void Entity_CircleThick::updateRadiusFromPoint()
{
    QVector3D v;
    v.setX(m_pointInRadius.x() - m_center.x());
    v.setY(m_pointInRadius.y() - m_center.y());
    v.setZ(m_pointInRadius.z() - m_center.z());
    setRadius( v.length() );
    qDebug()<<"updateRadiusFromPoint() - radius: "<<m_radius;
}
