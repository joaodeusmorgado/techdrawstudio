#ifndef LINETEXTURE_H
#define LINETEXTURE_H

#include <QQuick3DTextureData>
#include <QObject>
#include <QColor>

class LineTexture : public QQuick3DTextureData
{
    Q_OBJECT
    Q_PROPERTY(QColor color1 READ color1 WRITE setColor1 NOTIFY color1Changed)
    Q_PROPERTY(QColor color2 READ color2 WRITE setColor2 NOTIFY color2Changed)

public:
    LineTexture();


    QColor color1() const {return m_color1;}
    QColor color2() const {return m_color2;}

public slots:
    void setColor1(const QColor &color1_);
    void setColor2(const QColor &color2_);

signals:
    void color1Changed(QColor newColor1);
    void color2Changed(QColor newColor2);

private:
    void generateTextureData();

private:
    QColor m_color1;
    QColor m_color2;
    QByteArray m_textureData;//color patern
    float scale;
};

#endif // LINETEXTURE_H
