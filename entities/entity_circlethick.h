#ifndef ENTITY_CIRCLETHICK_H
#define ENTITY_CIRCLETHICK_H

#include <QObject>
#include "entity.h"

#include <QObject>
#include "entity.h"

class Entity_CircleThick : public Entity
{
    Q_OBJECT
    Q_PROPERTY(QVector3D center READ center WRITE setCenter NOTIFY centerChanged)
    Q_PROPERTY(QVector3D pointInRadius READ pointInRadius WRITE setPointInRadius NOTIFY pointInRadiusChanged)
    Q_PROPERTY(float radius READ radius WRITE setRadius NOTIFY radiusChanged)
public:
    Entity_CircleThick();

    //~Entity_Line();
    int type() const override { return EntityType::Circle; }

    QVector3D center() const { return m_center; }
    QVector3D pointInRadius() const { return m_pointInRadius; }
    float radius() const { return m_radius; }

public slots:
    void setCenter(const QVector3D &center);
    void setCenter(const float &px, const float &py, const float &pz);
    void setPointInRadius(const QVector3D &pointInRadius);
    void setPointInRadius(const float &px, const float &py, const float &pz);
    void setRadius(float radius);
    //do not use both setPointInRadius() and setRadius() at the same time, choose only one

signals:
    void centerChanged(QVector3D newCenter);
    void pointInRadiusChanged(QVector3D newPointInRadius);
    void radiusChanged(float newRadius);

private:
    void updateGeometry() override;
    void updateRadiusFromPoint();
    QVector3D m_center;
    QVector3D m_pointInRadius;
    float m_radius;
};
#endif // ENTITY_CIRCLETHICK_H
