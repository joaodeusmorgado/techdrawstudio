#ifndef ENUMS_H
#define ENUMS_H


#include <QObject>
#include <QtQml>

//enum used in c++
namespace Event {

    enum Event
    {
        Pressed,
        Released,
        Clicked,
        RightClicked,
        PolylineFinish,
        MouseChanged
    };
}

//enum class to expose to qml
class Events : public QObject
{
    Q_OBJECT
public:
    enum EnEvents
    {
        Pressed,
        Released,
        Clicked,
        RightClicked,
        PolylineFinish,
        MouseChanged
    };
    Q_ENUM(EnEvents)

    static void declareQML() {
        qmlRegisterType<Events>("QMLEnumEvents", 1, 0, "Events");
    }
};


#endif // ENUMS_H
