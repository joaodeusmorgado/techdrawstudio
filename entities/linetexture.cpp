#include "linetexture.h"
#include <QSize>

LineTexture::LineTexture()
{

}

void LineTexture::setColor1(const QColor &color1_)
{
    if (m_color1 == color1_)
        return;
    m_color1 = color1_;
    generateTextureData();
    emit color1Changed(color1_);
    update();
}

void LineTexture::setColor2(const QColor &color2_)
{
    if (m_color2 == color2_)
        return;
    m_color2 = color2_;
    generateTextureData();
    emit color2Changed(color2_);
    update();
}

void LineTexture::generateTextureData()
{
    //m_textureData.resize(2 * sizeof (QColor));
    m_textureData.resize(2 * 6 * sizeof (float));
    //m_textureData.resize(2 * 4 * sizeof (int));
    float *p = reinterpret_cast<float *>(m_textureData.data());
    //int *p = reinterpret_cast<int *>(m_textureData.data());

  /*  *p++ = m_color1.red();
    *p++ = m_color1.green();
    *p++ = m_color1.blue();
    *p++ = m_color1.alpha();

    *p++ = m_color2.red();
    *p++ = m_color2.green();
    *p++ = m_color2.blue();
    *p++ = m_color2.alpha();
*/
    *p++ = m_color1.redF();
    *p++ = m_color1.greenF();
    *p++ = m_color1.blueF();
    *p++ = m_color1.alphaF();

    *p++ = m_color2.redF();
    *p++ = m_color2.greenF();
    *p++ = m_color2.blueF();
    *p++ = m_color2.alphaF();

    setTextureData(m_textureData);
    m_textureData.clear();
    setSize(QSize(2,1));
    //setFormat(QQuick3DTextureData::Format::RGBA8);
    setFormat(QQuick3DTextureData::Format::RGBA32F);
    setHasTransparency(true);
}
