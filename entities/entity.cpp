#include "entity.h"
#include <QRandomGenerator>

Entity::Entity(QQuick3DObject *ptr) :
    QQuick3DGeometry(ptr),
    //m_count(1),
    m_thick(1.0),
    m_isPicked(false),
    m_isHighlighted(false),
    m_pickInterval(m_thick+6)
{

}

void Entity::setCount(int count)
{
    if (count == m_count)
        return;
    m_count = count;
    updateGeometry();
    emit countChanged(m_count);
}

void Entity::setThick(const float &thick_)
{
    if (m_thick == thick_)
        return;
    m_thick = thick_;
    updateGeometry();
    emit thickChanged(m_thick);
}

void Entity::setPickPoint(const QVector3D &pickPoint_)
{
    if (m_pick_point == pickPoint_)
        return;
    m_pick_point = pickPoint_;
    emit pickPointChanged(pickPoint_);
    pick(pickPoint_);
}



void Entity::setPicked(const bool &picked_)
{
    if (m_isPicked == picked_)
        return;
    m_isPicked = picked_;
    //updateGeometry();
    //qDebug()<<"--------------------------------------------------------------";
    //qDebug()<<"m_isPicked: "<<m_isPicked;
    emit pickedChanged(m_isPicked);
}

void Entity::setHighlighted(const bool &highlighted_)
{
    if (m_isHighlighted == highlighted_)
        return;
    m_isHighlighted = highlighted_;
    emit highlightedChanged(m_isHighlighted);
}

void Entity::setPickInterval(const float &pickInterval_)
{
    if (m_pickInterval == pickInterval_)
        return;
    m_pickInterval = pickInterval_;
    emit pickIntervalChanged(m_pickInterval);
}

QVector3D Entity::generateRandomPoint(float min, float max) const
{
    float px = QRandomGenerator::global()->generateDouble() *(max - min) + min;
    float py = QRandomGenerator::global()->generateDouble() *(max - min) + min;
    float pz = QRandomGenerator::global()->generateDouble() *(max - min) + min;

    return QVector3D(px, py, pz);
}

/*bool Entity::inside_Pick(const QVector3D &point, const float &pickInterval)
{
    //qDebug()<<"vertexXmin: "<<vertexXmin();
    //qDebug()<<"vertexXmax: "<<vertexXmax();
    //qDebug()<<"vertexYmin: "<<vertexYmin();
    //qDebug()<<"vertexYmax: "<<vertexYmax();

    if (point.x() < vertexXmin()-pickInterval)
        return false;
    if (point.x() > vertexXmax()+pickInterval)
        return false;
    if (point.y() < vertexYmin()-pickInterval)
        return false;
    if (point.y() > vertexYmax()+pickInterval)
        return false;
    if (point.z() < vertexZmin()-pickInterval)
        return false;
    if (point.z() > vertexZmax()+pickInterval)
        return false;

    return true;
}*/

bool Entity::inside_Pick(const QVector3D &point)
{
    //qDebug()<<"vertexXmin: "<<vertexXmin();
    //qDebug()<<"vertexXmax: "<<vertexXmax();
    //qDebug()<<"vertexYmin: "<<vertexYmin();
    //qDebug()<<"vertexYmax: "<<vertexYmax();

    if (point.x() < vertexXmin()-m_pickInterval)
        return false;
    if (point.x() > vertexXmax()+m_pickInterval)
        return false;
    if (point.y() < vertexYmin()-m_pickInterval)
        return false;
    if (point.y() > vertexYmax()+m_pickInterval)
        return false;
    if (point.z() < vertexZmin()-m_pickInterval)
        return false;
    if (point.z() > vertexZmax()+m_pickInterval)
        return false;

    return true;
}

bool Entity::inside(const QVector3D &point)
{
    if (point.x() < vertexXmin())
        return false;
    if (point.x() > vertexXmax())
        return false;
    if (point.y() < vertexYmin())
        return false;
    if (point.y() > vertexYmax())
        return false;
    if (point.z() < vertexZmin())
        return false;
    if (point.z() > vertexZmax())
        return false;

    return true;
}
// picking -----------------------------------
