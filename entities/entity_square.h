#ifndef ENTITY_SQUARE_H
#define ENTITY_SQUARE_H

#include <QQuick3DGeometry>
#include <QObject>
#include <QVector3D>
#include <QColor>

class Entity_Square : public QQuick3DGeometry
{
    Q_OBJECT
    Q_PROPERTY(QVector3D center READ center WRITE setCenter NOTIFY centerChanged)
    Q_PROPERTY(float squareDist READ squareDist WRITE setSquareDist NOTIFY squareDistChanged)

public:
    Entity_Square(QQuick3DObject *ptr = nullptr);

    QVector3D center() const { return m_center; }
    float squareDist() const { return m_squareDist; }

public slots:
    void setCenter(const QVector3D &center);
    void setCenter(const float &px, const float &py, const float &pz);
    void setSquareDist(const float &squareDist);

signals:
    void centerChanged(QVector3D newCenter);
    void squareDistChanged(float newSquareDist);

private:
    void createGeometry();
    void updateGeometry();

    QByteArray m_vertexData;
    int m_count;
    QVector3D m_center;
    float m_squareDist;
};

#endif // ENTITY_SQUARE_H
