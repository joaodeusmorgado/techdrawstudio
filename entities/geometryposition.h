#ifndef GEOMETRYPOSITION_H
#define GEOMETRYPOSITION_H


// This class saves the vertex corners of entities
// For example, a line with the vertex's
// P0(-5,3, -5) and P1(7,-4,12) it will save the vertex's
// vertex_xmin = -5
// vertex_xmax = 7
// vertex_ymin = -4
// vertex_ymax = 3
// vertex_zmin = -5
// vertex_zmax = 12

//This is usefull because it with gives the entity geometry limits
// that we can use for entity selecting and snap


#include <QVector3D>

class GeometryPosition
{
public:
    GeometryPosition();

    void setMinXRef(const float &minXRef){m_minX = minXRef;}
    void setMinYRef(const float &minYRef){m_minY = minYRef;}
    void setMinZRef(const float &minZRef){m_minZ = minZRef;}

    void setMinX(const float &minX);
    void setMinY(const float &minY);
    void setMinZ(const float &minZ);

    void setMaxXRef(const float &maxXRef){m_maxX = maxXRef;}
    void setMaxYRef(const float &maxYRef){m_maxY = maxYRef;}
    void setMaxZRef(const float &maxZRef){m_maxZ = maxZRef;}

    void setMaxX(const float &maxX);
    void setMaxY(const float &maxY);
    void setMaxZ(const float &maxZ);


  /*  void setVertexXmin(const float &xmin){vertex_xmin = xmin;}
     void setVertexXmax(const float &xmax){vertex_xmax = xmax;}
     void setVertexYmin(const float &ymin){vertex_ymin = ymin;}
     void setVertexYmax(const float &ymax){vertex_ymax = ymax;}
     void setVertexZmin(const float &zmin){vertex_zmin = zmin;}
     void setVertexZmax(const float &zmax){vertex_zmax = zmax;}
*/
     float vertexXmin(){return m_minX;}
     float vertexYmin(){return m_minY;}
     float vertexZmin(){return m_minZ;}

     float vertexXmax(){return m_maxX;}
     float vertexYmax(){return m_maxY;}
     float vertexZmax(){return m_maxZ;}

     //void reset();

     QVector3D middlePoint();

private:
    float m_minX;
    float m_minY;
    float m_minZ;

    float m_maxX;
    float m_maxY;
    float m_maxZ;
};

#endif // GEOMETRYPOSITION_H
