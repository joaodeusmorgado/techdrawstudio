#ifndef ENTITY_H
#define ENTITY_H

#include <QQuick3DGeometry>
#include <QObject>
#include <QVector3D>
#include <QtMath>

#include "geometryposition.h"

//typedef QSharedPointer<QVector3D> QVector3DPtr;
namespace EntityType {
    enum Entity {
        None,
        Line,
        LineThick,
        Polyline,
        Circle,
        Rectangle,
        Block
    };
}

class Entity : public QQuick3DGeometry, public GeometryPosition
{
    Q_OBJECT
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(float thick READ thick WRITE setThick NOTIFY thickChanged)
    Q_PROPERTY(bool picked READ picked WRITE setPicked NOTIFY pickedChanged)
    Q_PROPERTY(bool highlighted READ highlighted WRITE setHighlighted NOTIFY highlightedChanged)
    Q_PROPERTY(QVector3D pickPoint READ pickPoint WRITE setPickPoint NOTIFY pickPointChanged)
    Q_PROPERTY(float pickInterval READ pickInterval WRITE setPickInterval NOTIFY pickIntervalChanged)
public:
    Entity(QQuick3DObject *ptr = nullptr);
    virtual int type()const{return EntityType::None;}
    virtual void updateGeometry(){}

    int count() const {return m_count;}
    float thick() const {return m_thick;}
    bool picked() const {return m_isPicked;}
    bool highlighted() const {return m_isHighlighted;}
    QVector3D pickPoint() const {return m_pick_point;}
    float pickInterval() const {return m_pickInterval;}

    virtual bool pick(const QVector3D &pickPoint){Q_UNUSED(pickPoint);return true;}

public slots:
    void setCount(int count);
    void setThick(const float &thick_);
    void setPickPoint(const QVector3D &pickPoint_);
    void setPicked(const bool &picked_);
    void setHighlighted(const bool &highlighted_);
    void setPickInterval(const float &pickInterval_);

signals:
    void countChanged(int newCount);
    void thickChanged(float newThick);

    void pickedChanged(bool newPicked);
    void highlightedChanged(bool newHighlighted);

    void pickPointChanged(QVector3D newPickPoint);
    void pickIntervalChanged(float newPickInterval);

protected:
    QVector3D generateRandomPoint(float min, float max) const;

public:
    // saveGeometryCorners() saves entity geometry limits
    //i.e. a line with p0(-5,-5) and p1(10,7), the geometry limits will be a "rectangle" from (-5,5) to (10,7)
    // this will be usefull to help picking the entity
    virtual void saveGeometryCorners(){}

    // inside_Pick() checks if a point is inside the Geometry corners, it will be usefull to help picking the entity
    //uses a pickInterval delta
    //Q_INVOKABLE bool inside_Pick(const QVector3D &point, const float &pickInterval);
    Q_INVOKABLE bool inside_Pick(const QVector3D &point);

    // inside() checks if a point is inside the Geometry corners, no delta used
    Q_INVOKABLE bool inside(const QVector3D &point);

    //Q_INVOKABLE bool chekHighLight();

protected:
    QByteArray m_vertexData;
    int m_count;
    float m_thick;

    QVector3D m_pick_point;
    bool m_isPicked;
    bool m_isHighlighted;
    float m_pickInterval;

};

#endif // ENTITY_H
