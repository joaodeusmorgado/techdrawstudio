#include "entity_square.h"

Entity_Square::Entity_Square(QQuick3DObject *ptr) :
    QQuick3DGeometry(ptr),
    m_count(5),
    m_squareDist(5)
{
    createGeometry();
}

void Entity_Square::setCenter(const QVector3D &center)
{
    if (m_center == center)
        return;
    m_center = center;
    updateGeometry();
    emit centerChanged(m_center);
}

void Entity_Square::setCenter(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setCenter(v);
}

void Entity_Square::setSquareDist(const float &squareDist)
{
    if (m_squareDist == squareDist)
        return;
    m_squareDist = squareDist;
    updateGeometry();
    emit squareDistChanged(m_squareDist);
}

void Entity_Square::createGeometry()
{
    clear();

    m_vertexData.resize(m_count * 3 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    // square top left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    // square top right
    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    // square bottom right
    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    // square bottom left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    // square top left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    setVertexData(m_vertexData);
    m_vertexData.clear();

    setStride(3 * sizeof(float));
    setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);

    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    update();

}

void Entity_Square::updateGeometry()
{
    m_vertexData.resize(m_count * 3 * sizeof(float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    // square top left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    // square top right
    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    // square bottom right
    *p++ = m_center.x() + m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    // square bottom left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() - m_squareDist;
    *p++ = m_center.z();

    // square top left
    *p++ = m_center.x() - m_squareDist;
    *p++ = m_center.y() + m_squareDist;
    *p++ = m_center.z();

    setVertexData(m_vertexData);
    m_vertexData.clear();
    update();

}
