#include "entity_rectanglethick.h"

Entity_RectangleThick::Entity_RectangleThick()
{
    setCount(22);

    connect(this,SIGNAL(p0Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
    connect(this,SIGNAL(p1Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
    connect(this,SIGNAL(p2Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
    connect(this,SIGNAL(p3Changed(QVector3D)), this, SLOT(saveGeometryCorners()));
}

void Entity_RectangleThick::setP0(const QVector3D &p0)
{
    if (m_p0 == p0)
        return;
    m_p0 = p0;
   // m_p1.setY(m_p0.y());
    //m_p2.setX(m_p0.x());
    //setP1_from_P0_P3();
    //setP2_from_P0_P3();
    updateGeometry();
    emit p0Changed(m_p0);
    //qDebug()<<"Entity_Rectangle p0: "<<p0;
}

void Entity_RectangleThick::setP0(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP0(v);
}

void Entity_RectangleThick::setP1(const QVector3D &p1)
{
    if (m_p1 == p1)
        return;
    m_p1 = p1;
    updateGeometry();
    emit p1Changed(m_p1);
    //qDebug()<<"Entity_Rectangle p1: "<<p1;
}

void Entity_RectangleThick::setP1(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP1(v);
}

void Entity_RectangleThick::setP2(const QVector3D &p2)
{
    if (m_p2 == p2)
        return;
    m_p2 = p2;
    updateGeometry();
    emit p2Changed(p2);
}

void Entity_RectangleThick::setP2(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP2(v);
}

void Entity_RectangleThick::setP3(const QVector3D &p3)
{
    //qDebug()<<"Entity_Rectangle p3"<<p3;
    if (m_p3 == p3)
        return;
    m_p3 = p3;
    //m_p2.setY(m_p3.y());
    //m_p1.setX(m_p3.x());
    //setP1_from_P0_P3();
    //setP2_from_P0_P3();
    updateGeometry();
    emit p3Changed(m_p3);
}

void Entity_RectangleThick::setP3(const float &px, const float &py, const float &pz)
{
    QVector3D v(px, py, pz);
    setP3(v);
}

void Entity_RectangleThick::saveGeometryCorners()
{
    if (m_count == 0)
        return;

    setMinXRef(m_p0.x());
    setMinX(m_p0.x());
    setMinX(m_p1.x());
    setMinX(m_p2.x());
    setMinX(m_p3.x());

    setMinYRef(m_p0.y());
    setMinY(m_p0.y());
    setMinY(m_p1.y());
    setMinY(m_p2.y());
    setMinY(m_p3.y());

    setMinZRef(m_p0.z());
    setMinZ(m_p0.z());
    setMinZ(m_p1.z());
    setMinZ(m_p2.z());
    setMinZ(m_p3.z());

    setMaxXRef(m_p0.x());
    setMaxX(m_p0.x());
    setMaxX(m_p1.x());
    setMaxX(m_p2.x());
    setMaxX(m_p3.x());

    setMaxYRef(m_p0.y());
    setMaxY(m_p0.y());
    setMaxY(m_p1.y());
    setMaxY(m_p2.y());
    setMaxY(m_p3.y());

    setMaxZRef(m_p0.z());
    setMaxZ(m_p0.z());
    setMaxZ(m_p1.z());
    setMaxZ(m_p2.z());
    setMaxZ(m_p3.z());

}

//-----------------------------
/*
void Entity_Rectangle::setP1_from_P0_P3()
{
    setP1(m_p3.x(), m_p0.y(), m_p0.z());//todo: check if p1.z should be the average of po.z and p3.z
    qDebug()<<"Entity_Rectangle setP1_from_P0_P3: "<<m_p1;
}

void Entity_Rectangle::setP2_from_P0_P3()
{
    setP2(m_p0.x(), m_p3.y(), m_p3.z());//todo: check if p2.z should be the average of po.z and p3.z
    qDebug()<<"Entity_Rectangle setP2_from_P0_P3: "<<m_p2;
}*/


void Entity_RectangleThick::updateGeometry()
{
    clear();

    m_vertexData.resize(m_count * 5 * sizeof (float));
    float *p = reinterpret_cast<float *>(m_vertexData.data());

    float halfThick = m_thick * 0.5;

    float angle = atan2(m_p1.y() - m_p0.y(), m_p1.x() - m_p0.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI
    float deltaX = halfThick * qCos(angle);
    float deltaY = halfThick * qSin(angle);
    /*qDebug()<<"m_p0: "<<m_p0;
    qDebug()<<"m_p1: "<<m_p1;
    qDebug()<<"m_p2: "<<m_p2;
    qDebug()<<"m_p3: "<<m_p3;

    qDebug()<<"angle: "<<angle;
    qDebug()<<"deltaX: "<<deltaX;
    qDebug()<<"deltaY: "<<deltaY;
*/
    QVector3D p0(m_p0.x() - deltaX, m_p0.y() - deltaY, m_p0.z());
    QVector3D p1(m_p1.x() - deltaX, m_p1.y() - deltaY, m_p1.z());
    QVector3D p2(m_p0.x() + deltaX, m_p0.y() + deltaY, m_p0.z());
    QVector3D p3(m_p1.x() + deltaX, m_p1.y() + deltaY, m_p1.z());

    //-----------------------------------------------------------
    //start rectangle first line p0->p1
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V
    //end rectangle first line p0->p1
    //-----------------------------------------------------------


    //triangle degenerated vertex - repeat previous vertex
    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V

    angle = atan2(m_p2.y() - m_p1.y(), m_p2.x() - m_p1.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI
    deltaX = halfThick * qCos(angle);
    deltaY = halfThick * qSin(angle);

    p0 = QVector3D(m_p1.x() - deltaX, m_p1.y() - deltaY, m_p1.z());
    p1 = QVector3D(m_p2.x() - deltaX, m_p2.y() - deltaY, m_p2.z());
    p2 = QVector3D(m_p1.x() + deltaX, m_p1.y() + deltaY, m_p1.z());
    p3 = QVector3D(m_p2.x() + deltaX, m_p2.y() + deltaY, m_p2.z());

    //triangle degenerated vertex - repeat next vertex
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    //-----------------------------------------------------------
    //start rectangle second line p1->p2

    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V

    //end rectangle second line p1->p2
    //-----------------------------------------------------------

    //triangle degenerated vertex - repeat previous vertex
    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V


    angle = atan2(m_p3.y() - m_p2.y(), m_p3.x() - m_p2.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI
    deltaX = halfThick * qCos(angle);
    deltaY = halfThick * qSin(angle);

    p0 = QVector3D(m_p2.x() - deltaX, m_p2.y() - deltaY, m_p2.z());
    p1 = QVector3D(m_p3.x() - deltaX, m_p3.y() - deltaY, m_p3.z());
    p2 = QVector3D(m_p2.x() + deltaX, m_p2.y() + deltaY, m_p2.z());
    p3 = QVector3D(m_p3.x() + deltaX, m_p3.y() + deltaY, m_p3.z());

    //triangle degenerated vertex - repeat next vertex
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    //-----------------------------------------------------------
    //start rectangle second line p2->p3
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V
    //end rectangle second line p2->p3
    //-----------------------------------------------------------

    //triangle degenerated vertex - repeat previous vertex
    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V

    angle = atan2(m_p0.y() - m_p3.y(), m_p0.x() - m_p3.x()) + M_PI_2;
    //angle is in radians, if you need it in degrees just do angle * 180 / PI
    deltaX = halfThick * qCos(angle);
    deltaY = halfThick * qSin(angle);

    p0 = QVector3D(m_p3.x() - deltaX, m_p3.y() - deltaY, m_p3.z());
    p1 = QVector3D(m_p0.x() - deltaX, m_p0.y() - deltaY, m_p0.z());
    p2 = QVector3D(m_p3.x() + deltaX, m_p3.y() + deltaY, m_p3.z());
    p3 = QVector3D(m_p0.x() + deltaX, m_p0.y() + deltaY, m_p0.z());

    //triangle degenerated vertex - repeat next vertex
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    //-----------------------------------------------------------
    //start rectangle second line p3->p0
    *p++ = p0.x();
    *p++ = p0.y();
    *p++ = p0.z();
    *p++ = 0.0f; // U
    *p++ = 0.0f; // V

    *p++ = p1.x();
    *p++ = p1.y();
    *p++ = p1.z();
    *p++ = 1.0f; // U
    *p++ = 0.0f; // V

    *p++ = p2.x();
    *p++ = p2.y();
    *p++ = p2.z();
    *p++ = 0.0f; // U
    *p++ = 1.0f; // V

    *p++ = p3.x();
    *p++ = p3.y();
    *p++ = p3.z();
    *p++ = 1.0f; // U
    *p++ = 1.0f; // V
    //end rectangle second line p3->p0
    //-----------------------------------------------------------

    setVertexData(m_vertexData);
    m_vertexData.clear();
    setPrimitiveType(QQuick3DGeometry::PrimitiveType::TriangleStrip);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::Lines);
    //setPrimitiveType(QQuick3DGeometry::PrimitiveType::LineStrip);
    setStride(5 * sizeof(float));
    setBounds(m_p0, m_p2);
    addAttribute(QQuick3DGeometry::Attribute::PositionSemantic, 0, Attribute::F32Type);
    update();
}

